{-# LANGUAGE NamedFieldPuns  #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections   #-}

import qualified Data.HashMap.Lazy                as HMap
import           Data.List
import           Data.Ord
import           Language.GhcCore.Backend.Phi
import           Language.Haskell.Backend.GhcCore
import           Phileas.Prelude
import           Phileas.Prover
import           System.FilePath
import           Test.Tasty
import           Test.Tasty.HUnit

data Tree n a =
  Leaf a
  | Node n [Tree n a]

data TestParams = TestParams {
  _testPredName :: String
  , _testVeredict :: ProverAnswer -> Bool
  , _testMaxInductionDepth :: Int
  }

type TreeOfTests = Tree TestName (TestName, [TestParams])

main :: IO ()
main = testFiles >>= defaultMain

testingOptions :: TestParams -> PhileasOptions
testingOptions TestParams{..} = testOptions {
  _maxInductionDepth = _testMaxInductionDepth
  }

scanTestFile :: FilePath -> IO (HashMap String (Phileas Identity ProveResult))
scanTestFile hsFile =
  fmap snd . proveModule <$> parseFilteredModule [incl] hsFile
  where incl = "src/"

testFile :: FilePath -> TreeOfTests -> IO TestTree
testFile hsFile tree = flip testProveResult tree <$> scanTestFile hsFile

testProveResult :: HashMap String (Phileas Identity ProveResult) -> TreeOfTests -> TestTree
testProveResult m tree = case tree of
  Node name trees    -> testGroup name (map (testProveResult m) trees)
  Leaf (name, params) -> testGroup name (map testPred params)
  where
    assertBool' = assertBool "failed"
    testPred params@TestParams{..} =
      testCase _testPredName $ assertBool' (_testVeredict res)
      where res = fromJustNote "timeout unexpected" .  resultAnswer
              $ runPhileasNoTrace (testingOptions params)
              $ HMap.lookupDefault missing _testPredName m
              where missing = error $ "missing property " ++ _testPredName

test :: Int -> String -> (ProverAnswer -> Bool) -> TestParams
test d name cond = TestParams name cond d

test1 :: String -> (ProverAnswer -> Bool) -> TestParams
test0 = test 0
test1 = test 1
test2 = test 2
test3 = test 3


testFiles :: IO TestTree
testFiles = testGroup "Tests" <$> sequence all
  where
    testTreeFile :: FilePath -> TreeOfTests -> IO TestTree
    testTreeFile hs = testFile ("test/Examples" </> hs)
    all = [
       testTrivial
       ,  testNat
        -- testZeno
      , testList
      , testClasses
      , testImplications
      , testTree
      ]
    testZeno :: IO TestTree
    testZeno = let hs = "ZenoExamples.hs" in
      testTreeFile hs $ Leaf (takeBaseName hs,
        [
          test1 "propAddRightIdent" provenTrue
          , test1 "propLengthInsertsort" provenTrue
          , test1 "propLengthInsertsort" provenTrue
          , test1 "propMulRightIdent" provenTrue
          , test2 "propReverseAppend" provenTrue
          , test1 "propLeqTrn" provenTrue
          , test1 "propLengthDrop" provenTrue
          , test1 "propCountReverse" provenTrue
          , test2 "propReverseTwice" provenTrue
          , test1 "propTakeDrop1" provenTrue
          , test1 "propMaxAssoc" provenTrue
          , test1 "propLengthReverse" provenTrue
          , test1 "propMulCommu" provenTrue
          , test1 "propDropDrop" provenTrue
          , test2 "propHeightMirror" provenTrue
          , test1 "propCountAddApp" provenTrue
          , test1 "propCountSnoc" provenTrue
          , test1 "propTakeDrop2" provenTrue
          , test1 "propMulAddDist" provenTrue
          , test2 "propMinAssoc" provenTrue
          , test1 "propElemAppendRight" provenTrue
          , test1 "propAddCommu" provenTrue
          , test1 "propLengthFilter" provenTrue
          , test1 "propLengthSnoc" provenTrue
          , test2 "propInsertsortIdem" provenTrue
          , test1 "propElemInsertEq" provenTrue
          , test3 "propDropMap" provenTrue
          , test1 "propElemAppendLeft" provenTrue
          , test1 "propInsertsortSorts" provenTrue
          , test1 "propInsertSorts" provenTrue
          , test1 "propLengthDelete" provenTrue
          , test1 "propAddAssoc" provenTrue
          , test1 "propTakeMap" provenTrue
          , test1 "propMulAssoc" provenTrue
          , test1 "propLeqRef" provenTrue
          , test1 "propLeqTotal" provenTrue
          , test1 "propMirrorTwice" provenTrue
          , test1 "propElemInsert" provenTrue
          , test1 "propCountInsertsort" provenTrue
          , test1 "propCountLeqApp" provenTrue
          , test1 "propFilterApp" provenTrue
          , test1 "propAppendAssoc" provenTrue
          ])
    testImplications :: IO TestTree
    testImplications = let hs = "Implications.hs" in
      testTreeFile hs $ Leaf (takeBaseName hs,
      [test1 "transitivity" provenTrue
      , test1 "symmetry" provenTrue
      , test1 "purity" provenTrue
      ])
    testTree :: IO TestTree
    testTree = let hs = "Tree.hs" in
      testTreeFile hs $ Leaf (takeBaseName hs,
      [
        test2 "heightMirror" provenTrue
      , test1 "doubleMirror" provenTrue
      ])
    testTrivial :: IO TestTree
    testTrivial = let hs = "Trivial.hs" in
      testTreeFile hs $ Leaf (takeBaseName hs,
      [test1 "andCommut" provenTrue
      , test1 "unit" provenTrue
      , test1 "andAssoc" provenTrue
      , test1 "andId" provenTrue
      , test1 "orId" provenTrue
      , test1 "notNot" provenTrue
      , test1 "may" (not . provenTrue)
      , test1 "ten" provenTrue
      , test1 "zeroLeft" provenTrue
      , test1 "mulZeroLeft" provenTrue
      , test1 "mulOneLeft" provenTrue
      , test1 "tailSnoc" provenTrue
      , test1 "absurd1" (not . provenTrue)
      , test1 "absurd2" (not . provenTrue)
      , test1 "bottomCase" provenTrue
      ])
    testList = let hs = "List.hs" in
      testTreeFile hs $ Leaf (takeBaseName hs,
      [
        test1 "sumLength" provenTrue
       , test1 "mapConst" provenTrue
       , test1 "lengthMap" provenTrue
       , test1 "memberSnoc" provenTrue
       , test1 "lengthConcat" provenTrue
       , test1 "lastCons" provenTrue
       , test1 "lengthReplicate" provenTrue
       , test1 "lastSnoc" provenTrue
       , test2 "lengthReverse" provenTrue
       , test1 "memberInsert" provenTrue
       , test2 "takeMap" provenTrue
       , test2 "reverseTail" provenTrue
       , test2 "reverseReverse" provenTrue
       , test2 "reverseConcat" provenTrue
      ])
    testNat = let hs = "Nat.hs" in
      testTreeFile hs $ Leaf (takeBaseName hs,
      [
       test1 "addZeroRight" provenTrue
       , test1 "addSucRight" provenTrue
       , test1 "structEqImpliesEq" provenTrue
       , test1 "eqImpliesStructEq" provenTrue
       , test1 "partialOrderRefl" provenTrue
       , test1 "partialOrderAntisimmetry" provenTrue
       , test1 "partialOrderTransitivity" provenTrue
      ])
    testClasses = let hs = "Classes.hs" in
      testTreeFile hs $ Node (takeBaseName hs)
      [ Node "Pair"
        [ Leaf ("Functor",
                [ test1 "funcPairId" provenTrue
                , test1 "funcPairCompose" provenTrue
                ])]
      , Node "Maybe"
        [ Leaf ("Functor",
                [
                  test1 "funcMaybeId" provenTrue
                , test1 "funcMaybeCompose" provenTrue
                ])
        , Leaf ("Applicative Functor",
                [
                  test1 "appMaybeId" provenTrue
                , test1 "appMaybeCompose" provenTrue
                , test1 "appMaybeHomomorphism" provenTrue
                , test1 "appMaybeInterchange" provenTrue
                ])
        , Leaf ("Monad",
                [
                  test1 "monadMaybeLeftId" provenTrue
                , test1 "monadMaybeRightId" provenTrue
                , test1 "monadMaybeAssociative" provenTrue
                  ])
        , Leaf ("Alternative",
                [
                  test1 "alternativeMaybeLeftId" provenTrue
                , test1 "alternativeMaybeRightId" provenTrue
                , test1 "alternativeMaybeAssociative" provenTrue
                  ])
        , Leaf ("MonadPlus",
                 [
                   test1 "monadPlusMaybeLeftAbsorb" provenTrue
                 , test1 "monadPlusMaybeRightAbsorb" provenTrue
                 ])
        ]
      , Node "List"
        [ Leaf ("Semigroup",
                 [test1 "semigroupListAssociative" provenTrue]
               )
        , Leaf ("Monoid",
                [
                  test1 "monoidListLeftId" provenTrue
                , test1 "monoidListRightId" provenTrue
                ])
        , Leaf ("Functor",
                [
                  test1 "funcListId" provenTrue
                , test1 "funcListCompose" provenTrue
                ])
        , Leaf ("Applicative Functor",
                [
                  test1 "appListId" provenTrue
                , test1 "appListCompose" provenTrue
                , test1 "appListHomomorphism" provenTrue
                , test1 "appListInterchange" provenTrue
                ])
        , Leaf ("Monad",
                [
                  test1 "monadListLeftId" provenTrue
                , test1 "monadListRightId" provenTrue
                , test1 "monadListAssociative" provenTrue
                  ])
        , Leaf ("Alternative",
                [
                  test1 "alternativeListLeftId" provenTrue
                , test1 "alternativeListRightId" provenTrue
                , test1 "alternativeListAssociative" provenTrue
                  ])
        , Leaf ("MonadPlus",
                 [
                   test1 "monadPlusListLeftAbsorb" provenTrue
                 , test1 "monadPlusListRightAbsorb" provenTrue
                 ])
        ]
      , Node "Tree"
        [ Leaf ("Functor",
                [
                  test1 "funcTreeId" provenTrue
                , test1 "funcTreeCompose" provenTrue
                ]) ]
      , Node "Reader"
        [ Leaf ("Functor",
                [
                  test1 "funcReaderId" provenTrue
                , test1 "funcReaderCompose" provenTrue
                ])
        , Leaf ("Applicative Functor",
                [
                  test1 "appReaderId" provenTrue
                , test1 "appReaderCompose" provenTrue
                , test1 "appReaderHomomorphism" provenTrue
                , test1 "appReaderInterchange" provenTrue
                ])
        , Leaf ("Monad",
                [
                  test1 "monadReaderLeftId" provenTrue
                , test1 "monadReaderRightId" provenTrue
                , test1 "monadReaderAssociative" provenTrue
                  ])]
      , Node "State"
        [ Leaf ("Functor",
                [
                  test1 "funcStateId" provenTrue
                , test1 "funcStateCompose" provenTrue
                ])
        , Leaf ("Applicative Functor",
                [
                  test1 "appStateId" provenTrue
                , test1 "appStateCompose" provenTrue
                , test1 "appStateHomomorphism" provenTrue
                , test1 "appStateInterchange" provenTrue
                ])
        , Leaf ("Monad",
                [
                  test1 "monadStateLeftId" provenTrue
                , test1 "monadStateRightId" provenTrue
                , test1 "monadStateAssociative" provenTrue
                  ])]
      , Node "Either"
        [ Leaf ("Functor",
                [
                  test1 "funcEitherId" provenTrue
                , test1 "funcEitherCompose" provenTrue
                ])
        , Leaf ("Applicative Functor",
                [
                  test1 "appEitherId" provenTrue
                , test1 "appEitherCompose" provenTrue
                , test1 "appEitherHomomorphism" provenTrue
                , test1 "appEitherInterchange" provenTrue
                ])
        , Leaf ("Monad",
                [
                  test1 "monadEitherLeftId" provenTrue
                , test1 "monadEitherRightId" provenTrue
                , test1 "monadEitherAssociative" provenTrue
                  ])
        , Leaf ("Semigroup",
                [
                  test1 "semigroupEitherAssociative" provenTrue
                  ])]
       , Node "Endofunction"
        [ Leaf ("Semigroup",
                [test1 "semigroupEndofunctionAssociative" provenTrue])
        , Leaf ("Monoid",
                [
                  test1 "monoidEndofunctionLeftId" provenTrue
                , test1 "monoidEndofunctionRightId" provenTrue
                ])]
        , Node "Naturals over addition"
          [ Leaf ("Semigroup",
                   [test1 "semigroupNatPlusAssociative" provenTrue])
          , Leaf ("Monoid",
                  [
                    test1 "monoidNatPlusLeftId" provenTrue
                  , test1 "monoidNatPlusRightId" provenTrue
                  ])
          ]
        , Node "Naturals over multiplication"
          [ Leaf ("Semigroup",
                [test1 "semigroupNatMulAssociative" provenTrue])
          , Leaf ("Monoid",
                [
                  test1 "monoidNatMulLeftId" provenTrue
                , test2 "monoidNatMulRightId" provenTrue
                ])]
        -- , Node "Functions (->)"
        --   [ Leaf ("Category",
        --         [
        --           test1 "categoryFunctionAssoc" provenTrue
        --         , test1 "categoryFunctionLeftId" provenTrue
        --         , test1 "categoryFunctionRightId" provenTrue
        --         ])
        --   , Leaf ("Arrow",
        --         [
        --           test1 "arrowFunctionId" provenTrue
        --         , test1 "arrowFunctionDistrib" provenTrue
        --         , test1 "arrowFunctionFirstCommut" provenTrue
        --         , test1 "arrowFunctionFirstDistrib" provenTrue
        --         , test1 "arrowFunctionProp1" provenTrue
        --         , test1 "arrowFunctionProp2" provenTrue
        --         , test1 "arrowFunctionProp3" provenTrue
        --         ])]
        ]
