{-# LANGUAGE NoImplicitPrelude #-}
module Induction1 where

import PhileasPrelude

class Eq a where
  (==) :: a -> a -> Bool

class Eq a => Ord a where
  (<=) :: a -> a -> Bool

instance Eq Ordering where
  EQ == EQ = True
  LT == LT = True
  GT == GT = True
  _ == _ = False

instance Ord Nat where
  Zero <= _ = True
  Suc a <= Suc b = a <= b
  _ <= _ = False

data Nat = Zero
  | Suc Nat

data Ordering = LT | EQ | GT

instance Eq Nat where
  Zero == Zero = True
  Suc a == Suc b = a == b
  _ == _ = False

add :: Nat -> Nat -> Nat
add Zero b = b
add (Suc a) b = Suc (add a b)

addZeroRight :: Nat -> Property
addZeroRight a = add a Zero ≡ a

addSucRight :: Nat -> Nat -> Property
addSucRight a b = add a (Suc b) ≡ Suc (add a b)

trueEq :: Nat -> Nat -> Property
trueEq a b = a == b ≡ True ⇒ a ≡ b

eqImpliesStructEq :: Nat -> Nat -> Property
eqImpliesStructEq a b = a == b ≡ True ⇒ a ≡ b

structEqImpliesEq :: Nat -> Nat -> Property
structEqImpliesEq a b = a ≡ b ⇒ a == b ≡ True

partialOrderAntisimmetry :: Nat -> Nat -> Property
partialOrderAntisimmetry a b = a <= b ≡ True ⇒ b <= a ≡ True ⇒ a == b ≡ True

partialOrderRefl :: Nat -> Property
partialOrderRefl a = a <= a ≡ True

partialOrderTransitivity :: Nat -> Nat -> Nat -> Property
partialOrderTransitivity a b c = a <= b ≡ True ⇒ b <= c ≡ True ⇒ a <= c ≡ True
