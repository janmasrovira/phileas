{-# LANGUAGE NoImplicitPrelude #-}
module Classes where

import PhileasPrelude

data Nat = Zero | Suc Nat
data Maybe a = Nothing | Just a
data Pair a b = Pair a b
data Tree a = Leaf a | Node (Tree a) (Tree a)
data State s a = State (s -> Pair s a)
data Reader r a = Reader (r -> a)
data Either l r = Left l | Right r
data Endofunction a = Endofunction (a -> a)
data NatPlus = NatPlus Nat
data NatMul = NatMul Nat

class Functor f where
  fmap :: (a -> b) -> f a -> f b

class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b

class Applicative f => Monad f where
  return :: a -> f a
  (>>=) :: f a -> (a -> f b) -> f b

class Applicative f => Alternative f where
  empty :: f a
  (<|>) :: f a -> f a -> f a

class (Alternative m, Monad m) => MonadPlus m where
  mzero :: m a
  msum :: m a -> m a -> m a

class Semigroup a where
  (<>) :: a -> a -> a

class Semigroup a => Monoid a where
  mempty :: a

class Category cat where
  -- | the identity morphism
  idCat :: cat a a
  -- | morphism composition
  compose :: cat b c -> cat a b -> cat a c
  (>>>) :: cat a b -> cat b c -> cat a c
  f >>> g = compose g f

class Category a => Arrow a where
    -- | Lift a function to an arrow.
    arr :: (b -> c) -> a b c

    -- | Send the first component of the input through the argument
    --   arrow, and copy the rest unchanged to the output.
    first :: a b c -> a (Pair b d) (Pair c d)
    first = (*** idCat)

    (***) :: a b c -> a b' c' -> a (Pair b b') (Pair c c')
    f *** g = first f >>> arr swap >>> first g >>> arr swap

instance Category (->) where
  idCat = id
  compose = (.)

instance Arrow (->) where
  arr = id
  (f *** g) (Pair x y) = Pair (f x) (g y)

instance Functor Maybe where
  fmap f Nothing = Nothing
  fmap f (Just x) = Just (f x)

instance Applicative Maybe where
  pure = Just
  a <*> b = case a of
    Just f -> fmap f b
    Nothing -> Nothing

instance Monad Maybe where
  return = Just
  x >>= f = case x of
    Nothing -> Nothing
    Just y -> f y

instance Alternative Maybe where
  empty = Nothing
  Nothing <|> r = r
  l       <|> _ = l

instance MonadPlus Maybe where
  mzero = empty
  msum = (<|>)

instance Functor [] where
  fmap f [] = []
  fmap f (x:xs) = f x : fmap f xs

-- instance Applicative [] where
--   pure x = [x]
--   [] <*> l = []
--   (f:fs) <*> l = fmap f l <> (fs <*> l)

instance Applicative [] where
  pure x = [x]
  fs <*> xs = [ f x | f <- fs, x <- xs ]

-- instance Monad [] where
--   return = pure
--   l >>= f = concatMap f l

instance Monad [] where
  return = pure
  xs >>= f = [ y | x <- xs, y <- f x ]

instance Alternative [] where
  empty = []
  (<|>) = (<>)

instance MonadPlus [] where
  mzero = empty
  msum = (<|>)

instance Semigroup [a] where
  [] <> b = b
  (a:as) <> b = a : (as <> b)

instance Monoid [a] where
  mempty = []

instance Functor Tree where
  fmap f (Leaf x) = Leaf (f x)
  fmap f (Node a b) = Node (fmap f a) (fmap f b)

instance Functor (Pair s) where
  fmap f (Pair s a) = Pair s (f a)

instance Functor (State s) where
  fmap f (State m) = State (fmap f . m)

instance Applicative (State s) where
  pure x = State (\s -> Pair s x)
  State mf <*> State mx =
    State (\s -> case mf s of
              Pair s' f -> case mx s' of
                Pair s'' x -> Pair s'' (f x))

instance Monad (State s) where
  return = pure
  State mx >>= mf =
    State (\s -> case mx s of
              Pair s' a -> case mf a of
                State mfa -> mfa s')

instance Functor (Reader r) where
  fmap f (Reader x) = Reader (f . x)

instance Applicative (Reader r) where
  pure = Reader . const
  Reader ab <*> Reader a = Reader (\r -> ab r (a r))

instance Monad (Reader r) where
  return = pure
  Reader ra >>= rafa = Reader
    (\r -> case rafa (ra r) of
           Reader x -> x r)

instance Functor (Either l) where
  fmap f (Left l) = Left l
  fmap f (Right r) = Right (f r)

instance Applicative (Either l) where
  pure = Right
  fab <*> fa = case fab of
    Left b -> Left b
    Right ab -> fmap ab fa

instance Monad (Either l) where
  return = pure
  ma >>= amb = case ma of
    Left l -> Left l
    Right a -> amb a

instance Semigroup (Either a b) where
    Left _ <> b = b
    a      <> _ = a

instance Semigroup (Endofunction a) where
  (Endofunction f) <> (Endofunction g) = Endofunction (f . g)

instance Monoid (Endofunction a) where
  mempty = Endofunction id

instance Semigroup NatPlus where
  NatPlus a <> NatPlus b = NatPlus (a `add` b)

instance Monoid NatPlus where
  mempty = NatPlus Zero

instance Semigroup NatMul where
  NatMul a <> NatMul b = NatMul (a `mul` b)

instance Monoid NatMul where
  mempty = NatMul (Suc Zero)

add :: Nat -> Nat -> Nat
add Zero b = b
add (Suc a) b = Suc (add a b)

mul :: Nat -> Nat -> Nat
mul Zero b = Zero
mul (Suc a) b = add (mul a b) b

id :: p -> p
id x = x

(.) :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
f . g  = \x -> f (g x)

($) :: (a -> b) -> a -> b
f $ x = f x

const :: p1 -> p2 -> p1
const x _ = x

swap :: Pair b a -> Pair a b
swap (Pair a b) = Pair b a

fst :: Pair a b -> a
fst (Pair a _) = a

concatMap :: (a1 -> [a2]) -> [a1] -> [a2]
concatMap f = concat . fmap f

concat :: [[a]] -> [a]
concat [] = []
concat (x:xs) = x <> concat xs

-- Pair
funcPairId :: Pair s x -> Property
funcPairId l = fmap id l ≡ l

funcPairCompose :: (a1 -> b) -> (a2 -> a1) -> Pair s a2 -> Property
funcPairCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

-- Maybe
funcMaybeId :: Maybe a -> Property
funcMaybeId l = fmap id l ≡ l

funcMaybeCompose :: (a1 -> b) -> (a2 -> a1) -> Maybe a2 -> Property
funcMaybeCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

appMaybeId :: Maybe b -> Property
appMaybeId x = pure id <*> x ≡ x

appMaybeCompose :: Maybe (b1 -> b2) -> Maybe (a -> b1) -> Maybe a -> Property
appMaybeCompose a b c = pure (.) <*> a <*> b <*> c ≡ a <*> (b <*> c)

appMaybeHomomorphism :: (a -> b) -> a -> Property
appMaybeHomomorphism f x = (pure :: x -> Maybe x) f <*> (pure x) ≡ pure (f x)

appMaybeInterchange :: Maybe (a -> b) -> a -> Property
appMaybeInterchange f x = f <*> pure x ≡ pure ($ x) <*> f

monadMaybeLeftId :: a -> (a -> Maybe a) -> Property
monadMaybeLeftId a f = return a >>= f ≡ f a

monadMaybeRightId :: Maybe a -> Property
monadMaybeRightId x = x >>= return ≡ x

monadMaybeAssociative :: Maybe a -> (a -> Maybe a) -> (a -> Maybe a) -> Property
monadMaybeAssociative a b c = a >>= (\x -> b x >>= c) ≡ (a >>= b) >>= c

alternativeMaybeRightId :: Maybe a -> Property
alternativeMaybeRightId a = empty <|> a ≡ a

alternativeMaybeLeftId :: Maybe a -> Property
alternativeMaybeLeftId a = a <|> empty ≡ a

alternativeMaybeAssociative :: Maybe a -> Maybe a -> Maybe a -> Property
alternativeMaybeAssociative a b c = a <|> (b <|> c) ≡ (a <|> b) <|> c

monadPlusMaybeLeftAbsorb :: (a -> Maybe a) -> Property
monadPlusMaybeLeftAbsorb f = mzero >>= f ≡ mzero

monadPlusMaybeRightAbsorb :: Maybe a -> Property
monadPlusMaybeRightAbsorb v = v >>= const mzero ≡ mzero

-- List
funcListId :: [b] -> Property
funcListId l = fmap id l ≡ l

funcListCompose :: (a1 -> b) -> (a2 -> a1) -> [a2] -> Property
funcListCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

appListId :: [b] -> Property
appListId x = pure id <*> x ≡ x

appListCompose :: [] (b1 -> b2) -> [] (a -> b1) -> [] a -> Property
appListCompose a b c = pure (.) <*> a <*> b <*> c ≡ a <*> (b <*> c)

appListHomomorphism :: (a -> b) -> a -> Property
appListHomomorphism f x = (pure :: x -> [] x) f <*> (pure x) ≡ pure (f x)

appListInterchange :: [] (a -> b) -> a -> Property
appListInterchange f x = f <*> pure x ≡ pure ($ x) <*> f

monadListLeftId :: a -> (a -> [] a) -> Property
monadListLeftId a f = return a >>= f ≡ f a

monadListRightId :: [] a -> Property
monadListRightId x = x >>= return ≡ x

monadListAssociative :: [] a -> (a -> [] a) -> (a -> [] a) -> Property
monadListAssociative a b c = a >>= (\x -> b x >>= c) ≡ (a >>= b) >>= c

semigroupListAssociative :: [a] -> [a] -> [a] -> Property
semigroupListAssociative a b c = (a <> b) <> c ≡ a <> (b <> c)

monoidListLeftId :: [a] -> Property
monoidListLeftId l = mempty <> l ≡ l

monoidListRightId :: [a] -> Property
monoidListRightId l = l <> mempty ≡ l

alternativeListRightId :: [a] -> Property
alternativeListRightId a = empty <|> a ≡ a

alternativeListLeftId :: [a] -> Property
alternativeListLeftId a = a <|> empty ≡ a

alternativeListAssociative :: [a] -> [a] -> [a] -> Property
alternativeListAssociative a b c = a <|> (b <|> c) ≡ (a <|> b) <|> c

monadPlusListLeftAbsorb :: (a -> [a]) -> Property
monadPlusListLeftAbsorb f = mzero >>= f ≡ mzero

monadPlusListRightAbsorb :: [a] -> Property
monadPlusListRightAbsorb v = v >>= const mzero ≡ mzero

-- Tree
funcTreeId :: Tree a -> Property
funcTreeId l = fmap id l ≡ l

funcTreeCompose :: (a1 -> b) -> (a2 -> a1) -> Tree a2 -> Property
funcTreeCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

funcStateId :: State s x -> Property
funcStateId l = fmap id l ≡ l

funcStateCompose :: (a1 -> b) -> (a2 -> a1) -> State s a2 -> Property
funcStateCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

-- State
appStateId :: State s b -> Property
appStateId x = pure id <*> x ≡ x

appStateCompose :: State s (b1 -> b2) -> State s (a -> b1) -> State s a -> Property
appStateCompose a b c = pure (.) <*> a <*> b <*> c ≡ a <*> (b <*> c)

appStateHomomorphism :: (a -> b) -> a -> Property
appStateHomomorphism f x = (pure :: x -> State s x) f <*> (pure x) ≡ pure (f x)

appStateInterchange :: State s (a -> b) -> a -> Property
appStateInterchange f x = f <*> pure x ≡ pure ($ x) <*> f

monadStateLeftId :: a -> (a -> State s a) -> Property
monadStateLeftId a f = return a >>= f ≡ f a

monadStateRightId :: State s a -> Property
monadStateRightId x = x >>= return ≡ x

monadStateAssociative :: State s a -> (a -> State s a) -> (a -> State s a) -> Property
monadStateAssociative a b c = a >>= (\x -> b x >>= c) ≡ (a >>= b) >>= c

-- Reader
funcReaderId :: Reader r x -> Property
funcReaderId l = fmap id l ≡ l

funcReaderCompose :: (a1 -> b) -> (a2 -> a1) -> Reader r a2 -> Property
funcReaderCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

appReaderId :: Reader r b -> Property
appReaderId x = pure id <*> x ≡ x

appReaderCompose :: Reader r (b1 -> b2) -> Reader r (a -> b1) -> Reader r a -> Property
appReaderCompose a b c = pure (.) <*> a <*> b <*> c ≡ a <*> (b <*> c)

appReaderHomomorphism :: (a -> b) -> a -> Property
appReaderHomomorphism f x = (pure :: x -> Reader r x) f <*> (pure x) ≡ pure (f x)

appReaderInterchange :: Reader r (a -> b) -> a -> Property
appReaderInterchange f x = f <*> pure x ≡ pure ($ x) <*> f

monadReaderLeftId :: a -> (a -> Reader r a) -> Property
monadReaderLeftId a f = return a >>= f ≡ f a

monadReaderRightId :: Reader r a -> Property
monadReaderRightId x = x >>= return ≡ x

monadReaderAssociative :: Reader r a -> (a -> Reader r a) -> (a -> Reader r a) -> Property
monadReaderAssociative a b c = a >>= (\x -> b x >>= c) ≡ (a >>= b) >>= c


-- Either
funcEitherId :: Either l x -> Property
funcEitherId l = fmap id l ≡ l

funcEitherCompose :: (a1 -> b) -> (a2 -> a1) -> Either l a2 -> Property
funcEitherCompose f g l = fmap f (fmap g l) ≡ fmap (f . g) l

appEitherId :: Either l b -> Property
appEitherId x = pure id <*> x ≡ x

appEitherCompose :: Either l (b1 -> b2) -> Either l (a -> b1) -> Either l a -> Property
appEitherCompose a b c = pure (.) <*> a <*> b <*> c ≡ a <*> (b <*> c)

appEitherHomomorphism :: (a -> b) -> a -> Property
appEitherHomomorphism f x = (pure :: x -> Either l x) f <*> (pure x) ≡ pure (f x)

appEitherInterchange :: Either l (a -> b) -> a -> Property
appEitherInterchange f x = f <*> pure x ≡ pure ($ x) <*> f

monadEitherLeftId :: a -> (a -> Either l a) -> Property
monadEitherLeftId a f = return a >>= f ≡ f a

monadEitherRightId :: Either l a -> Property
monadEitherRightId x = x >>= return ≡ x

monadEitherAssociative :: Either l a -> (a -> Either l a) -> (a -> Either l a) -> Property
monadEitherAssociative a b c = a >>= (\x -> b x >>= c) ≡ (a >>= b) >>= c

semigroupEitherAssociative :: Either a b -> Either a b -> Either a b -> Property
semigroupEitherAssociative a b c = (a <> b) <> c ≡ a <> (b <> c)

-- Endofunction
semigroupEndofunctionAssociative :: Endofunction a -> Endofunction a -> Endofunction a -> Property
semigroupEndofunctionAssociative a b c = (a <> b) <> c ≡ a <> (b <> c)

monoidEndofunctionLeftId :: Endofunction a -> Property
monoidEndofunctionLeftId l = mempty <> l ≡ l

monoidEndofunctionRightId :: Endofunction a -> Property
monoidEndofunctionRightId l = l <> mempty ≡ l

-- Nat Plus
semigroupNatPlusAssociative :: NatPlus -> NatPlus -> NatPlus -> Property
semigroupNatPlusAssociative a b c = (a <> b) <> c ≡ a <> (b <> c)

monoidNatPlusLeftId :: NatPlus -> Property
monoidNatPlusLeftId l = mempty <> l ≡ l

monoidNatPlusRightId :: NatPlus -> Property
monoidNatPlusRightId l = l <> mempty ≡ l

-- Nat Mul
semigroupNatMulAssociative :: NatMul -> NatMul -> NatMul -> Property
semigroupNatMulAssociative a b c = (a <> b) <> c ≡ a <> (b <> c)

monoidNatMulLeftId :: NatMul -> Property
monoidNatMulLeftId l = mempty <> l ≡ l

monoidNatMulRightId :: NatMul -> Property
monoidNatMulRightId l = l <> mempty ≡ l

-- Function
categoryFunctionLeftId :: (a -> b) -> Property
categoryFunctionLeftId f = compose idCat f ≡ f

categoryFunctionRightId :: (a -> b) -> Property
categoryFunctionRightId f = compose f idCat ≡ f

categoryFunctionAssoc :: (a -> b) -> (c -> a) -> (d -> c) -> Property
categoryFunctionAssoc f g h = f `compose` (g `compose` h) ≡ (f `compose` g) `compose` h

arrFun :: (a -> b) -> (a -> b)
arrFun = arr

arrowFunctionId :: Property
arrowFunctionId = arrFun id ≡ idCat

arrowFunctionDistrib :: (a -> b) -> (b -> c) -> Property
arrowFunctionDistrib f g = (arrFun :: (a -> b) -> (a -> b)) (f >>> g) ≡ arrFun f >>> arrFun g

arrowFunctionFirstCommut :: (b -> c) -> Property
arrowFunctionFirstCommut f = first (arrFun f) ≡ arrFun (first f)

arrowFunctionFirstDistrib :: (a -> b) -> (b -> c) -> Property
arrowFunctionFirstDistrib f g = first (f >>> g) ≡ first f >>> first g

arrowFunctionProp1 :: (b -> c) -> Property
arrowFunctionProp1 f = first f >>> arrFun fst ≡ arrFun fst >>> f

arrowFunctionProp2 :: (a -> b) -> (c -> d) -> Property
arrowFunctionProp2 f g = first f >>> arrFun (id *** g) ≡ arrFun (id *** g) >>> first f

arrowFunctionProp3 :: (b -> b) -> (Pair b d1 -> Pair b d2) -> Property
arrowFunctionProp3 f assoc = (first f) >>> arrFun assoc ≡ arrFun assoc >>> first f
