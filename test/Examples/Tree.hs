{-# LANGUAGE NoImplicitPrelude #-}
module Tree where

import PhileasPrelude

data Nat = Zero | Suc Nat
data Tree a = Leaf | Node a (Tree a) (Tree a)

max :: Nat -> Nat -> Nat
max Zero y = y
max (Suc x) (Suc y) = Suc (max x y)
max x Zero = x

height :: Tree a -> Nat
height Leaf = Zero
height (Node x l r) = Suc (max (height l) (height r))

mirror :: Tree a -> Tree a
mirror Leaf = Leaf
mirror (Node x l r) = Node x (mirror r) (mirror l)

heightMirror :: Tree a -> Property
heightMirror t = height t ≡ height (mirror t)

doubleMirror :: Tree a -> Property
doubleMirror t = mirror (mirror t) ≡ t
