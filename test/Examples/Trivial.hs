{-# LANGUAGE NoImplicitPrelude #-}
module Trivial where

import PhileasPrelude

data Maybe a = Just a | Nothing
data ℕ = Zero | Suc ℕ
data Unit = Unit

(&&) :: Bool -> Bool -> Bool
False && _ = False
True && b = b

(||) :: Bool -> Bool -> Bool
True || _ = True
False || b = b

not True = False
not False = True

unit :: Unit -> Property
unit u = u ≡ Unit

absurd1 :: Bool -> Property
absurd1 a = a ≡ not a

absurd2 :: Property
absurd2 = False ≡ True

andCommut :: Bool -> Bool -> Property
andCommut a b = a && b ≡ b && a

bottomCase :: Property
bottomCase = bottom ≡ case True of False -> False

andAssoc :: Bool -> Bool -> Bool -> Property
andAssoc a b c = a && (b && c) ≡ (a && b) && c

deMorgan :: Bool -> Bool -> Property
deMorgan a b = not (a && b) ≡ not b || not a

orId :: Bool -> Property
orId a = a || a ≡ a

andId :: Bool -> Property
andId a = a && a ≡ a

notNot :: Bool -> Property
notNot a = a ≡ not (not a)

fromMaybe :: p -> Maybe p -> p
fromMaybe a Nothing = a
fromMaybe _ (Just x) = x

may :: Maybe Bool -> Property
may x = fromMaybe False x ≡ False

add :: ℕ -> ℕ -> ℕ
add Zero b = b
add (Suc a) b = Suc (add a b)

mul Zero _ = Zero
mul (Suc a) b = add (mul a b) b

zero = Zero
one = Suc zero
two = Suc one
three = add one two

five :: Property
five = add three two ≡
       add two three

ten :: Property
ten = add three (add three (add three one)) ≡
      add (add two (add one three)) (add zero (add two two))

zeroLeft :: ℕ -> Property
zeroLeft x = add zero x ≡ x

mulZeroLeft :: ℕ -> Property
mulZeroLeft x = mul zero x ≡ zero

mulOneLeft :: ℕ -> Property
mulOneLeft x = mul one x ≡ x

snoc :: [a] -> a -> [a]
snoc [] a = [a]
snoc (x:xs) a = x : (snoc xs a)

tail :: [a] -> [a]
tail [] = bottom
tail (x:xs) = xs

tailSnoc :: a -> [a] -> a -> Property
tailSnoc y l x = tail (snoc (y:l) x) ≡ snoc (tail (y:l)) x
