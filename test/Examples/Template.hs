{-# LANGUAGE NoImplicitPrelude #-}
module Template where

import PhileasPrelude

----------------------
-- Data definitions --
----------------------

data Nat = Zero | Suc Nat

--------------------------
-- Function definitions --
--------------------------

(+) :: Nat -> Nat -> Nat
Zero + b = b
Suc a + b = Suc (a + b)

----------------------------
-- Properties definitions --
----------------------------

addRightId :: Nat -> Property
addRightId a = a + Zero ≡ a
