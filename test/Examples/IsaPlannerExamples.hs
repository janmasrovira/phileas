{-# LANGUAGE NoImplicitPrelude #-}
module IsaPlannerExamples where

import PhileasPrelude

data Nat = Zero | Suc Nat
data Maybe a = Just a | Nothing
data Pair a b = Pair a b
data Tree a = Leaf | Node a (Tree a) (Tree a)

class Eq a where
  (==) :: a -> a -> Bool

instance Eq Bool where
  False == False = True
  True == True = True
  _ == _ = False

initLast :: [a] -> Property
initLast xs = null xs ≡ False ⇒ init xs ++ (maybeToList (last xs)) ≡ xs

initSnoc :: [a] -> a -> Property
initSnoc xs x = init (xs ++ [x]) ≡ xs

countAppend :: [Nat] -> Nat -> [Nat] -> Property
countAppend l n m = count n l `add` count n m ≡ count n (l ++ m)

countLeq :: Nat -> [Nat] -> [Nat] -> Property
countLeq n l m = count n l <= count n (l ++ m) ≡ True

sucCount :: Nat -> [Nat] -> Property
sucCount n l = Suc Zero `add` count n l ≡ count n (n : l)

sucCountHyp :: Nat -> Nat -> [Nat] -> Property
sucCountHyp n x l = n ≡ x ⇒ Suc Zero `add` count n l ≡ count n (x : l)

minusPlus :: Nat -> Nat -> Property
minusPlus n m = n - (n `add` m) ≡ Zero

plusMinus :: Nat -> Nat -> Property
plusMinus n m = (n `add` m) - n ≡ m

plusMinusPlus :: Nat -> Nat -> Nat -> Property
plusMinusPlus k m n = (k `add` m) - (k `add` n) ≡ m - n

minusSelf :: Nat -> Property
minusSelf m = m - m ≡ Zero

dropZero :: [a] -> Property
dropZero xs = drop Zero xs ≡ xs

dropSuc :: Nat -> a -> [a] -> Property
dropSuc n x xs = drop (Suc n) (x : xs) ≡ drop n xs

filterAppend :: (a -> Bool) -> [a] -> [a] -> Property
filterAppend p xs ys = filter p (xs ++ ys) ≡ filter p xs ++ filter p ys

lengthInsert :: Nat -> [Nat] -> Property
lengthInsert x l = length (insert x l) ≡ Suc (length l)

appendNullHyp :: [a] -> [a] -> Property
appendNullHyp xs ys = ys ≡ [] ⇒ last (xs ++ ys) ≡ last xs

singleLastHyp :: [a] -> a -> Property
singleLastHyp xs x = xs ≡ [] ⇒ last (x : xs) ≡ Just x

lastNotNull :: a -> [a] -> Property
lastNotNull x xs = null xs ≡ False ⇒ last (x : xs) ≡ last xs

lastAppend :: [a] -> a -> Property
lastAppend xs x = last (xs ++ [x]) ≡ Just x

leqZero :: Nat -> Property
leqZero n = n <= Zero ≡ True ⇒ n ≡ Zero

lessSucAdd :: Nat -> Nat -> Property
lessSucAdd i m = i < (Suc (i `add` m)) ≡ True

leqAdd :: Nat -> Nat -> Property
leqAdd n m = n <= (n `add`m) ≡ True

memberAppend :: Nat -> [Nat] -> [Nat] -> Property
memberAppend x l t = x `member` l ≡ True ⇒ x `member` (l ++ t) ≡ True

-- Yes, this is a repeated theorem. This is IsaPlanner's fault. I keep it so the
-- numbers match.
memberAppend2 :: Nat -> [Nat] -> [Nat] -> Property
memberAppend2 x l t = x `member` l ≡ True ⇒ x `member` (l ++ t) ≡ True

memberSnoc :: Nat -> [Nat] -> Property
memberSnoc x l = x `member` (l ++ [x]) ≡ True

memberInsert :: Nat -> [Nat] -> Property
memberInsert x l = x `member` insert x l ≡ True

notMemberInsert :: Nat -> Nat -> [Nat] -> Property
notMemberInsert x y l = x == y ≡ False ⇒ (x `member` insert y l) ≡ x `member` l

-- another duplicate
memberInsert2 :: Nat -> [Nat] -> Property
memberInsert2 x l = x `member` insert x l ≡ True

dropWhileFalse :: [a] -> Property
dropWhileFalse xs = dropWhile (\x -> False) xs ≡ xs

takeWhileTrue :: [a] -> Property
takeWhileTrue xs = takeWhile (\x -> True) xs ≡ xs

countAdd :: Nat -> Nat -> [Nat] -> Property
countAdd n h t = count n [h] `add` count n t ≡ count n (h : t)

takeZero :: [a] -> Property
takeZero xs = take Zero xs ≡ []

takeSuc :: Nat -> a -> [a] -> Property
takeSuc n x xs = take (Suc n) (x : xs) ≡ x : take n xs

zipCons :: a -> [a] -> b -> [b] -> Property
zipCons x xs y ys = zip (x : xs) (y : ys) ≡ (Pair x y : zip xs ys)

zipNull :: [b] -> Property
zipNull ys = zip [] ys ≡ []

countSnoc :: Nat -> [Nat] -> Property
countSnoc n x = count n (x ++ [n]) ≡ Suc (count n x)

lastAppendNull :: [a] -> [a] -> Property
lastAppendNull xs ys = null ys ≡ False ⇒ last (xs ++ ys) ≡ last ys

lastAppendIf :: [a] -> [a] -> Property
lastAppendIf xs ys = last (xs ++ ys) ≡ (if null ys then last xs else last ys)

takeMap :: Nat -> (a1 -> a2) -> [a1] -> Property
takeMap n f xs = take n (map f xs) ≡ map f (take n xs)

takeZip :: Nat -> [a] -> [b] -> Property
takeZip n xs ys = take n (zip xs ys) ≡ zip (take n xs) (take n ys)

zipAppend :: [a] -> [a] -> [b] -> Property
zipAppend xs ys zs = zip (xs ++ ys) zs ≡ zip xs (take (length xs) zs) ++ zip ys (drop (length xs) zs)

zipConsCase :: a -> [a] -> [b] -> Property
zipConsCase x xs ys = zip (x : xs) ys ≡ (
  case ys of
    [] -> []
    y : ys -> Pair x y : zip xs ys)

lengthEqZip :: [a] -> [b] -> Property
lengthEqZip xs ys = length xs ≡ length ys ⇒ zip (reverse xs) (reverse ys) ≡ reverse (zip xs ys)

heightMirror :: Tree a -> Property
heightMirror a = height (mirror a) ≡ height a

initappend :: [a] -> [a] -> Property
initappend xs ys = init (xs ++ ys) ≡ (if null ys then init xs else xs ++ init ys)

initTakeLength :: [a] -> Property
initTakeLength xs = init xs ≡ take (length xs - Suc Zero) xs

countRev :: Nat -> [Nat] -> Property
countRev n l = count n l ≡ count n (reverse l)

countSort :: Nat -> [Nat] -> Property
countSort x l = count x l ≡ count x (insertSort l)

lastDropLength :: Nat -> [a] -> Property
lastDropLength n xs = n < length xs ≡ True ⇒ last (drop n xs) ≡ last xs

lessPlus :: Nat -> Nat -> Property
lessPlus i m = i < Suc (m + i) ≡ True

lengthFilter :: (a -> Bool) -> [a] -> Property
lengthFilter p xs = length (filter p xs) <= length xs ≡ True

lengthInit :: [a] -> Property
lengthInit xs = length (init xs) ≡ length xs - Suc Zero

lengthDelete :: Nat -> [Nat] -> Property
lengthDelete x l = length (delete x l) <= length l ≡ True

lengthDrop :: Nat -> [a] -> Property
lengthDrop n xs =  length (drop n xs) ≡ (length xs) - n

lengthSort :: [Nat] -> Property
lengthSort l = length (insertSort l) ≡ length l

leqPlus :: Nat -> Nat -> Property
leqPlus n m = n <= (m + n) ≡ True

leqSuc :: Nat -> Nat -> Property
leqSuc m n = m <= n ≡ True ⇒ m <= Suc n ≡ True

maxLeq :: Nat -> Nat -> Property
maxLeq a b = max a b ≡ a ⇒ b <= a ≡ True

maxLeq2 :: Nat -> Nat -> Property
maxLeq2 a b = max a b ≡ b ⇒ a <= b ≡ True

lessMemberInsert :: Nat -> Nat -> [Nat] -> Property
lessMemberInsert x y l = x < y ≡ True ⇒ x `member` insert y l ≡ x `member` l

minAssoc :: Nat -> Nat -> Nat -> Property
minAssoc a b c =  min (min a b) c ≡ min a (min b c)

minLeq :: Nat -> Nat -> Property
minLeq a b = min a b ≡ a ⇒ a <= b ≡ True

minLeq2 :: Nat -> Nat -> Property
minLeq2 a b = min a b ≡ b ⇒ b <= a ≡ True

revDropTake :: Nat -> [a] -> Property
revDropTake i xs = reverse (drop i xs) ≡ take ((length xs) - i) (reverse xs)

reverseFilter :: (a -> Bool) -> [a] -> Property
reverseFilter p xs = reverse (filter p xs) ≡ filter p (reverse xs)

reverseTakeDropLength :: Nat -> [a] -> Property
reverseTakeDropLength i xs = reverse (take i xs) ≡ drop ((length xs) - i) (reverse xs)

countNeqSnoc :: Nat -> Nat -> [Nat] -> Property
countNeqSnoc n h x = n == h ≡ False ⇒ count n (x ++ [h]) ≡ count n x

takeAppendMinusLength :: Nat -> [a] -> [a] -> Property
takeAppendMinusLength n xs ys = take n (xs ++ ys) ≡ take n xs ++ take (n - (length xs)) ys


-- FAILS

countPlusSnoc :: Nat -> [Nat] -> Nat -> Property
countPlusSnoc n t h = count n t + count n [h] ≡ count n (h : t)

memberDelete :: Nat -> [Nat] -> Property
memberDelete x l = x `member` delete x l ≡ False

takeWhileDropWhile :: (a -> Bool) -> [a] -> Property
takeWhileDropWhile p xs = takeWhile p xs ++ dropWhile p xs ≡ xs

takeDrop :: Nat -> [a] -> Property
takeDrop n xs = take n xs ++ drop n xs ≡ xs

plusMinusRight :: Nat -> Nat -> Property
plusMinusRight m n = (m + n) - n ≡ m

minusMinus :: Nat -> Nat -> Nat -> Property
minusMinus i j k = (i - j) - k ≡ i - (k - j)

minusPlusRight :: Nat -> Nat -> Nat -> Property
minusPlusRight i j k = (i - j) - k ≡ i - (j + k)

dropAppendMinusLength :: Nat -> [a] -> [a] -> Property
dropAppendMinusLength n xs ys = drop n (xs ++ ys) ≡ drop n xs ++ drop (n - (length xs)) ys

dropPlus :: Nat -> Nat -> [a] -> Property
dropPlus n m xs = drop n (drop m xs) ≡ drop (n + m) xs

dropMap :: Nat -> (a1 -> a2) -> [a1] -> Property
dropMap n f xs = drop n (map f xs) ≡ map f (drop n xs)

dropTake :: Nat -> Nat -> [a] -> Property
dropTake n m xs = drop n (take m xs) ≡ take (m - n) (drop n xs)

dropZip :: Nat -> [a] -> [b] -> Property
dropZip n xs ys =  drop n (zip xs ys) ≡ zip (drop n xs) (drop n ys)

maxAssoc :: Nat -> Nat -> Nat -> Property
maxAssoc a b c = max (max a b) c ≡ max a (max b c)

maxCommut :: Nat -> Nat -> Property
maxCommut a b =  max a b ≡ max b a

minCommut :: Nat -> Nat -> Property
minCommut a b = min a b ≡ min b a

insertSortStep :: [Nat] -> Nat -> Property
insertSortStep l x = sorted l ≡ True ⇒ sorted (insert x l) ≡ True

insertSortCorrect :: [Nat] -> Property
insertSortCorrect l = sorted (insertSort l) ≡ True

sucMinusSides :: Nat -> Nat -> Nat -> Property
sucMinusSides m n k = ((Suc m) - n) - (Suc k) ≡ (m - n) - k

takeDropTakePlus :: Nat -> Nat -> [a] -> Property
takeDropTakePlus n m xs = take n (drop m xs) ≡ drop m (take (n + m) xs)

zipAppend2 :: [a] -> [b] -> [b] -> Property
zipAppend2 xs ys zs = zip xs (ys ++ zs) ≡ zip (take (length ys) xs) ys ++ zip (drop (length ys) xs) zs

  -- functions
null :: [a] -> Bool
null [] = True
null _ = False

maybeToList (Just x) = [x]
maybeToList Nothing = []

filter f [] = []
filter f (x:xs)
  | f x = x : filter f xs
  | True = filter f xs

count :: Nat -> [Nat] -> Nat
count n = length . filter (==n)

add :: Nat -> Nat -> Nat
add Zero a    = a
add (Suc a) b = Suc (add a b)

max :: Nat -> Nat -> Nat
max Zero b = b
max (Suc a) (Suc b) = Suc (max a b)
max a Zero = a

min :: Nat -> Nat -> Nat
min (Suc a) (Suc b) = Suc (min a b)
min  _ _ = Zero

(+) :: Nat -> Nat -> Nat
(+) = add

(-) :: Nat -> Nat -> Nat
a - Zero = a
Suc a - Suc b = a - b
Zero - _ = Zero

map :: (t -> a) -> [t] -> [a]
map f []       = []
map f (x : xs) = (:) (f x) (map f xs)

init :: [a] -> [a]
init [] = []
init [x] = []
init (x:xs) = x : init xs

(.) :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
f . g = \x -> f (g x)

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile f [] = []
dropWhile f (x:xs)
  | f x = dropWhile f xs
  | True = x:xs

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile f [] = []
takeWhile f (x:xs)
  | f x = x : takeWhile f xs
  | True = []

const :: p1 -> p2 -> p1
const x _ = x

foldr :: (t1 -> t2 -> t2) -> t2 -> [t1] -> t2
foldr f ini []     = ini
foldr f ini (x:xs) = f x (foldr f ini xs)

sum :: [Nat] -> Nat
sum = foldr add Zero

take :: Nat -> [a] -> [a]
take (Suc n) (x:xs) = x : take n xs
take _ _            = []

length :: [a] -> Nat
length []     = Zero
length (_:xs) = Suc (length xs)

id :: p -> p
id x = x

reverse :: [a] -> [a]
reverse []     = []
reverse (x:xs) = reverse xs ++ [x]

drop :: Nat -> [a] -> [a]
drop (Suc n) (x:xs) = drop n xs
drop _ l = l

reverse' :: [a] -> [a]
reverse' = go []
  where go ac []     = ac
        go ac (x:xs) = go (x:ac) xs

(++) :: [a] -> [a] -> [a]
[] ++ l = l
(a:as) ++ b = a : (as ++ b)

snoc :: t -> [t] -> [t]
snoc x []     = [x]
snoc x (y:xs) = y : snoc x xs

member :: Eq t => t -> [t] -> Bool
member x [] = False
member x (y:ys)
  | x == y = True
  | True = member x ys

delete :: Eq a => a -> [a] -> [a]
delete n [] = []
delete n (a:as)
  | n == a = delete n as
  | True = n : delete n as

last :: [a] -> Maybe a
last []     = Nothing
last [x]    = Just x
last (x:xs) = last xs

replicate :: Nat -> a -> [a]
replicate Zero _    = []
replicate (Suc n) x = x : replicate n x

insert :: Nat -> [Nat] -> [Nat]
insert a [] = [a]
insert a (x:xs)
  | a <= x = a:x:xs
insert a (x:xs) = x : insert a xs

sorted :: [Nat] -> Bool
sorted [] = True
sorted [x] = True
sorted (x:y:ys)
  | x <= y = sorted (y:ys)
  | True = False

insertSort :: [Nat] -> [Nat]
insertSort [] = []
insertSort (x:xs) = insert x (insertSort xs)

(<=) :: Nat -> Nat -> Bool
Zero <= _ = True
Suc a <= Suc b = a <= b
_ <= _ = False

(<) :: Nat -> Nat -> Bool
a < b = Suc a <= b

zip :: [a] -> [b] -> [Pair a b]
zip (x:xs) (y:ys) = Pair x y : zip xs ys
zip _ _ = []

instance Eq Nat where
  Zero == Zero = True
  Suc a == Suc b = a == b
  _ == _ = False

height :: Tree a -> Nat
height Leaf = Zero
height (Node x l r) = Suc (max (height l) (height r))

mirror :: Tree a -> Tree a
mirror Leaf = Leaf
mirror (Node x l r) = Node x (mirror r) (mirror l)

