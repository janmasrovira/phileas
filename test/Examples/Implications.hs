{-# LANGUAGE NoImplicitPrelude #-}
module Implications where

import PhileasPrelude

data Product a b = Prod a b

transitivity :: a -> a -> a -> Property
transitivity a b c = a ≡ b ⇒ b ≡ c ⇒ a ≡ c

symmetry :: a -> a -> Property
symmetry a b = a ≡ b ⇒ b ≡ a

prodElim :: a -> b -> a -> b -> Property
prodElim a b c d = Prod a b ≡ Prod c d ⇒ a ≡ c

purity :: (b -> a) -> b -> b -> Property
purity f x y = x ≡ y ⇒ f x ≡ f y
