{-# LANGUAGE NoImplicitPrelude #-}
module Induction1 where

import PhileasPrelude

data Nat =
  Zero
  | Suc Nat

data Maybe a = Just a | Nothing

class Eq a where
  (==) :: a -> a -> Bool

instance Eq Bool where
  False == False = True
  True == True = True
  _ == _ = False

add Zero a    = a
add (Suc a) b = Suc (add a b)

sumLength :: [a] -> Property
sumLength l = sum (map (const (Suc Zero)) l) ≡ length l

mapConst :: a -> p2 -> Property
mapConst x y = map (const x) ≡ map (const x) . map (const y)

lengthMap :: (t -> a) -> [t] -> Property
lengthMap f l = length l ≡ length (map f l)

takeMap :: Nat -> [a1] -> (a1 -> a2) -> Property
takeMap n l f = take n (map f l) ≡ map f (take n l)

memberSnoc :: Bool -> [Bool] -> Property
memberSnoc x xs = member x (snoc x xs) ≡ True

lengthConcat :: [a] -> [a] -> Property
lengthConcat a b = length a `add` length b ≡ length (a ++ b)

lastCons :: a -> a -> [a] -> Property
lastCons a b l = last (b:l) ≡ last (a:b:l)

lengthReplicate :: a -> Nat -> Property
lengthReplicate x k = length (replicate k x) ≡ k

lastSnoc :: a -> [a] -> Property
lastSnoc x l = last (snoc x l) ≡ Just x

lengthReverse :: [a] -> Property
lengthReverse l = length l ≡ length (reverse l)

reverseTail :: Property
reverseTail = reverse ≡ reverse'

reverseReverse :: Property
reverseReverse = reverse . reverse ≡ id

memberInsert :: Nat -> [Nat] -> Property
memberInsert x l = member x (insert x l) ≡ True

reverseConcat :: [a] -> [a] -> Property
reverseConcat a b = reverse (a ++ b) ≡ reverse b ++ reverse a

-- functions
map f []       = []
map f (x : xs) = (:) (f x) (map f xs)

(.) :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
f . g = \x -> f (g x)

const :: p1 -> p2 -> p1
const x _ = x

foldr f ini []     = ini
foldr f ini (x:xs) = f x (foldr f ini xs)

sum :: [Nat] -> Nat
sum = foldr add Zero

take :: Nat -> [a] -> [a]
take (Suc n) (x:xs) = x : take n xs
take _ _            = []

length []     = Zero
length (_:xs) = Suc (length xs)

id :: p -> p
id x = x

reverse :: [a] -> [a]
reverse []     = []
reverse (x:xs) = reverse xs ++ [x]

reverse' :: [a] -> [a]
reverse' = go []
  where go ac []     = ac
        go ac (x:xs) = go (x:ac) xs

(++) :: [a] -> [a] -> [a]
[] ++ l = l
(a:as) ++ b = a : (as ++ b)

snoc :: t -> [t] -> [t]
snoc x []     = [x]
snoc x (y:xs) = y : snoc x xs

member x [] = False
member x (y:ys)
  | x == y = True
member x (y:ys) = member x ys

last :: [a] -> Maybe a
last []     = Nothing
last [x]    = Just x
last (x:xs) = last xs

replicate :: Nat -> a -> [a]
replicate Zero _    = []
replicate (Suc n) x = x : replicate n x

insert a [] = [a]
insert a (x:xs)
  | a <= x = a:x:xs
insert a (x:xs) = x : insert a xs

(<=) :: Nat -> Nat -> Bool
Zero <= _ = True
Suc a <= Suc b = a <= b
_ <= _ = False

instance Eq Nat where
  Zero == Zero = True
  Suc a == Suc b = a == b
  _ == _ = False
