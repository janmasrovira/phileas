{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Example where

import PhileasPrelude

-- Some basic inductive datatypes

data Nat =
  Zero
  | Succ Nat

data Tree a = Leaf
  | Node (Tree a) a (Tree a)

-- Standard type-classes

class Eq a where
  (==) :: a -> a -> Bool

class Eq a => Ord a where
  (<=) :: a -> a -> Bool
  max, min :: a -> a -> a

class Num a where
  (+), (-), (*), (^) :: a -> a -> a

-- Purely polymorphic functions

id :: a -> a
id x = x

const :: a -> b -> a
const x y = x

-- Boolean functions

not :: Bool -> Bool
not True = False
not False = True

(&&) :: Bool -> Bool -> Bool
True && True = True
_ && _ = False

(||) :: Bool -> Bool -> Bool
False || False = False
_ || _ = True

otherwise :: Bool
otherwise = True
-- Natural number functions

instance Eq Nat where
  Zero == Zero = True
  Succ x == Succ y = x == y
  _ == _ = False

instance Ord Nat where
  Zero <= y = True
  Succ x <= Zero = False
  Succ x <= Succ y = x <= y

  max Zero y = y
  -- TODO: On swapping next two lines, zeno starts producing error -
  -- zeno: Zeno representation for variable not found: void#
  -- Same happens for min, take, drop. Investigate further.
  max (Succ x) (Succ y) = Succ (max x y)
  max x Zero = x

  min Zero y = Zero
  min (Succ x) (Succ y) = Succ (min x y)
  min x Zero = Zero

instance Num Nat where
  Zero + y = y
  Succ x + y = Succ (x + y)

  x - Zero = x
  Zero - y = Zero
  Succ x - Succ y = x - y

  Zero * y = Zero
  Succ x * y = y + (x * y)

  x ^ Zero = Succ Zero
  x ^ Succ y = x * (x ^ y)

one :: Nat
one = Succ Zero


-- List functions

instance Eq a => Eq [a] where
  [] == [] = True
  [] == (_:_) = False
  (_:_) == [] = False
  (x:xs) == (y:ys) = (x == y) && (xs == ys)

length :: [a] -> Nat
length [] = Zero
length (x:xs) = Succ (length xs)

(++) :: [a] -> [a] -> [a]
[] ++ ys = ys
(x:xs) ++ ys = x : (xs ++ ys)

reverse :: [a] -> [a]
reverse [] = []
reverse (x:xs) = reverse xs ++ [x]

elem :: Eq a => a -> [a] -> Bool
elem _ [] = False
elem n (x:xs)
  | n == x = True
elem n (x:xs) = elem n xs

filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x:xs) = if p x then x : xs' else xs'
  where xs' = filter p xs

take :: Nat -> [a] -> [a]
take Zero _ = []
take (Succ x) (y:ys) = y : (take x ys)
take _ [] = []

drop :: Nat -> [a] -> [a]
drop Zero xs = xs
drop (Succ x) (_:xs) = drop x xs
drop _ [] = []

count :: Nat -> [Nat] -> Nat
count x [] = Zero
count x (y:ys)
  | x == y = Succ (count x ys)
count x (y:ys) = count x ys

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ [] = []
takeWhile p (x:xs)
  | p x = x : (takeWhile p xs)
takeWhile p (x:xs) = []

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ [] = []
dropWhile p (x:xs)
  | p x = dropWhile p xs
dropWhile p (x:xs) = x:xs

delete :: Nat -> [Nat] -> [Nat]
delete _ [] = []
delete n (x:xs)
  | n == x = delete n xs
delete n (x:xs) = x : delete n xs

map :: (a -> b) -> [a] -> [b]
map f [] = []
map f (x:xs) = f x : map f xs

insert :: Nat -> [Nat] -> [Nat]
insert n [] = [n]
insert n (x:xs)
  | n <= x = n:x:xs
insert n (x:xs) = x : insert n xs

insertsort :: [Nat] -> [Nat]
insertsort [] = []
insertsort (x:xs) = insert x (insertsort xs)

sorted :: [Nat] -> Bool
sorted [] = True
sorted [x] = True
sorted (x:y:ys)
  | x <= y = sorted (y:ys)
sorted (x:y:ys) = False


-- Tree functions

height :: Tree a -> Nat
height Leaf = Zero
height (Node l x r) = Succ (max (height l) (height r))

mirror :: Tree a -> Tree a
mirror Leaf = Leaf
mirror (Node l x r) = Node (mirror r) x (mirror l)

-- Natural number properties
propAddRightIdent :: Nat -> Property
propAddRightIdent x = x + Zero ≡ x

propAddAssoc :: Nat -> Nat -> Nat -> Property
propAddAssoc x y z = x + (y + z) ≡ (x + y) + z

propAddCommu :: Nat -> Nat -> Property
propAddCommu x y = x + y ≡ y + x

propMulAddDist :: Nat -> Nat -> Nat -> Property
propMulAddDist x y z = x * (y + z) ≡ (x * y) + (x * z)

propMulRightIdent :: Nat -> Property
propMulRightIdent x = x * one ≡ x

propMulCommu :: Nat -> Nat -> Property
propMulCommu x y = x * y ≡ y * x

propMulAssoc :: Nat -> Nat -> Nat -> Property
propMulAssoc x y z = x * (y * z) ≡ (x * y) * z

propLeqRef :: Nat -> Property
propLeqRef x = (x <= x ≡ True)

propLeqTrn :: Nat -> Nat -> Nat -> Property
propLeqTrn x y z = x <= y ≡ True ⇒ y <= z ≡ True ⇒ x <= z ≡ True

propLeqTotal :: Nat -> Nat -> Property
propLeqTotal x y = x <= y ≡ False ⇒ y <= x ≡ True

propMaxAssoc :: Nat -> Nat -> Nat -> Property
propMaxAssoc x y z = max (max x y) z ≡ max x (max y z)

propMinAssoc :: Nat -> Nat -> Nat -> Property
propMinAssoc x y z = min (min x y) z ≡ min x (min y z)

-- List properties

propReverseTwice :: [a] -> Property
propReverseTwice xs = reverse (reverse xs) ≡ xs

propReverseAppend :: [a] -> [a] -> Property
propReverseAppend xs ys = (reverse (xs ++ ys) ≡ reverse ys ++ reverse xs)

propAppendAssoc :: [a] -> [a] -> [a] -> Property
propAppendAssoc xs ys zs = (xs ++ ys) ++ zs ≡ xs ++ (ys ++ zs)

propLengthSnoc :: [a] -> a -> Property
propLengthSnoc xs x = length (xs ++ [x]) ≡ one + length xs

propLengthReverse :: [a] -> Property
propLengthReverse xs = length (reverse xs) ≡ length xs

propLengthFilter :: [a] -> (a -> Bool) -> Property
propLengthFilter xs p = length (filter p xs) <= length xs ≡ True

propLengthDelete :: Nat -> [Nat] -> Property
propLengthDelete n xs = length (delete n xs) <= length xs ≡ True

propLengthDrop :: Nat -> [a] -> Property
propLengthDrop n xs = length (drop n xs) ≡ length xs - n

propLengthInsertsort :: [Nat] -> Property
propLengthInsertsort xs = length (insertsort xs) ≡ length xs

propElemAppendLeft :: Nat -> [Nat] -> [Nat] -> Property
propElemAppendLeft n xs ys = elem n xs ≡ True ⇒ elem n (xs ++ ys) ≡ True

propElemAppendRight :: Nat -> [Nat] -> [Nat] -> Property
propElemAppendRight n xs ys = elem n ys ≡ True ⇒ (elem n (xs ++ ys)) ≡ True

propElemInsert :: Nat -> [Nat] -> Property
propElemInsert n xs = elem n (insert n xs) ≡ True

propElemInsertEq :: Nat -> Nat -> [Nat] -> Property
propElemInsertEq x y xs = x == y ≡ True ⇒ elem x (insert y xs) ≡ True

propTakeDrop1 :: Nat -> [a] -> p -> Property
propTakeDrop1 n xs ys = take n xs ++ drop n xs ≡ xs

propTakeDrop2 :: Nat -> Nat -> [a] -> Property
propTakeDrop2 n m xs = drop n (take m xs) ≡ take (m - n) (drop n xs)

propDropMap :: (a -> b) -> Nat -> [a] -> Property
propDropMap f n xs = drop n (map f xs) ≡ map f (drop n xs)

propTakeMap :: (a -> b) -> Nat -> [a] -> Property
propTakeMap f n xs = take n (map f xs) ≡ map f (take n xs)

propDropDrop :: Nat -> Nat -> [a] -> Property
propDropDrop n m xs = drop n (drop m xs) ≡ drop (n + m) xs

propFilterApp :: (a -> Bool) -> [a] -> [a] -> Property
propFilterApp p xs ys = filter p (xs ++ ys) ≡ filter p xs ++ filter p ys

propCountAddApp :: Nat -> [Nat] -> [Nat] -> Property
propCountAddApp n xs ys = count n xs + count n ys ≡ count n (xs ++ ys)

propCountLeqApp :: Nat -> [Nat] -> [Nat] -> Property
propCountLeqApp n xs ys = count n xs <= count n (xs ++ ys) ≡ True

propCountSnoc :: Nat -> [Nat] -> Property
propCountSnoc n xs = Succ (count n xs) ≡ count n (xs ++ [n])

propCountReverse :: Nat -> [Nat] -> Property
propCountReverse n xs = count n (reverse xs) ≡ count n xs

propCountInsertsort :: Nat -> [Nat] -> Property
propCountInsertsort n xs = count n (insertsort xs) ≡ count n xs

propInsertSorts :: Nat -> [Nat] -> Property
propInsertSorts x xs = sorted xs ≡ True ⇒ sorted (insert x xs) ≡ True

propInsertsortSorts :: [Nat] -> Property
propInsertsortSorts xs = sorted (insertsort xs) ≡ True

propInsertsortIdem :: [Nat] -> Property
propInsertsortIdem xs = insertsort (insertsort xs) ≡ insertsort xs

-- Tree properties
-- Grounding the polymorphic type to Bool means these are solved more quickly

propHeightMirror :: Tree a -> Property
propHeightMirror t = height t ≡ height (mirror t)

propMirrorTwice :: Tree a -> Property
propMirrorTwice t = mirror (mirror t) ≡ t
