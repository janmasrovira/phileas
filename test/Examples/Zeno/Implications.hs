{-# LANGUAGE NoImplicitPrelude #-}
module Implications where

import Zeno

data Product a b = Prod a b

transitivity :: a -> a -> a -> Prop
transitivity a b c = given (a :=: b) (given  (b :=: c) (prove $ a :=: c))

symmetry :: a -> a -> Prop
symmetry a b = given (a :=: b) (prove $ b :=: a)

prodElim :: a -> b -> a -> b -> Prop
prodElim a b c d = given (Prod a b :=: Prod c d) (prove $ a :=: c)

purity :: (b -> a) -> b -> b -> Prop
purity f x y = given (x :=: y) (prove $ f x :=: f y)
