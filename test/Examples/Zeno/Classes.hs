{-# LANGUAGE NoImplicitPrelude #-}
module Classes where

import Zeno
data Nat = Zero | Suc Nat
data Maybe a = Nothing | Just a
data Pair a b = Pair a b
data Tree a = Leaf a | Node (Tree a) (Tree a)
data State s a = State (s -> Pair s a)
data Reader r a = Reader (r -> a)
data Either l r = Left l | Right r
data Endofunction a = Endofunction (a -> a)

data NatPlus = NatPlus Nat
data NatMul = NatMul Nat

add :: Nat -> Nat -> Nat
add Zero b = b
add (Suc a) b = Suc (add a b)

mul :: Nat -> Nat -> Nat
mul Zero b = Zero
mul (Suc a) b = add (mul a b) b

id :: p -> p
id x = x

(.) :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
f . g  = \x -> f (g x)

const :: p1 -> p2 -> p1
const x _ = x

swap :: Pair b a -> Pair a b
swap (Pair a b) = Pair b a

fst :: Pair a b -> a
fst (Pair a _) = a

class Functor f where
  fmap :: (a -> b) -> f a -> f b

class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b

class Applicative f => Monad f where
  return :: a -> f a
  (>>=) :: f a -> (a -> f a) -> f a

class Applicative f => Alternative f where
  empty :: f a
  (<|>) :: f a -> f a -> f a

class (Alternative m, Monad m) => MonadPlus m where
  mzero :: m a
  msum :: m a -> m a -> m a

class Semigroup a where
  (<>) :: a -> a -> a

class Semigroup a => Monoid a where
  mempty :: a

class Category cat where
  -- | the identity morphism
  idCat :: cat a a
  -- | morphism composition
  compose :: cat b c -> cat a b -> cat a c
  (>>>) :: cat a b -> cat b c -> cat a c
  f >>> g = compose g f

class Category a => Arrow a where
    -- | Lift a function to an arrow.
    arr :: (b -> c) -> a b c

    -- | Send the first component of the input through the argument
    --   arrow, and copy the rest unchanged to the output.
    first :: a b c -> a (Pair b d) (Pair c d)
    first = (*** idCat)

    (***) :: a b c -> a b' c' -> a (Pair b b') (Pair c c')
    f *** g = first f >>> arr swap >>> first g >>> arr swap

-- NOTE Zeno crashes!
-- instance Category (->) where
--   idCat = id
--   compose = (.)

-- instance Arrow (->) where
--   arr = id
--   (f *** g) (Pair x y) = Pair (f x) (g y)

instance Functor Maybe where
  fmap f Nothing = Nothing
  fmap f (Just x) = Just (f x)

instance Applicative Maybe where
  pure = Just
  a <*> b = case a of
    Just f -> fmap f b
    Nothing -> Nothing

instance Monad Maybe where
  return = Just
  x >>= f = case x of
    Nothing -> Nothing
    Just y -> f y

instance Alternative Maybe where
  empty = Nothing
  Nothing <|> r = r
  l       <|> _ = l

-- NOTE Zeno crashes
-- instance MonadPlus Maybe where
--   mzero = empty
--   msum = (<|>)

instance Functor [] where
  fmap f [] = []
  fmap f (x:xs) = f x : fmap f xs

instance Applicative [] where
  pure x = [x]
  fs <*> xs = [ f x | f <- fs, x <- xs ]

-- NOTE Zeno crashes
-- instance Monad [] where
--   return = pure
--   xs >>= f = [ y | x <- xs, y <- f x ]

instance Semigroup [a] where
  [] <> b = b
  (a:as) <> b = a : (as <> b)

instance Alternative [] where
  empty = []
  (<|>) = (<>)

-- NOTE zeno crashes for Monad instance
-- instance MonadPlus [] where
--   mzero = empty
--   msum = (<|>)

instance Monoid [a] where
  mempty = []

instance Functor Tree where
  fmap f (Leaf x) = Leaf (f x)
  fmap f (Node a b) = Node (fmap f a) (fmap f b)

instance Functor (Pair s) where
  fmap f (Pair s a) = Pair s (f a)

instance Functor (State s) where
  fmap f (State m) = State (fmap f . m)

instance Applicative (State s) where
  pure x = State (\s -> Pair s x)
  State mf <*> State mx =
    State (\s -> case mf s of
              Pair s' f -> case mx s' of
                Pair s'' x -> Pair s'' (f x))

-- NOTE zeno crashes
-- instance Monad (State s) where
--   return = pure
--   State mx >>= mf =
--     State (\s -> case mx s of
--               Pair s' a -> case mf a of
--                 State mfa -> mfa s')

instance Functor (Reader r) where
  fmap f (Reader x) = Reader (f . x)

instance Applicative (Reader r) where
  pure = Reader . const
  Reader ab <*> Reader a = Reader (\r -> ab r (a r))

instance Monad (Reader r) where
  return = pure
  Reader ra >>= rafa = Reader
    (\r -> case rafa (ra r) of
           Reader x -> x r)

instance Functor (Either l) where
  fmap f (Left l) = Left l
  fmap f (Right r) = Right (f r)

instance Applicative (Either l) where
  pure = Right
  fab <*> fa = case fab of
    Left b -> Left b
    Right ab -> fmap ab fa

instance Monad (Either l) where
  return = pure
  ma >>= amb = case ma of
    Left l -> Left l
    Right a -> amb a

instance Semigroup (Endofunction a) where
  (Endofunction f) <> (Endofunction g) = Endofunction (f . g)

instance Monoid (Endofunction a) where
  mempty = Endofunction id

instance Semigroup NatPlus where
  NatPlus a <> NatPlus b = NatPlus (a `add` b)

instance Monoid NatPlus where
  mempty = NatPlus Zero

instance Semigroup NatMul where
  NatMul a <> NatMul b = NatMul (a `mul` b)

instance Monoid NatMul where
  mempty = NatMul (Suc Zero)

-- Pair
-- funcPairId :: Pair s x -> Prop
-- funcPairId l = prove (fmap id l :=: l)

-- funcPairCompose :: (a1 -> b) -> (a2 -> a1) -> Pair s a2 -> Prop
-- funcPairCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- Maybe
-- funcMaybeId :: Maybe a -> Prop
-- funcMaybeId l = prove (fmap id l :=: l)

-- funcMaybeCompose :: (a1 -> b) -> (a2 -> a1) -> Maybe a2 -> Prop
-- funcMaybeCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- appMaybeId :: Maybe b -> Prop
-- appMaybeId x = prove (pure id <*> x :=: x)

-- appMaybeCompose :: Maybe (b1 -> b2) -> Maybe (a -> b1) -> Maybe a -> Prop
-- appMaybeCompose a b c = prove (pure (.) <*> a <*> b <*> c :=: a <*> (b <*> c))

-- appMaybeHomomorphism :: (a -> b) -> a -> Prop
-- appMaybeHomomorphism f x = prove ((pure :: x -> Maybe x) f <*> (pure x) :=: pure (f x))

-- appMaybeInterchange :: Maybe (a -> b) -> a -> Prop
-- appMaybeInterchange f x = prove (f <*> pure x :=: pure ($ x) <*> f)

-- monadMaybeLeftId :: a -> (a -> Maybe a) -> Prop
-- monadMaybeLeftId a f = prove (return a >>= f :=: f a)

-- monadMaybeRightId :: Maybe a -> Prop
-- monadMaybeRightId x = prove (x >>= return :=: x)

-- monadMaybeAssociative :: Maybe a -> (a -> Maybe a) -> (a -> Maybe a) -> Prop
-- monadMaybeAssociative a b c = prove (a >>= (\x -> b x >>= c) :=: (a >>= b) >>= c)

-- alternativeMaybeRightId :: Maybe a -> Prop
-- alternativeMaybeRightId a = prove (empty <|> a :=: a)

-- alternativeMaybeLeftId :: Maybe a -> Prop
-- alternativeMaybeLeftId a = prove (a <|> empty :=: a)

-- alternativeMaybeAssociative :: Maybe a -> Maybe a -> Maybe a -> Prop
-- alternativeMaybeAssociative a b c = prove (a <|> (b <|> c) :=: (a <|> b) <|> c)

-- monadPlusMaybeLeftAbsorb :: (a -> Maybe a) -> Prop
-- monadPlusMaybeLeftAbsorb f = prove (mzero >>= f :=: mzero)

-- monadPlusMaybeRightAbsorb :: Maybe a -> Prop
-- monadPlusMaybeRightAbsorb v = prove (v >>= const mzero :=: mzero)

-- -- List
-- funcListId :: [b] -> Prop
-- funcListId l = prove (fmap id l :=: l)

-- funcListCompose :: (a1 -> b) -> (a2 -> a1) -> [a2] -> Prop
-- funcListCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- appListId :: [b] -> Prop
-- appListId x = prove (pure id <*> x :=: x)

-- appListCompose :: [] (b1 -> b2) -> [] (a -> b1) -> [] a -> Prop
-- appListCompose a b c = prove (pure (.) <*> a <*> b <*> c :=: a <*> (b <*> c))

-- appListHomomorphism :: (a -> b) -> a -> Prop
-- appListHomomorphism f x = prove ((pure :: x -> [] x) f <*> (pure x) :=: pure (f x))

-- appListInterchange :: [] (a -> b) -> a -> Prop
-- appListInterchange f x = prove (f <*> pure x :=: pure ($ x) <*> f)

-- monadListLeftId :: a -> (a -> [] a) -> Prop
-- monadListLeftId a f = prove (return a >>= f :=: f a)

-- monadListRightId :: [] a -> Prop
-- monadListRightId x = prove (x >>= return :=: x)

-- monadListAssociative :: [] a -> (a -> [] a) -> (a -> [] a) -> Prop
-- monadListAssociative a b c = prove (a >>= (\x -> b x >>= c) :=: (a >>= b) >>= c)

-- semigroupListAssociative :: [a] -> [a] -> [a] -> Prop
-- semigroupListAssociative a b c = prove ((a <> b) <> c :=: a <> (b <> c))

-- monoidListLeftId :: [a] -> Prop
-- monoidListLeftId l = prove (mempty <> l :=: l)

-- monoidListRightId :: [a] -> Prop
-- monoidListRightId l = prove (l <> mempty :=: l)

-- alternativeListRightId :: [a] -> Prop
-- alternativeListRightId a = prove (empty <|> a :=: a)

-- alternativeListLeftId :: [a] -> Prop
-- alternativeListLeftId a = prove (a <|> empty :=: a)

-- alternativeListAssociative :: [a] -> [a] -> [a] -> Prop
-- alternativeListAssociative a b c = prove (a <|> (b <|> c) :=: (a <|> b) <|> c)

-- monadPlusListLeftAbsorb :: (a -> [a]) -> Prop
-- monadPlusListLeftAbsorb f = prove (mzero >>= f :=: mzero)

-- monadPlusListRightAbsorb :: [a] -> Prop
-- monadPlusListRightAbsorb v = prove (v >>= const mzero :=: mzero)

-- -- Tree
-- funcTreeId :: Tree a -> Prop
-- funcTreeId l = prove (fmap id l :=: l)

-- funcTreeCompose :: (a1 -> b) -> (a2 -> a1) -> Tree a2 -> Prop
-- funcTreeCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- funcStateId :: State s x -> Prop
-- funcStateId l = prove (fmap id l :=: l)

-- funcStateCompose :: (a1 -> b) -> (a2 -> a1) -> State s a2 -> Prop
-- funcStateCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- -- State
-- appStateId :: State s b -> Prop
-- appStateId x = prove (pure id <*> x :=: x)

-- appStateCompose :: State s (b1 -> b2) -> State s (a -> b1) -> State s a -> Prop
-- appStateCompose a b c = prove (pure (.) <*> a <*> b <*> c :=: a <*> (b <*> c))

-- appStateHomomorphism :: (a -> b) -> a -> Prop
-- appStateHomomorphism f x = prove ((pure :: x -> State s x) f <*> (pure x) :=: pure (f x))

-- appStateInterchange :: State s (a -> b) -> a -> Prop
-- appStateInterchange f x = prove (f <*> pure x :=: pure ($ x) <*> f)

-- monadStateLeftId :: a -> (a -> State s a) -> Prop
-- monadStateLeftId a f = prove (return a >>= f :=: f a)

-- monadStateRightId :: State s a -> Prop
-- monadStateRightId x = prove (x >>= return :=: x)

-- monadStateAssociative :: State s a -> (a -> State s a) -> (a -> State s a) -> Prop
-- monadStateAssociative a b c = prove (a >>= (\x -> b x >>= c) :=: (a >>= b) >>= c)

-- -- Reader
funcReaderId :: Reader r x -> Prop
funcReaderId l = prove (fmap id l :=: l)

-- funcReaderCompose :: (a1 -> b) -> (a2 -> a1) -> Reader r a2 -> Prop
-- funcReaderCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- appReaderId :: Reader r b -> Prop
-- appReaderId x = prove (pure id <*> x :=: x)

-- appReaderCompose :: Reader r (b1 -> b2) -> Reader r (a -> b1) -> Reader r a -> Prop
-- appReaderCompose a b c = prove (pure (.) <*> a <*> b <*> c :=: a <*> (b <*> c))

-- appReaderHomomorphism :: (a -> b) -> a -> Prop
-- appReaderHomomorphism f x = prove ((pure :: x -> Reader r x) f <*> (pure x) :=: pure (f x))

-- appReaderInterchange :: Reader r (a -> b) -> a -> Prop
-- appReaderInterchange f x = prove (f <*> pure x :=: pure ($ x) <*> f)

-- monadReaderLeftId :: a -> (a -> Reader r a) -> Prop
-- monadReaderLeftId a f = prove (return a >>= f :=: f a)

-- monadReaderRightId :: Reader r a -> Prop
-- monadReaderRightId x = prove (x >>= return :=: x)

-- monadReaderAssociative :: Reader r a -> (a -> Reader r a) -> (a -> Reader r a) -> Prop
-- monadReaderAssociative a b c = prove (a >>= (\x -> b x >>= c) :=: (a >>= b) >>= c)


-- -- Either
-- funcEitherId :: Either l x -> Prop
-- funcEitherId l = prove (fmap id l :=: l)

-- funcEitherCompose :: (a1 -> b) -> (a2 -> a1) -> Either l a2 -> Prop
-- funcEitherCompose f g l = prove (fmap f (fmap g l) :=: fmap (f . g) l)

-- appEitherId :: Either l b -> Prop
-- appEitherId x = prove (pure id <*> x :=: x)

-- appEitherCompose :: Either l (b1 -> b2) -> Either l (a -> b1) -> Either l a -> Prop
-- appEitherCompose a b c = prove (pure (.) <*> a <*> b <*> c :=: a <*> (b <*> c))

-- appEitherHomomorphism :: (a -> b) -> a -> Prop
-- appEitherHomomorphism f x = prove ((pure :: x -> Either l x) f <*> (pure x) :=: pure (f x))

-- appEitherInterchange :: Either l (a -> b) -> a -> Prop
-- appEitherInterchange f x = prove (f <*> pure x :=: pure ($ x) <*> f)

-- monadEitherLeftId :: a -> (a -> Either l a) -> Prop
-- monadEitherLeftId a f = prove (return a >>= f :=: f a)

-- monadEitherRightId :: Either l a -> Prop
-- monadEitherRightId x = prove (x >>= return :=: x)

-- monadEitherAssociative :: Either l a -> (a -> Either l a) -> (a -> Either l a) -> Prop
-- monadEitherAssociative a b c = prove (a >>= (\x -> b x >>= c) :=: (a >>= b) >>= c)

-- -- Endofunction
-- semigroupEndofunctionAssociative :: Endofunction a -> Endofunction a -> Endofunction a -> Prop
-- semigroupEndofunctionAssociative a b c = prove ((a <> b) <> c :=: a <> (b <> c))

-- monoidEndofunctionLeftId :: Endofunction a -> Prop
-- monoidEndofunctionLeftId l = prove (mempty <> l :=: l)

-- monoidEndofunctionRightId :: Endofunction a -> Prop
-- monoidEndofunctionRightId l = prove (l <> mempty :=: l)

-- -- Nat Plus
-- NOTE zeno finds a false counterexample. This property is true!
-- semigroupNatPlusAssociative :: NatPlus -> NatPlus -> NatPlus -> Prop
-- semigroupNatPlusAssociative a b c = prove ((a <> b) <> c :=: a <> (b <> c))

monoidNatPlusLeftId :: NatPlus -> Prop
monoidNatPlusLeftId l = prove (mempty <> l :=: l)

monoidNatPlusRightId :: NatPlus -> Prop
monoidNatPlusRightId l = prove (l <> mempty :=: l)

-- -- Nat Mul
-- NOTE zeno finds a false counterexample. This property is true!
-- semigroupNatMulAssociative :: NatMul -> NatMul -> NatMul -> Prop
-- semigroupNatMulAssociative a b c = prove ((a <> b) <> c :=: a <> (b <> c))

monoidNatMulLeftId :: NatMul -> Prop
monoidNatMulLeftId l = prove (mempty <> l :=: l)

monoidNatMulRightId :: NatMul -> Prop
monoidNatMulRightId l = prove (l <> mempty :=: l)

-- -- Function
-- categoryFunctionLeftId :: (a -> b) -> Prop
-- categoryFunctionLeftId f = prove (compose idCat f :=: f)

-- categoryFunctionRightId :: (a -> b) -> Prop
-- categoryFunctionRightId f = prove (compose f idCat :=: f)

-- categoryFunctionAssoc :: (a -> b) -> (c -> a) -> (d -> c) -> Prop
-- categoryFunctionAssoc f g h = prove (f `compose` (g `compose` h) :=: (f `compose` g) `compose` h)

-- arrowFunctionId :: Prop
-- arrowFunctionId = prove (arr id :=: id)

-- arrowFunctionDistrib :: (a -> b) -> (b -> c) -> Prop
-- arrowFunctionDistrib f g = prove ((arr :: (a -> b) -> (a -> b)) (f >>> g) :=: arr f >>> arr g)

-- arrowFunctionFirstCommut :: (b -> c) -> Prop
-- arrowFunctionFirstCommut f = prove (first ((arr :: (a -> b) -> (a -> b)) f) :=: arr (first f))

-- arrowFunctionFirstDistrib :: (a -> b) -> (b -> c) -> Prop
-- arrowFunctionFirstDistrib f g = prove (first (f >>> g) :=: first f >>> first g)

-- arrowFunctionProp1 :: (b -> c) -> Prop
-- arrowFunctionProp1 f = prove (first f >>> arr fst :=: arr fst >>> f)

-- arrowFunctionProp2 :: (a -> b) -> (c -> d) -> Prop
-- arrowFunctionProp2 f g = prove (first f >>> arr (id *** g) :=: arr (id *** g) >>> first f)

-- arrowFunctionProp3 :: (b -> b) -> (Pair b d1 -> Pair b d2) -> Prop
-- arrowFunctionProp3 f assoc = prove ((first f) >>> arr assoc :=: arr assoc >>> first f)
