{-# LANGUAGE NoImplicitPrelude #-}
module Trivial where

import Zeno

data Maybe a = Just a | Nothing
data ℕ = Zero | Suc ℕ
data Unit = Unit

(&&) :: Bool -> Bool -> Bool
False && _ = False
True && b = b

(||) :: Bool -> Bool -> Bool
True || _ = True
False || b = b

not True = False
not False = True

unit :: Unit -> Prop
unit u = prove (u :=: Unit)

absurd1 :: Bool -> Prop
absurd1 a = prove (a :=: not a)

absurd2 :: Prop
absurd2 = prove (False :=: True)

andCommut :: Bool -> Bool -> Prop
andCommut a b = prove (a && b :=: b && a)

andAssoc :: Bool -> Bool -> Bool -> Prop
andAssoc a b c = prove (a && (b && c) :=: (a && b) && c)

deMorgan :: Bool -> Bool -> Prop
deMorgan a b = prove (not (a && b) :=: not b || not a)

orId :: Bool -> Prop
orId a = prove (a || a :=: a)

andId :: Bool -> Prop
andId a = prove (a && a :=: a)

notNot :: Bool -> Prop
notNot a = prove (a :=: not (not a))

fromMaybe :: p -> Maybe p -> p
fromMaybe a Nothing = a
fromMaybe _ (Just x) = x

may :: Maybe Bool -> Prop
may x = prove (fromMaybe False x :=: False)

add :: ℕ -> ℕ -> ℕ
add Zero b = b
add (Suc a) b = Suc (add a b)

mul Zero _ = Zero
mul (Suc a) b = add (mul a b) b

zero = Zero
one = Suc zero
two = Suc one
three = add one two

five :: Prop
five = prove (add three two :=:
       add two three)

ten :: Prop
ten = prove (add three (add three (add three one)) :=:
      add (add two (add one three)) (add zero (add two two)))

zeroLeft :: ℕ -> Prop
zeroLeft x = prove (add zero x :=: x)

mulZeroLeft :: ℕ -> Prop
mulZeroLeft x = prove (mul zero x :=: zero)

mulOneLeft :: ℕ -> Prop
mulOneLeft x = prove (mul one x :=: x)

snoc :: [a] -> a -> [a]
snoc [] a = [a]
snoc (x:xs) a = x : (snoc xs a)

tail :: [a] -> [a]
tail [] = []
tail (x:xs) = xs

tailSnoc :: a -> [a] -> a -> Prop
tailSnoc y l x = prove (tail (snoc (y:l) x) :=: snoc (tail (y:l)) x)
