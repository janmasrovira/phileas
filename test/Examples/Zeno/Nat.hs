{-# LANGUAGE NoImplicitPrelude #-}
module Induction1 where

import Zeno

class Eq a where
  (==) :: a -> a -> Bool

class Eq a => Ord a where
  (<=) :: a -> a -> Bool

instance Eq Ordering where
  EQ == EQ = True
  LT == LT = True
  GT == GT = True
  _ == _ = False

instance Ord Nat where
  Zero <= _ = True
  Suc a <= Suc b = a <= b
  _ <= _ = False

data Nat = Zero
  | Suc Nat

data Ordering = LT | EQ | GT

instance Eq Nat where
  Zero == Zero = True
  Suc a == Suc b = a == b
  _ == _ = False

add :: Nat -> Nat -> Nat
add Zero b = b
add (Suc a) b = Suc (add a b)

addZeroRight :: Nat -> Prop
addZeroRight a = prove (add a Zero :=: a)

addSucRight :: Nat -> Nat -> Prop
addSucRight a b = prove (add a (Suc b) :=: Suc (add a b))

trueEq :: Nat -> Nat -> Prop
trueEq a b = given (a == b :=: True) (prove $ a :=: b)

eqImpliesStructEq :: Nat -> Nat -> Prop
eqImpliesStructEq a b = given (a == b :=: True) (prove $ a :=: b)

structEqImpliesEq :: Nat -> Nat -> Prop
structEqImpliesEq a b = given (a :=: b) (prove $ a == b :=: True)

partialOrderAntisimmetry :: Nat -> Nat -> Prop
partialOrderAntisimmetry a b = given (a <= b :=: True) (given (b <= a :=: True) (prove $ a == b :=: True))

partialOrderRefl :: Nat -> Prop
partialOrderRefl a = prove (a <= a :=: True)

partialOrderTransitivity :: Nat -> Nat -> Nat -> Prop
partialOrderTransitivity a b c = given (a <= b :=: True) (given (b <= c :=: True) (prove $ a <= c :=: True))
