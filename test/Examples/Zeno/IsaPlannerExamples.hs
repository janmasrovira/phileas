{-# LANGUAGE NoImplicitPrelude #-}
module IsaPlannerExamples where

import Zeno

data Nat = Zero | Suc Nat
data Maybe a = Just a | Nothing
data Pair a b = Pair a b
data Tree a = Leaf | Node a (Tree a) (Tree a)

class Eq a where
  (==) :: a -> a -> Bool

instance Eq Bool where
  False == False = True
  True == True = True
  _ == _ = False

infix 4 ≡
(≡) :: a -> a -> Prop
a ≡ b = prove (a :=: b)

infixr 3 ⇒
(⇒) :: Equals -> Prop -> Prop
a ⇒ b = given a b

-- NOTE: SUCCESS
propInitLast :: [a] -> Prop
propInitLast xs = (null xs :=: False) ⇒ init xs ++ (maybeToList (last xs)) ≡ xs

propInitSnoc :: [a] -> a -> Prop
propInitSnoc xs x = init (xs ++ [x]) ≡ xs

propSucCount :: Nat -> [Nat] -> Prop
propSucCount n l = Suc Zero `add` count n l ≡ count n (n : l)

propMinusPlus :: Nat -> Nat -> Prop
propMinusPlus n m = n - (n `add` m) ≡ Zero

propPlusMinus :: Nat -> Nat -> Prop
propPlusMinus n m = (n `add` m) - n ≡ m

propPlusMinusPlus :: Nat -> Nat -> Nat -> Prop
propPlusMinusPlus k m n = (k `add` m) - (k `add` n) ≡ m - n

propMinusSelf :: Nat -> Prop
propMinusSelf m = m - m ≡ Zero

propDropZero :: [a] -> Prop
propDropZero xs = drop Zero xs ≡ xs

propDropSuc :: Nat -> a -> [a] -> Prop
propDropSuc n x xs = drop (Suc n) (x : xs) ≡ drop n xs

propFilterAppend :: (a -> Bool) -> [a] -> [a] -> Prop
propFilterAppend p xs ys = filter p (xs ++ ys) ≡ filter p xs ++ filter p ys

propLengthInsert :: Nat -> [Nat] -> Prop
propLengthInsert x l = length (insert x l) ≡ Suc (length l)

propAppendNullHyp :: [a] -> [a] -> Prop
propAppendNullHyp xs ys = (ys :=: []) ⇒ last (xs ++ ys) ≡ last xs

propSingleLastHyp :: [a] -> a -> Prop
propSingleLastHyp xs x = (xs :=: []) ⇒ last (x : xs) ≡ Just x

propLastNotNull :: a -> [a] -> Prop
propLastNotNull x xs = (null xs :=: False) ⇒ last (x : xs) ≡ last xs

propLastAppend :: [a] -> a -> Prop
propLastAppend xs x = last (xs ++ [x]) ≡ Just x

propLeqZero :: Nat -> Prop
propLeqZero n = (n <= Zero :=: True) ⇒ n ≡ Zero

propLessSucAdd :: Nat -> Nat -> Prop
propLessSucAdd i m = i < (Suc (i `add` m)) ≡ True

propLeqAdd :: Nat -> Nat -> Prop
propLeqAdd n m = n <= (n `add`m) ≡ True

propMemberAppend :: Nat -> [Nat] -> [Nat] -> Prop
propMemberAppend x l t = (x `member` l :=: True) ⇒ x `member` (l ++ t) ≡ True

propMemberAppend2 :: Nat -> [Nat] -> [Nat] -> Prop
propMemberAppend2 x l t = (x `member` l :=: True) ⇒ x `member` (l ++ t) ≡ True

propMemberSnoc :: Nat -> [Nat] -> Prop
propMemberSnoc x l = x `member` (l ++ [x]) ≡ True

propMemberInsert :: Nat -> [Nat] -> Prop
propMemberInsert x l = x `member` insert x l ≡ True

propNotMemberInsert :: Nat -> Nat -> [Nat] -> Prop
propNotMemberInsert x y l = (x == y :=: False) ⇒ (x `member` insert y l) ≡ x `member` l

propMemberInsert2 :: Nat -> [Nat] -> Prop
propMemberInsert2 x l = x `member` insert x l ≡ True

propDropWhileFalse :: [a] -> Prop
propDropWhileFalse xs = dropWhile (\x -> False) xs ≡ xs

propTakeWhileTrue :: [a] -> Prop
propTakeWhileTrue xs = takeWhile (\x -> True) xs ≡ xs

propCountAdd :: Nat -> Nat -> [Nat] -> Prop
propCountAdd n h t = count n [h] `add` count n t ≡ count n (h : t)

propTakeZero :: [a] -> Prop
propTakeZero xs = take Zero xs ≡ []

propTakeSuc :: Nat -> a -> [a] -> Prop
propTakeSuc n x xs = take (Suc n) (x : xs) ≡ x : take n xs

propTakeWhileDropWhile :: (a -> Bool) -> [a] -> Prop
propTakeWhileDropWhile p xs = takeWhile p xs ++ dropWhile p xs ≡ xs

propZipCons :: a -> [a] -> b -> [b] -> Prop
propZipCons x xs y ys = zip (x : xs) (y : ys) ≡ (Pair x y : zip xs ys)

propZipNull :: [b] -> Prop
propZipNull ys = zip [] ys ≡ []

propTakeDrop :: Nat -> [a] -> Prop
propTakeDrop n xs = take n xs ++ drop n xs ≡ xs

propInitappend :: [a] -> [a] -> Prop
propInitappend xs ys = init (xs ++ ys) ≡ (if null ys then init xs else xs ++ init ys)


propInitTakeLength :: [a] -> Prop
propInitTakeLength xs = init xs ≡ take (length xs - Suc Zero) xs

propPlusMinusRight :: Nat -> Nat -> Prop
propPlusMinusRight m n = (m + n) - n ≡ m

propLengthDrop :: Nat -> [a] -> Prop
propLengthDrop n xs =  length (drop n xs) ≡ (length xs) - n

propLengthSort :: [Nat] -> Prop
propLengthSort l = length (insertSort l) ≡ length l

propLeqPlus :: Nat -> Nat -> Prop
propLeqPlus n m = n <= (m + n) ≡ True

propLeqSuc :: Nat -> Nat -> Prop
propLeqSuc m n = (m <= n :=: True) ⇒ m <= Suc n ≡ True

propDropZip :: Nat -> [a] -> [b] -> Prop
propDropZip n xs ys =  drop n (zip xs ys) ≡ zip (drop n xs) (drop n ys)

propLastAppendNull :: [a] -> [a] -> Prop
propLastAppendNull xs ys = (null ys :=: False) ⇒ last (xs ++ ys) ≡ last ys

propLastAppendIf :: [a] -> [a] -> Prop
propLastAppendIf xs ys = last (xs ++ ys) ≡ (if null ys then last xs else last ys)

propLastDropLength :: Nat -> [a] -> Prop
propLastDropLength n xs = (n < length xs :=: True) ⇒ last (drop n xs) ≡ last xs

propLessPlus :: Nat -> Nat -> Prop
propLessPlus i m = i < Suc (m + i) ≡ True

propLengthFilter :: (a -> Bool) -> [a] -> Prop
propLengthFilter p xs = length (filter p xs) <= length xs ≡ True

propLengthInit :: [a] -> Prop
propLengthInit xs = length (init xs) ≡ length xs - Suc Zero

propLengthDelete :: Nat -> [Nat] -> Prop
propLengthDelete x l = length (delete x l) <= length l ≡ True

propDropPlus :: Nat -> Nat -> [a] -> Prop
propDropPlus n m xs = drop n (drop m xs) ≡ drop (n + m) xs

propDropMap :: Nat -> (a1 -> a2) -> [a1] -> Prop
propDropMap n f xs = drop n (map f xs) ≡ map f (drop n xs)

propDropTake :: Nat -> Nat -> [a] -> Prop
propDropTake n m xs = drop n (take m xs) ≡ take (m - n) (drop n xs)

propDropAppendMinusLength :: Nat -> [a] -> [a] -> Prop
propDropAppendMinusLength n xs ys = drop n (xs ++ ys) ≡ drop n xs ++ drop (n - (length xs)) ys

propMinusPlusRight :: Nat -> Nat -> Nat -> Prop
propMinusPlusRight i j k = (i - j) - k ≡ i - (j + k)

propMaxAssoc :: Nat -> Nat -> Nat -> Prop
propMaxAssoc a b c = max (max a b) c ≡ max a (max b c)

propMaxCommut :: Nat -> Nat -> Prop
propMaxCommut a b =  max a b ≡ max b a

propMaxLeq :: Nat -> Nat -> Prop
propMaxLeq a b = (max a b :=: a) ⇒ b <= a ≡ True

propMaxLeq2 :: Nat -> Nat -> Prop
propMaxLeq2 a b = (max a b :=: b) ⇒ a <= b ≡ True

propLessMemberInsert :: Nat -> Nat -> [Nat] -> Prop
propLessMemberInsert x y l = (x < y :=: True) ⇒ x `member` insert y l ≡ x `member` l

propMinAssoc :: Nat -> Nat -> Nat -> Prop
propMinAssoc a b c =  min (min a b) c ≡ min a (min b c)

propMinCommut :: Nat -> Nat -> Prop
propMinCommut a b = min a b ≡ min b a

propMinLeq :: Nat -> Nat -> Prop
propMinLeq a b = (min a b :=: a) ⇒ a <= b ≡ True

propMinLeq2 :: Nat -> Nat -> Prop
propMinLeq2 a b = (min a b :=: b) ⇒ b <= a ≡ True

propReverseFilter :: (a -> Bool) -> [a] -> Prop
propReverseFilter p xs = reverse (filter p xs) ≡ filter p (reverse xs)

propCountPlusSnoc :: Nat -> [Nat] -> Nat -> Prop
propCountPlusSnoc n t h = count n t + count n [h] ≡ count n (h : t)

propInsertSortStep :: [Nat] -> Nat -> Prop
propInsertSortStep l x = (sorted l :=: True) ⇒ sorted (insert x l) ≡ True

propInsertSortCorrect :: [Nat] -> Prop
propInsertSortCorrect l = sorted (insertSort l) ≡ True

propTakeAppendMinusLength :: Nat -> [a] -> [a] -> Prop
propTakeAppendMinusLength n xs ys = take n (xs ++ ys) ≡ take n xs ++ take (n - (length xs)) ys

propTakeDropTakePlus :: Nat -> Nat -> [a] -> Prop
propTakeDropTakePlus n m xs = take n (drop m xs) ≡ drop m (take (n + m) xs)

propTakeMap :: Nat -> (a1 -> a2) -> [a1] -> Prop
propTakeMap n f xs = take n (map f xs) ≡ map f (take n xs)

propTakeZip :: Nat -> [a] -> [b] -> Prop
propTakeZip n xs ys = take n (zip xs ys) ≡ zip (take n xs) (take n ys)

propZipAppend :: [a] -> [a] -> [b] -> Prop
propZipAppend xs ys zs = zip (xs ++ ys) zs ≡ zip xs (take (length xs) zs) ++ zip ys (drop (length xs) zs)

propZipAppend2 :: [a] -> [b] -> [b] -> Prop
propZipAppend2 xs ys zs = zip xs (ys ++ zs) ≡ zip (take (length ys) xs) ys ++ zip (drop (length ys) xs) zs

propZipConsCase :: a -> [a] -> [b] -> Prop
propZipConsCase x xs ys = zip (x : xs) ys ≡ (
  case ys of
    [] -> []
    y : ys -> Pair x y : zip xs ys)

propHeightMirror :: Tree a -> Prop
propHeightMirror a = height (mirror a) ≡ height a


-- NOTE: FAIL
propCountAppend :: [Nat] -> Nat -> [Nat] -> Prop
propCountAppend l n m = count n l `add` count n m ≡ count n (l ++ m)

propCountLeq :: Nat -> [Nat] -> [Nat] -> Prop
propCountLeq n l m = count n l <= count n (l ++ m) ≡ True

propSucCountHyp :: Nat -> Nat -> [Nat] -> Prop
propSucCountHyp n x l = (n :=: x) ⇒ Suc Zero `add` count n l ≡ count n (x : l)

propCountRev :: Nat -> [Nat] -> Prop
propCountRev n l = count n l ≡ count n (reverse l)

propCountSort :: Nat -> [Nat] -> Prop
propCountSort x l = count x l ≡ count x (insertSort l)

propRevDropTake :: Nat -> [a] -> Prop
propRevDropTake i xs = reverse (drop i xs) ≡ take ((length xs) - i) (reverse xs)

propReverseTakeDropLength :: Nat -> [a] -> Prop
propReverseTakeDropLength i xs = reverse (take i xs) ≡ drop ((length xs) - i) (reverse xs)

propCountNeqSnoc :: Nat -> Nat -> [Nat] -> Prop
propCountNeqSnoc n h x = (n == h :=: False) ⇒ count n (x ++ [h]) ≡ count n x

propSucMinusSides :: Nat -> Nat -> Nat -> Prop
propSucMinusSides m n k = ((Suc m) - n) - (Suc k) ≡ (m - n) - k

-- NOTE: FALSE
propMemberDelete :: Nat -> [Nat] -> Prop
propMemberDelete x l = x `member` delete x l ≡ False

propMinusMinus :: Nat -> Nat -> Nat -> Prop
propMinusMinus i j k = (i - j) - k ≡ i - (k - j)

-- NOTE: CRASH
-- propCountSnoc :: Nat -> [Nat] -> Prop
-- propCountSnoc n x = count n (x ++ [n]) ≡ Suc (count n x)

-- NOTE: TIMEOUT
-- propLengthEqZip :: [a] -> [b] -> Prop
-- propLengthEqZip xs ys = (length xs :=: length ys) ⇒ zip (reverse xs) (reverse ys) ≡ reverse (zip xs ys)


  -- functions
null :: [a] -> Bool
null [] = True
null _ = False

maybeToList (Just x) = [x]
maybeToList Nothing = []

filter f [] = []
filter f (x:xs)
  | f x = x : filter f xs
  | True = filter f xs

count :: Nat -> [Nat] -> Nat
count n = length . filter (==n)

add :: Nat -> Nat -> Nat
add Zero a    = a
add (Suc a) b = Suc (add a b)

max :: Nat -> Nat -> Nat
max Zero b = b
max (Suc a) (Suc b) = Suc (max a b)
max a Zero = a

min :: Nat -> Nat -> Nat
min (Suc a) (Suc b) = Suc (min a b)
min  _ _ = Zero

(+) :: Nat -> Nat -> Nat
(+) = add

(-) :: Nat -> Nat -> Nat
a - Zero = a
Suc a - Suc b = a - b
Zero - _ = Zero

map :: (t -> a) -> [t] -> [a]
map f []       = []
map f (x : xs) = (:) (f x) (map f xs)

init :: [a] -> [a]
init [] = []
init [x] = []
init (x:xs) = x : init xs

(.) :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
f . g = \x -> f (g x)

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile f [] = []
dropWhile f (x:xs)
  | f x = dropWhile f xs
  | True = x:xs

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile f [] = []
takeWhile f (x:xs)
  | f x = x : takeWhile f xs
  | True = []

const :: p1 -> p2 -> p1
const x _ = x

foldr :: (t1 -> t2 -> t2) -> t2 -> [t1] -> t2
foldr f ini []     = ini
foldr f ini (x:xs) = f x (foldr f ini xs)

sum :: [Nat] -> Nat
sum = foldr add Zero

take :: Nat -> [a] -> [a]
take (Suc n) (x:xs) = x : take n xs
take _ _            = []

length :: [a] -> Nat
length []     = Zero
length (_:xs) = Suc (length xs)

id :: p -> p
id x = x

reverse :: [a] -> [a]
reverse []     = []
reverse (x:xs) = reverse xs ++ [x]

drop :: Nat -> [a] -> [a]
drop (Suc n) (x:xs) = drop n xs
drop _ l = l

reverse' :: [a] -> [a]
reverse' = go []
  where go ac []     = ac
        go ac (x:xs) = go (x:ac) xs

(++) :: [a] -> [a] -> [a]
[] ++ l = l
(a:as) ++ b = a : (as ++ b)

snoc :: t -> [t] -> [t]
snoc x []     = [x]
snoc x (y:xs) = y : snoc x xs

member :: Eq t => t -> [t] -> Bool
member x [] = False
member x (y:ys)
  | x == y = True
  | True = member x ys

last :: [a] -> Maybe a
last []     = Nothing
last [x]    = Just x
last (x:xs) = last xs

replicate :: Nat -> a -> [a]
replicate Zero _    = []
replicate (Suc n) x = x : replicate n x

insert :: Nat -> [Nat] -> [Nat]
insert a [] = [a]
insert a (x:xs)
  | a <= x = a:x:xs
insert a (x:xs) = x : insert a xs

sorted :: [Nat] -> Bool
sorted [] = True
sorted [x] = True
sorted (x:y:ys)
  | x <= y = sorted (y:ys)
  | True = False

insertSort :: [Nat] -> [Nat]
insertSort [] = []
insertSort (x:xs) = insert x (insertSort xs)

delete :: Nat -> [Nat] -> [Nat]
delete n [] = []
delete n (a:as)
  | n == a = delete n as
  | True = n : delete n as

(<=) :: Nat -> Nat -> Bool
Zero <= _ = True
Suc a <= Suc b = a <= b
_ <= _ = False

(<) :: Nat -> Nat -> Bool
a < b = Suc a <= b

zip :: [a] -> [b] -> [Pair a b]
zip (x:xs) (y:ys) = Pair x y : zip xs ys
zip _ _ = []

instance Eq Nat where
  Zero == Zero = True
  Suc a == Suc b = a == b
  _ == _ = False

height :: Tree a -> Nat
height Leaf = Zero
height (Node x l r) = Suc (max (height l) (height r))

mirror :: Tree a -> Tree a
mirror Leaf = Leaf
mirror (Node x l r) = Node x (mirror r) (mirror l)

