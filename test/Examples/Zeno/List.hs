{-# LANGUAGE NoImplicitPrelude #-}
module Induction1 where

import Zeno

data Nat = Zero | Suc Nat

data Maybe a = Just a | Nothing
class Eq a where
  (==) :: a -> a -> Bool

instance Eq Bool where
  False == False = True
  True == True = True
  _ == _ = False

add Zero a    = a
add (Suc a) b = Suc (add a b)

-- functions
map f []       = []
map f (x : xs) = (:) (f x) (map f xs)

(.) :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
f . g = \x -> f (g x)

const :: p1 -> p2 -> p1
const x _ = x

foldr f ini []     = ini
foldr f ini (x:xs) = f x (foldr f ini xs)

sum :: [Nat] -> Nat
sum = foldr add Zero

take :: Nat -> [a] -> [a]
take (Suc n) (x:xs) = x : take n xs
take _ _            = []

length []     = Zero
length (_:xs) = Suc (length xs)

id :: p -> p
id x = x

reverse :: [a] -> [a]
reverse []     = []
reverse (x:xs) = reverse xs ++ [x]

reverse' :: [a] -> [a]
reverse' = go []
  where go ac []     = ac
        go ac (x:xs) = go (x:ac) xs

(++) :: [a] -> [a] -> [a]
[] ++ l = l
(a:as) ++ b = a : (as ++ b)

snoc :: t -> [t] -> [t]
snoc x []     = [x]
snoc x (y:xs) = y : snoc x xs

member x [] = False
member x (y:ys)
  | x == y = True
member x (y:ys) = member x ys

last :: [a] -> Maybe a
last []     = Nothing
last [x]    = Just x
last (x:xs) = last xs

replicate :: Nat -> a -> [a]
replicate Zero _    = []
replicate (Suc n) x = x : replicate n x

insert :: Nat -> [Nat] -> [Nat]
insert a [] = [a]
insert a (x:xs)
  | a == x = x:xs
  | a <= x = a:x:xs
insert a (x:xs) = x : insert a xs

(<=) :: Nat -> Nat -> Bool
Zero <= _ = True
Suc a <= Suc b = a <= b
_ <= _ = False

instance Eq Nat where
  Zero == Zero = True
  Suc a == Suc b = a == b
  _ == _ = False

sumLength :: [a] -> Prop
sumLength l = prove (sum (map (const (Suc Zero)) l) :=: length l)

-- NOTE zeno crashes
-- mapConst :: a -> p2 -> Prop
-- mapConst x y = prove (map (const x) :=: map (const x) . map (const y))

lengthMap :: (t -> a) -> [t] -> Prop
lengthMap f l = prove (length l :=: length (map f l))

takeMap :: Nat -> [a1] -> (a1 -> a2) -> Prop
takeMap n l f = prove (take n (map f l) :=: map f (take n l))

memberSnoc :: Bool -> [Bool] -> Prop
memberSnoc x xs = prove (member x (snoc x xs) :=: True)

lengthConcat :: [a] -> [a] -> Prop
lengthConcat a b = prove (length a `add` length b :=: length (a ++ b))

lastCons :: a -> a -> [a] -> Prop
lastCons a b l = prove (last (b:l) :=: last (a:b:l))

lengthReplicate :: a -> Nat -> Prop
lengthReplicate x k = prove (length (replicate k x) :=: k)

lastSnoc :: a -> [a] -> Prop
lastSnoc x l = prove (last (snoc x l) :=: Just x)

lengthReverse :: [a] -> Prop
lengthReverse l = prove (length l :=: length (reverse l))

reverseTail :: Prop
reverseTail = prove (reverse :=: reverse')

-- NOTE zeno crashes
-- reverseReverse :: Prop
-- reverseReverse = prove (reverse . reverse :=: id)

memberInsert :: Nat -> [Nat] -> Prop
memberInsert x l = prove (member x (insert x l) :=: True)

reverseConcat :: [a] -> [a] -> Prop
reverseConcat a b = prove (reverse (a ++ b) :=: reverse b ++ reverse a)
