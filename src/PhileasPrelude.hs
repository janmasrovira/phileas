{-# LANGUAGE ExistentialQuantification #-}
module PhileasPrelude (
  module Prelude
  , Property
  , bottom
  , (≡)
  , (⇒)
  , (∀)
  -- * Built in
  , builtinVarsNTypes
  ) where

import Prelude (Bool (..))

data Property =
  forall a. Eq a a
  | forall a. Forall (a -> Property)
  | Impl Property Property

infix 4 ≡
(≡) :: a -> a -> Property
a ≡ b = Eq a b

infixr 3 ⇒
(⇒) :: Property -> Property -> Property
a ⇒ b = a `Impl` b

(∀) :: (a -> Property) -> Property
(∀) = Forall

bottom :: bottomType
bottom = bottom

-- Used internally to bring all builtin vars into scope
builtinVarsNTypes :: [Bool]
builtinVarsNTypes = True : False : []
