{-# LANGUAGE StrictData      #-}
{-# LANGUAGE TemplateHaskell #-}
module Phileas.Phi.Syntax where

import           Data.Generics.Uniplate.Data
import qualified Data.Map                    as Map
import           Phileas.Prelude

data PolyTypeVar = PolyTypeVar {
  _polyId     :: Id
  , _polyName :: Name
  }
  deriving (Show, Data, Typeable)
instance Eq PolyTypeVar where
  (==) = (==)`on`_polyId
instance Ord PolyTypeVar where
  compare = compare`on`_polyId
instance Hashable PolyTypeVar where
  hashWithSalt salt p = hashWithSalt salt (_polyId p)

newtype Id = Id { _unId :: Int }
  deriving (Eq, Show, Ord, Hashable, Data, Typeable)

instance Enum Id where
  toEnum = Id
  fromEnum = _unId

data Module = Module {
  _topDefinitions :: HashMap Var DefBody
  , _dataTypes    :: HashMap TypeCon TypeConDataDef
  , _nameMap      :: HashMap Id Name
  }
  deriving (Show, Data, Typeable, Eq)

data DefBody =
  DefExpr Expression
  | DefPrimitive PrimitiveDef
  deriving (Show, Data, Typeable, Eq)

data PrimitiveDef =
  IntAddDef
  | IntMulDef
  deriving (Show, Data, Typeable, Eq)

data TypeConDataDef =
  PrimitiveTypeCon
  | DefinedTypeCon DataTypeDef
  deriving (Show, Data, Typeable, Eq)

data TypeCon = TypeCon {
  _typeConName :: Name
  , _typeConId :: Id
  }
  deriving (Show, Data, Typeable)
instance Eq TypeCon where
   (==) = (==)`on`_typeConId

instance Ord TypeCon where
  compare = compare`on`_typeConId

instance Hashable TypeCon where
  hashWithSalt i v = hashWithSalt i (_typeConId v)

data Type =
  TypeConApp TypeCon [Type]
  | PolyVarType PolyTypeVar
  | FunType Type Type
  | AppType Type Type
  | ForAllType PolyTypeVar Type
  deriving (Eq, Show, Data, Typeable)

data Expression =
  EVar Var
  | App Expression Expression
  | Case CaseId Expression Type [Alt]
  | Lit Literal
  | Lambda Var Expression
  | TypeLambda PolyTypeVar Expression
  | TypeApp Expression Type
  | Let Bind Expression
  | Bottom
  deriving (Eq, Show, Data, Typeable)

data Bind =
  NonRec (Var, Expression)
  | Rec [(Var, Expression)]
  deriving (Eq, Show, Data, Typeable)

data Alt = Alt {
  _altPat    :: Pattern
  , _altExpr :: Expression }
  deriving (Eq, Show, Data, Typeable)

data PConsApp = PConsApp Var [Var]
  deriving (Eq, Show, Data, Typeable)

data ConsApp = ConsApp DataCon [Expression]
  deriving (Eq, Show, Data, Typeable)

data Pattern
  = PatCons PConsApp
  | Wildcard
  deriving (Eq, Show, Data, Typeable)

data Name = Name {
  _nameId       :: Id
  , _nameString :: String
  }
  deriving (Show, Data, Typeable)
instance Eq Name where
  (==) = (==) `on` _nameId
instance Ord Name where
  compare = compare `on` _nameId

newtype CaseId = CaseId {unCaseId :: Id}
  deriving (Eq, Ord, Show, Data, Typeable, Hashable)

data HistoryEvent =
  InductionOn CaseId
  | ExpandOn TypeCon
  deriving (Eq, Ord, Show, Data, Typeable)

newtype History = History {
  _unHistory :: Map HistoryEvent Int
  }
  deriving (Show, Data, Typeable)
instance Monoid History where
  mempty = History mempty
  mappend a b = History (Map.unionWith (+) (_unHistory a) (_unHistory b))

instance Semigroup History where
  (<>) = mappend

data VarSort =
  UniversalVar History
  | ConstructorVar {
      _conIsSelfRecursive :: Bool
      }
  | DefinedVar {
      _varDef         :: Maybe DefBody
      , _varDefLetRec :: Bool
      }
  deriving (Show, Data, Typeable)

data Var = Var {
  _varName   :: Name
  , _varId   :: Id
  , _varType :: Type
  , _varSort :: VarSort
  }
  deriving (Show, Data, Typeable)

instance Eq Var where
   (==) a b = eqId
     where eqId = ((==)`on`_varId) a b
           -- eqType = ((==)`on`_varType)  a b

instance Ord Var where
  compare = compare`on`_varId

instance Hashable Var where
  hashWithSalt i v = hashWithSalt i (_varId v)

data Literal =
  IntLit Integer
  | UndefinedLit
  deriving (Show, Data, Typeable)
instance Eq Literal where
  IntLit i == IntLit j = i == j
  _ == _ = error "undefined literal"

-- | The variable inside is guaranteed to be a constructor.
newtype DataCon = DataCon {_conVar :: Var}
  deriving (Show, Data, Typeable, Ord, Hashable, Eq)

mkDataCon :: Var -> Maybe DataCon
mkDataCon v = case _varSort v of
  ConstructorVar{} -> Just (DataCon v)
  _                -> Nothing

data DataTypeDef = DataTypeDef {
  _typeCon        :: TypeCon
  , _typeDataCons :: [DataCon]
  }
  deriving (Show, Data, Typeable, Eq)

data TypeSig = TypeSig {
  _sigForAllVars   :: [PolyTypeVar]
  , _sigArgs       :: [Type]
  , _sigReturnType :: Type
  }
  deriving (Eq, Show)

class HasId a where
  getId :: a -> Id
instance HasId Id where
  getId = id
instance HasId Var where
  getId = _varId
instance HasId PolyTypeVar where
  getId = _polyId
instance HasId TypeCon where
  getId = _typeConId

makeLenses ''TypeSig
makeLenses ''Id
makeLenses ''DataCon
makeLenses ''DataTypeDef
makeLenses ''PolyTypeVar
makeLenses ''Module
makeLenses ''Name
makeLenses ''Var
makeLenses ''TypeCon
makeLenses ''Alt
