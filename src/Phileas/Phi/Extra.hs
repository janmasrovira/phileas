{-# LANGUAGE TemplateHaskell #-}
module Phileas.Phi.Extra where

import qualified Data.HashMap.Lazy  as HMap
import qualified Data.HashSet       as HSet
import qualified Data.Set           as Set
import qualified Data.Map           as Map
import           Phileas.Phi.Syntax
import           Phileas.Prelude

getVar :: Expression -> Maybe Var
getVar e = case e of
  EVar v -> Just v
  _      -> Nothing

isConstructorSort :: VarSort -> Bool
isConstructorSort ConstructorVar{} = True
isConstructorSort _                = False

isUniversalSort :: VarSort -> Bool
isUniversalSort UniversalVar{} = True
isUniversalSort _              = False

isDefinedSort :: VarSort -> Bool
isDefinedSort DefinedVar{} = True
isDefinedSort _            = False

isConstructorVar :: Var -> Bool
isConstructorVar = isConstructorSort . (^. varSort)

isDefinedVar :: Var -> Bool
isDefinedVar = isDefinedSort . (^. varSort)

isUniversalVar :: Var -> Bool
isUniversalVar = isUniversalSort . (^. varSort)

moduleDefinitions :: Module -> [(Var, Expression)]
moduleDefinitions m = [ (v, e) | (v, DefExpr e) <- HMap.toList (m ^. topDefinitions) ]

dataTypeConVars :: DataTypeDef -> [Var]
dataTypeConVars = map _conVar . (^. typeDataCons)

toConsApp :: PConsApp -> ConsApp
toConsApp (PConsApp c args) = ConsApp (DataCon c) (map EVar args)

getHistory :: Var -> Maybe History
getHistory v = case v ^. varSort of
  UniversalVar h -> Just h
  _              -> Nothing

definedVar :: Var -> DefBody -> Var
definedVar v e = over varSort aux v
  where aux vs = case vs of
          DefinedVar _ b -> DefinedVar (Just e) b
          _              -> error "definedVar over invalid sort"

unfoldConsApp :: Expression -> Maybe ConsApp
unfoldConsApp e = case unfoldApp e of
  (EVar c, args)
    | Just con <- mkDataCon c -> Just (ConsApp con args)
  _ -> Nothing

foldConsApp :: ConsApp -> Expression
foldConsApp (ConsApp c args) = foldl' App (EVar (_conVar c)) args

foldTypeApp :: Expression -> [Type] -> Expression
foldTypeApp = foldl' TypeApp

foldAppType :: Type -> [Type] -> Type
foldAppType = foldl' AppType

-- | Note that the application is not yet evaluated.
applyType :: Type -> [Type] -> Type
applyType = foldAppType

foldForAllType :: Type -> [PolyTypeVar] -> Type
foldForAllType = foldr ForAllType

foldFunType :: Type -> [Type] -> Type
foldFunType = foldr FunType

unfoldApp :: Expression -> (Expression, [Expression])
unfoldApp e = case e of
  App a b -> case unfoldApp a of
    (x, y) -> (x, snoc y b)
  _ -> (e, [])

unfoldApp1 :: Expression -> Maybe (Expression, NonEmpty Expression)
unfoldApp1 e = case unfoldApp e of
  (f, nonEmpty -> args) -> (f,) <$> args

unfoldAppType :: Type -> (Type, [Type])
unfoldAppType t = case t of
  AppType a b -> case unfoldAppType a of
    (x, y) -> (x, snoc y b)
  _ -> (t, [])

overVarDef :: (Expression -> Expression) -> Var -> Var
overVarDef f v = over varSort aux v
  where aux s = case s of
          DefinedVar (Just (DefExpr e)) r -> DefinedVar (Just (DefExpr (f e))) r
          _ -> s
