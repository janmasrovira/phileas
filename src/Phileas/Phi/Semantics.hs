{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Phileas.Phi.Semantics where

import qualified Data.HashMap.Lazy  as HMap
import qualified Data.HashSet       as HSet
import qualified Data.Map           as Map
import qualified Data.Set           as Set
import           Phileas.Phi.Extra
import           Phileas.Phi.Pretty
import           Phileas.Phi.Syntax
import           Phileas.Prelude

class HeadBetaReducible a where
  eval :: a -> a

class SubsExpr c where
  subsExpr :: (Expression -> Maybe Expression) -> c -> c
  universeExprs :: c -> [Expression]

class HasFreeVars c where
  freeVars :: c -> HashSet Var

instance HasFreeVars Expression where
  freeVars :: Expression -> HashSet Var
  freeVars = go mempty
    where
      (<+>) = HSet.insert
      go bound e = case e of
        EVar v
          | isFree v -> HSet.singleton v
          | otherwise -> mempty
        App a b -> go bound a <> go bound b
        TypeApp e t -> go bound e
        Lit{} -> mempty
        Lambda v e -> go (v <+> bound) e
        TypeLambda _ e -> go bound e
        Case _ e _ alts -> go bound e <> HSet.unions (map (goAlt bound) alts)
        Let _ e -> go bound e -- no need to add the defined vars since they will
                              -- be of sort `DefinedVar`, hence, not free.
        Bottom -> mempty
        where isFree v = isUniversalVar v && not (HSet.member v bound)
              isBound = not . isFree
      goAlt bound a = case a of
        Alt (PatCons (PConsApp _ vs)) e -> go (HSet.fromList vs <> bound) e
        Alt _ e                         -> go bound e


instance SubsExpr Alt where
  subsExpr subs = over altExpr (subsExpr subs)
  universeExprs = universeExprs . _altExpr


data MatchResult =
  NoMatch
  | Match Alt Expression

funTypeRet :: Type -> Type
funTypeRet = _sigReturnType . funTypeSig


-- NOTE old implementations
-- funTypeSig :: Type -> TypeSig
-- funTypeSig t = case t' of
--   FunType a b    -> over sigArgs (a:) (funTypeSig b)
--   ForAllType p t -> over sigForAllVars (HSet.insert p) (funTypeSig t)
--   _              -> TypeSig mempty [] t'
--   where t' = evalType "funtypesig" t

funTypeSig :: Type -> TypeSig
funTypeSig (evalType -> t)
  | t == foldTypeSig sig = sig
  | otherwise = error ("Type Sig\n" <> renderPhiDef t <> "\n!=\n" <> renderPhiDef (foldTypeSig sig))
  where
    sig = TypeSig {
      _sigForAllVars = uniVars
      , _sigArgs = args
      , _sigReturnType = ret
      }
    (uniVars, t') = splitForAllVars t
    (args, ret) = go t'
    go x = case x of
      FunType a b -> over _1 (a:) (go b)
      _ -> ([], x)

foldTypeSig :: TypeSig -> Type
foldTypeSig TypeSig {..} = ty
  where fun = foldFunType _sigReturnType _sigArgs
        ty = foldForAllType fun _sigForAllVars

splitForAllVars :: Type -> ([PolyTypeVar], Type)
splitForAllVars ty = case ty of
  ForAllType p t -> case splitForAllVars t of
    (vars, t') -> (p:vars, t')
  _ -> (mempty, ty)


singleEvent :: HistoryEvent -> History
singleEvent e = History (Map.singleton e 1)

getHistory :: Var -> Maybe History
getHistory v = case v ^. varSort of
  UniversalVar h -> Just h
  _              -> Nothing

evalType :: Type -> Type
evalType t = case t of
  AppType (evalType -> f) x -> case f of
    ForAllType poly t' -> evalType (substPolyVar (poly, x) t')
    TypeConApp{} -> error ("AppType to TypeConApp\n" <> "\n" <> renderPhi debugStyle t)
    FunType{} -> error ("AppType to FunType: " <> renderPhiDef t)
    PolyVarType{} -> t
    AppType{} -> t
  _ -> t

substPolyVar :: (PolyTypeVar, Type) -> Type -> Type
substPolyVar (pv, t) = substitutionTypeVar (HMap.singleton pv t)

substitutionTypeVar :: HashMap PolyTypeVar Type -> Type -> Type
substitutionTypeVar m t = case t of
      PolyVarType p       -> HMap.lookupDefault t p m
      TypeConApp con args -> TypeConApp con (map go args)
      FunType a b         -> FunType (go a) (go b)
      AppType a b         -> AppType (go a) (go b)
      ForAllType v t      -> ForAllType v (go' v t)
  where go = substitutionTypeVar m
        go' v = substitutionTypeVar (HMap.delete v m)

subsExprEq :: SubsExpr a => Expression -> Expression -> a -> a
subsExprEq x x' = subsExpr subs
  where subs y
          | y == x = Just x'
          | otherwise  = Nothing

substitution :: (Expression -> Maybe Expression) -> Expression -> Expression
substitution subs e
  | Just e' <- subs e
    = e'
      -- error ("Wrong types!\n" <> renderPhiDef (exprType e)
      --           <> "\n" <> renderPhiDef (exprType e') <> "\n"
      --           <> renderPhiDef e <> "\n" <> renderPhiDef e')
  | otherwise =
    case e of
      App a b           -> App (go a) (go b)
      Case cid e t alts -> Case cid (go e) t (map goalt alts)
      Lambda x e        -> Lambda x (go e)
      TypeLambda x e    -> TypeLambda x (go e)
      TypeApp e t       -> TypeApp (go e) t
      Let b e           -> Let (gobind b) (go e)
      EVar{}            -> e
      Lit{}             -> e
      Bottom{}          -> e
      where
        go = substitution subs
        goalt = over altExpr go
        gobind = substitutionBind subs

subsExprRec :: SubsExpr a => (Expression -> Maybe Expression) -> a -> a
subsExprRec subs = subsExpr subs'
  where subs' e = case subs e of
          Just x  -> Just (subsExprRec subs x)
          Nothing -> Nothing

betaReduce :: Expression -> Maybe Expression
betaReduce e = case e of
  App (Lambda v e) b -> Just (subsVarSameType (v, b) e)
  _ -> Nothing

simplifyCase :: Expression -> Maybe Expression
simplifyCase e = case e of
  Case cid e t alts -> rcase cid e t alts
  _                 -> Nothing
  where
    rcase cid e t alts = case unfoldConsApp e' of
      Just c -> case matchPatterns c alts of
        NoMatch        -> error "no match!"
        Match alt expr -> Just expr
      Nothing
        | isJust me -> Just (Case cid e' t alts)
        | otherwise -> Nothing
      where me = simplifyCase e
            e' = fromMaybe e me

substitutionBind :: (Expression -> Maybe Expression) -> Bind -> Bind
substitutionBind subs b = case b of
  NonRec (v, e) -> NonRec (v, go e)
  Rec bs        -> Rec [(var, go e) | (var, e) <- bs]
  where go = substitution subs

evalExpr :: Expression -> Expression
-- evalExpr _ | trace "evalExpr0" False = undefined -- NOTE this doesn't work with compiled code :(
evalExpr e = case e of
  EVar v          -> nvar v
  App a b         -> evalApp a b
  TypeApp a t     -> evalTypeApp a t
  Case i e t alts -> evalCase i e t alts
  Let b e         -> evalLet b e
  Bottom          -> e
  Lit{}           -> e
  Lambda{}        -> e
  TypeLambda{}    -> e
  where
    nvar :: Var -> Expression
    nvar v = case _varSort v of
      DefinedVar (Just (DefExpr e)) _
        | varIsLoop v -> Bottom
        | otherwise ->
          -- trace ("expand " <> renderPhiDef v <> "\nby\n" <> renderPhiDef e <> "\n")
          evalExpr (substDefinedVar v (DefExpr e) e)
      DefinedVar Nothing  _            ->
        error ("expanding nondefined var " <> v ^.varName . nameString)
      _                                -> EVar v
    evalTypeApp :: Expression -> Type -> Expression
    evalTypeApp e t = case evalExpr e of
      TypeLambda poly b -> substPolyVarExpr (poly, t) b
      EVar con
        | isConstructorVar con ->
          let con' = over varType (\x -> AppType x t) con in
          EVar con'
      e' -> TypeApp e' t
    napp' :: Expression -> Expression -> Expression
    napp' a b = case a of
      Bottom     -> Bottom
      Lambda v e -> evalExpr (subsVarSameType (v, b) e)
      _          -> App a b
    evalApp :: Expression -> Expression -> Expression
    evalApp a b = napp' (evalExpr a) b
    evalCase i e t alts =
      case e' of
        Bottom -> Bottom
        _ ->
          case matchExprPatterns e' alts of
            Just (Match _ rhs') -> evalExpr rhs'
            Just NoMatch        -> error "no match"
            Nothing             -> Case i e' t alts
      where e' = evalExpr e
    evalLet :: Bind -> Expression -> Expression
    evalLet b e = case b of
      NonRec (v, ve) -> evalExpr (substDefinedVar v (DefExpr ve) e)
      Rec [(v, ve)] ->
        let ve' = substDefinedVar v (DefExpr ve) ve in
        evalExpr (substDefinedVar v (DefExpr ve') e)
      -- FIXME: how to deal with a pack of mutually recursive bindings?
      Rec bs -> error "to be implemented"
        -- evalExpr (foldl' (\ac (v, ve) -> substDefinedVar v (DefExpr ve) ac) e bs)

substPolyVarExpr :: (PolyTypeVar, Type) -> Expression -> Expression
substPolyVarExpr pt@(p, t) e = case e of
  EVar v -> EVar (goVar v)
  App a b -> App (go a) (go b)
  Case cid e t alts -> Case cid (go e) (goTy t) (map goAlt alts)
  Lambda v e -> Lambda (goVar v) (go e)
  Let b e -> Let (goBind b) (go e)
  TypeLambda x e
    | x == p -> e
    | otherwise -> TypeLambda x (go e)
  TypeApp a t -> TypeApp (go a) (goTy t)
  Bottom{} -> e
  Lit{} -> e
  where
    go = substPolyVarExpr pt
    goVar = over varType goTy
    goTy = substPolyVar pt
    goPat pat = case pat of
      PatCons (PConsApp con args) -> PatCons (PConsApp (goVar con) (map goVar args))
      Wildcard -> pat
    goAlt (Alt pat e) = Alt (goPat pat) (go e)
    goBind b = case b of
      NonRec ve -> NonRec (goBind2 ve)
      Rec ves -> Rec (map goBind2 ves)
    goBind2 (v, e) = (goVar v, go e)

varIsLoop :: Var -> Bool
varIsLoop v = case _varSort v of
  DefinedVar (Just def@(DefExpr e')) _
    | EVar v == e' -> True
  _ -> False

matchExprPatterns :: Expression -> [Alt] -> Maybe MatchResult
matchExprPatterns e as = do
  c <- unfoldConsApp e
  return (matchPatterns c as)

-- | Replaces `Nothing` with Just def
substDefinedVar :: Var -> DefBody -> Expression -> Expression
substDefinedVar x def = trc $ subsVarSameType (x, x')
  where x' = EVar (definedVar x def)
        renderPhiSty :: forall p. PrettyPhi p => p -> String
        renderPhiSty = renderPhi defaultStyle { _showVarId = False, _showVarType=True}
        trc
          | True = id
          | otherwise = trace ("subsDefined " <> renderPhiSty x
                     <> " by " <> renderPhiSty def)

-- | When it returns `Nothing`, it means that none of the patterns matched,
-- hence, the case expression evaluates to `Bottom`.
--
-- Remember that if it exists, the wildcard will be the last Alt.
matchPatterns :: ConsApp -> [Alt] -> MatchResult
matchPatterns p = firstMatch . map (matchPattern p)
  where
    firstMatch [] = NoMatch
    firstMatch (x:xs) = case x of
      Match{} -> x
      _       -> firstMatch xs
    matchPattern :: ConsApp -> Alt -> MatchResult
    matchPattern ca@(ConsApp expCon expArgs) alt
      | Wildcard <- altPat = Match alt (alt ^. altExpr)
      | PatCons pat@(PConsApp altCon altArgs) <- altPat
        , (_conVar expCon) == altCon =
          let substMap = HMap.fromList $ zipExactNote errty altArgs expArgs
              errty = unlines ["failed to pattern match:"
                              , "pattern = " <> ppat
                              , "expression = " <> pargs]
              ppat = renderPhi debugStyle pat
              pargs = renderPhi debugStyle ca
          in Match alt (subsVars substMap (alt ^. altExpr))
      | otherwise = NoMatch
      where altPat = _altPat alt

subsVars :: SubsExpr c => HashMap Var Expression -> c -> c
subsVars m = subsExpr subs
  where subs e = case e of
          EVar v -> HMap.lookup v m
          _      -> Nothing

replaceVars :: SubsExpr c => HashMap Var Var -> c -> c
replaceVars = subsVars . HMap.map EVar

subsVarSameType :: SubsExpr c => (Var, Expression) -> c -> c
subsVarSameType (var, var') = subsExpr subs
  where subs e = case e of
          EVar v
            | v == var
            , _varType v == _varType var -> Just var'
          _ -> Nothing

subexpressions :: Expression -> [Expression]
subexpressions e = e : case e of
  EVar{}            -> []
  Bottom{}          -> []
  App a b           -> go a <> go b
  TypeApp a t       -> go a
  Case cid a t alts -> go a <> concatMap goAlt alts
  Lit{}             -> []
  Lambda v e        -> go e
  TypeLambda _ e    -> go e
  Let b e           -> subexpressionsBind b <> go e
  where go = subexpressions
        goAlt = go . _altExpr

equivType :: Type -> Type -> Bool
equivType ta tb = case (evalType ta, evalType tb) of
  (TypeConApp acon as, TypeConApp bcon bs) ->
    acon == bcon
    && and (zipWithExact equivType as bs)
  (PolyVarType a, PolyVarType b) -> a == b
  (FunType ax ar, FunType bx br) -> equivType ax bx && equivType ar br
  (AppType aa ab, AppType ba bb) -> equivType aa ba && equivType ab bb
  (ForAllType a ta, ForAllType b tb) ->
    -- | Unify a, b by replacing all instances of b by a
    equivType ta (substPolyVar (b, PolyVarType a) tb)
  _ -> False


-- NOTE: There are some sanity checks for debugging.
exprType :: Expression -> Type
exprType e = evalType $ case e of
  EVar v -> _varType v
  Case _ _ t alts@(a:_)
    | allSameOn equivType (t:map altType alts) -> exprType (a ^. altExpr)
    | otherwise -> let t' = evalType t in
                     error ("alts types\nevalCase type: " <> (renderPhi debugStyle t')
                          <> "\nalts:\n"
                          <> unlines [ "type = " <> renderPhi debugStyle t
                                       <> ",\nalt = " <> renderPhi debugStyle a
                                     | a <- alts, let t = altType a])
  Case _ _ _ _ -> error "empty case"
  Let _ e -> exprType e
  Lambda v e -> FunType (_varType v) (exprType e)
  TypeLambda p e -> ForAllType p (exprType e)
  TypeApp e t -> AppType (exprType e) t
  App f x ->
        let tf = exprType f
            tx = exprType x in
        case tf of
          FunType lamt ret -> ret
          PolyVarType poly ->
            error (render $ "exprType error!\n"
                    <> pretty f <> " :: " <> pretty (exprType f)
                    <> "\n@\n"
                    <> pretty x <> " :: " <> pretty (exprType x)
                    <> "\n")
          _ -> error $ render
            ("exprType Error!\nApp (lambda/argument/full expression):\n"
             <> "type of f: " <> text (show tf) <> "\n"
             <> "\nf: " <> pretty f <> "\n"
             <> "\ntype of f: " <> pretty tf <> "\n"
             <> "\nx: " <> pretty x <> "\n"
             <> "\nfullExpr: "<> pretty e)
  Bottom -> bottomType
  Lit{} -> error "literal type"
  where pretty :: forall p. PrettyPhi p => p -> Doc
        pretty = prettyPhi sty
        sty = debugStyle
        altType :: Alt -> Type
        altType = exprType . _altExpr

-- NOTE that this relies on being unique, so no other type should have a negative Id
bottomType :: Type
bottomType = ForAllType poly (PolyVarType poly)
  where
    poly = PolyTypeVar polyId polyName
    polyId = Id (-1)
    polyName = Name (Id (-2)) "bottom"

subexpressionsBind :: Bind -> [Expression]
subexpressionsBind b = case b of
  NonRec ve -> gob' ve
  Rec ves   -> concatMap gob' ves
  where
    gob' :: (Var, Expression) -> [Expression]
    gob' (_, e) = subexpressions e

instance HeadBetaReducible Expression where
  eval = evalExpr

instance SubsExpr Expression where
  subsExpr :: (Expression -> Maybe Expression) -> Expression -> Expression
  subsExpr subs = substitution subs
  universeExprs = subexpressions
