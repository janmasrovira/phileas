{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Phileas.Phi.Pretty (
  defaultStyle
  , debugStyle
  , verboseStyle
  , renderPhi
  , renderPhiDef
  , prettyPhi
  , PhiStyle(..)
  , PrettyPhi(..)
  , Styled
  ) where

import qualified Data.HashMap.Lazy  as HMap
import qualified Data.HashSet       as HSet
import qualified Data.Set           as Set
import qualified Data.Map           as Map
import           Phileas.Phi.Extra
import           Phileas.Phi.Syntax
import           Phileas.Prelude    hiding ((<>))
import           Text.PrettyPrint

type Styled = Reader PhiStyle

data PhiStyle = PhiStyle {
  _indent              :: Int
  , _showVarType       :: Bool
  , _showConsType      :: Bool
  , _showCaseType      :: Bool
  , _showLambdaVarType :: Bool
  , _showVarId         :: Bool
  , _showVarHistory    :: Bool
  } deriving (Data, Eq)
makeLenses ''PhiStyle

class PrettyPhi p where
  rprettyPhi :: p -> Styled Doc

prettyPhi :: PrettyPhi p => PhiStyle -> p -> Doc
prettyPhi s p = runReader (rprettyPhi p) s

renderPhi :: PrettyPhi p => PhiStyle -> p -> String
renderPhi sty p = render $ runReader (rprettyPhi p) sty

instance PrettyPhi Module where
  rprettyPhi = prettyModule
instance PrettyPhi Expression where
  rprettyPhi = prettyExpression
instance PrettyPhi TypeCon where
  rprettyPhi = prettyTypeCon
instance PrettyPhi Var where
  rprettyPhi = prettyVar
instance PrettyPhi Alt where
  rprettyPhi = prettyAlt
instance PrettyPhi Type where
  rprettyPhi = prettyType
instance PrettyPhi DataCon where
  rprettyPhi = prettyDataCon
instance PrettyPhi PolyTypeVar where
  rprettyPhi = prettyPolyTypeVar
instance PrettyPhi PConsApp where
  rprettyPhi = prettyPConsApp
instance PrettyPhi ConsApp where
  rprettyPhi = prettyConsApp
instance PrettyPhi HistoryEvent where
  rprettyPhi = prettyEvent
instance PrettyPhi History where
  rprettyPhi = prettyHistory
instance PrettyPhi DefBody where
  rprettyPhi = prettyDefBody

renderPhiDef :: PrettyPhi p => p -> String
renderPhiDef = renderPhi defaultStyle

instance Pretty Module where
  pPrint = withDefaultStyle . prettyModule
instance Pretty Expression where
  pPrint = withDefaultStyle . prettyExpression
instance Pretty Var where
  pPrint = withDefaultStyle . prettyVar
instance Pretty TypeCon where
  pPrint = withDefaultStyle . prettyTypeCon
instance Pretty Alt where
  pPrint = withDefaultStyle . prettyAlt
instance Pretty Type where
  pPrint = withDefaultStyle . prettyType
instance Pretty DataCon where
  pPrint = withDefaultStyle . prettyDataCon
instance Pretty PolyTypeVar where
  pPrint = withDefaultStyle . prettyPolyTypeVar
instance Pretty PConsApp where
  pPrint = withDefaultStyle . prettyPConsApp
instance Pretty ConsApp where
  pPrint = withDefaultStyle . prettyConsApp
instance Pretty HistoryEvent where
  pPrint = withDefaultStyle . prettyEvent
instance Pretty History where
  pPrint = withDefaultStyle . prettyHistory
instance Pretty DefBody where
  pPrint = withDefaultStyle . prettyDefBody

defaultStyle :: PhiStyle
defaultStyle = PhiStyle {
  _indent = 2
  , _showVarType = False
  , _showConsType = False
  , _showCaseType = False
  , _showLambdaVarType = True
  , _showVarId = False
  , _showVarHistory = False
  }

debugStyle :: PhiStyle
debugStyle = defaultStyle {
  _showVarType = True
  , _showCaseType = True
  , _showVarId = True
  , _showConsType = True
  }

verboseStyle :: PhiStyle
verboseStyle = defaultStyle {
  _showVarType = True
  , _showCaseType = True
  , _showVarId = True
  , _showConsType = True
  , _showVarHistory = True
  }

withDefaultStyle :: Styled c -> c
withDefaultStyle = flip runReader defaultStyle

showPretty :: Pretty p => p -> String
showPretty = render . pPrint

prettyModule :: Module -> Styled Doc
prettyModule m = do
  dt <- vcat <$> mapM (uncurry prettyTypeConDataTypeDef) (HMap.toList (m ^. dataTypes))
  funs <- vcat . punctuate "\n" <$> mapM (uncurry prettyTopBinding) (HMap.toList (m ^. topDefinitions))
  return (dt $+$ funs)

prettyTypeConDataTypeDef :: TypeCon -> TypeConDataDef -> Styled Doc
prettyTypeConDataTypeDef tycon tdt = case tdt of
  PrimitiveTypeCon -> do
    pt <- prettyTypeCon tycon
    return (pt <+> equals <+> "primitive type constructor")
  DefinedTypeCon dt -> prettyDataTypeDef tycon dt

prettyDataTypeDef :: TypeCon -> DataTypeDef -> Styled Doc
prettyDataTypeDef tycon dt = do
  pt <- prettyTypeCon tycon
  pd <- vcat . punctuate (text " |") <$> mapM prettyDataCon (dt ^. typeDataCons)
  return ("data" <+> pt <+> equals <+> pd)

prettyDataCon :: DataCon -> Styled Doc
prettyDataCon (DataCon v) = prettyTypedName v (v ^. varName) (v ^. varType)

prettyTypedName :: HasId i => i -> Name -> Type -> Styled Doc
prettyTypedName var n t = do
  pn <- prettyName var n
  pt <- prettyType t
  return (pn <+> "∷" <+> pt)

prettyTopBinding :: Var -> DefBody -> Styled Doc
prettyTopBinding v def = do
  sig <- prettyVar' True False v
  pn <- prettyVar v
  pd <- prettyDefBody def
  return (sig $+$ pn <+> equals <+> pd)

prettyPrimitiveDef :: PrimitiveDef -> Styled Doc
prettyPrimitiveDef p = case p of
  IntAddDef -> return "+"
  IntMulDef -> return "*"

prettyDefBody :: DefBody -> Styled Doc
prettyDefBody (DefExpr e) = prettyExpression e
prettyDefBody (DefPrimitive p) = prettyPrimitiveDef p

prettyBinding :: Var -> Expression -> Styled Doc
prettyBinding v e = prettyTopBinding v (DefExpr e)

prettyType :: Type -> Styled Doc
prettyType t = case t of
  TypeConApp con [arg]
    | isListTypeCon con -> do
        a <- prettyType arg
        return ("[" <> a <> "]")
  TypeConApp con args ->
    prettyTypeCon con <++> (hsep <$> mapM prettyTypeParen args)
  ForAllType v t -> do
    pv <- prettyPolyTypeVar v
    pt <- prettyType t
    return ("∀" <+> pv <> char '.' <+> pt)
  PolyVarType v -> prettyPolyTypeVar v
  FunType a b -> do
    pa <- prettyTypeParen a
    pb <- prettyTypeParen b
    return (pa <+> "↦" <+> pb)
  AppType a b -> do
    pa <- prettyTypeParen a
    pb <- prettyTypeParen b
    return (pa <> " @ " <> pb)

isListTypeCon :: TypeCon -> Bool
isListTypeCon con = "[]" == _nameString (_typeConName con)

prettyPolyTypeVar :: PolyTypeVar -> Styled Doc
prettyPolyTypeVar poly = prettyName poly (poly ^. polyName)

prettyTypeCon :: TypeCon -> Styled Doc
prettyTypeCon tc = prettyName tc . (^. typeConName) $ tc

prettyLambdaVar :: Var -> Styled Doc
prettyLambdaVar v = do
  t <- view showLambdaVarType
  prettyVar' t True v

prettyHistory :: History -> Styled Doc
prettyHistory (History s) = do
  contents <- hcat . punctuate ", " <$> mapM prettyAssoc (Map.toList s)
  return ("{" <> contents <> "}")
  where prettyAssoc (event, k) = do
          pe <- prettyEvent event
          return (parens (pe <> comma <> " " <> int k))

prettyCaseId :: CaseId -> Styled Doc
prettyCaseId = prettyId . unCaseId

prettyEvent :: HistoryEvent -> Styled Doc
prettyEvent e = case e of
  InductionOn cid -> ("I." <>) <$> prettyCaseId cid
  ExpandOn tcon  -> ("E." <>) <$> prettyTypeCon tcon

prettyVar' :: Bool -> Bool -> Var -> Styled Doc
prettyVar' showType useParens v = do
  hist <- asks _showVarHistory
  addHist <- if
    | hist, Just h <- getHistory v -> do
        h <- prettyHistory h
        return (<> h)
    | otherwise -> return id
  if
    | showType -> parens' . addHist <$> prettyTypedName v name' (v ^. varType)
    | otherwise -> addHist <$> prettyName v name'
  where
    parens' = if useParens then parens else id
    name = v ^. varName
    name'
      | DefinedVar Nothing _ <- v ^. varSort = over nameString (++"?") name
      | DefinedVar (Just _) _ <- v ^. varSort = over nameString (++"$") name
      | otherwise = name

prettyVar :: Var -> Styled Doc
prettyVar v = do
  c <- view showVarType
  cons <- view showConsType
  prettyVar' (c && (cons || not (isConstructorVar v))) True v

prettyTypeParen :: Type -> Styled Doc
prettyTypeParen t = case t of
  PolyVarType{} -> prettyType t
  TypeConApp _ [] -> prettyType t
  TypeConApp con [_]
    | isListTypeCon con -> prettyType t
  _ -> parens <$> prettyType t

prettyExprParen :: Expression -> Styled Doc
prettyExprParen e = case e of
  EVar{} -> prettyExpression e
  Bottom -> prettyExpression e
  _      -> parens <$> prettyExpression e

prettyExpression :: Expression -> Styled Doc
prettyExpression e = case e of
  EVar v -> prettyVar v
  App{} ->
    let (f, args) = unfoldApp e in
      hsep <$> mapM prettyExprParen (f:args)
  TypeApp e t -> do
    pe <- prettyExprParen e
    pt <- prettyTypeParen t
    return (pe <+> char '@' <+> pt)
  Case i e ty alts -> do
    pa <- prettyAlts alts
    pe <- go e
    addCaseId <- ifM (asks _showVarHistory)
      (do
          i' <- prettyCaseId i
          return (<> ("[" <> i' <> "]")))
      (return id)
    addCaseType <- ifM (asks _showCaseType)
      (do
          tyCase <- prettyType ty
          return (parens . (<+> ("∷" <+> tyCase))))
      (return id)
    ind <- view indent
    return (hang (addCaseType (addCaseId "case") <+> pe <+> "of") ind pa)
  Lit l -> prettyLiteral l
  Let b e -> do
    pb <- prettyBind b
    pe <- go e
    return ("let" <+> pb $+$ "in" <+> pe)
  TypeLambda v e -> do
    pv <- prettyPolyTypeVar v
    pe <- go e
    return (text "λ@" <> pv <> char '.' <+> pe)
  Lambda v e -> do
    pv <- prettyLambdaVar v
    pe <- go e
    return (char 'λ' <> pv <> char '.' <+> pe)
  Bottom -> return "⊥"
  where go = prettyExpression

prettyBind :: Bind -> Styled Doc
prettyBind (NonRec b) = uncurry prettyBinding b
prettyBind (Rec bs)   = vcat <$> mapM (uncurry prettyBinding) bs

prettyLiteral :: Literal -> Styled Doc
prettyLiteral l = case l of
  IntLit i     -> return (integer i)
  UndefinedLit -> return "UndefinedLit"

prettyAlts :: [Alt] -> Styled Doc
prettyAlts = fmap vcat . mapM prettyAlt

prettyId :: Id -> Styled Doc
prettyId = return . int . (^. unId)

prettyAlt :: Alt -> Styled Doc
prettyAlt (Alt p e) = do
  pp <- prettyPattern p
  pe <- prettyExpression e
  return (pp <+> "→" <+> pe)

prettyPattern :: Pattern -> Styled Doc
prettyPattern Wildcard         = return "_"
prettyPattern (PatCons pat) = prettyPConsApp pat

prettyPConsApp :: PConsApp -> Styled Doc
prettyPConsApp (PConsApp con args) = fmap hsep (mapM prettyVar (con:args))

prettyConsApp :: ConsApp -> Styled Doc
prettyConsApp (ConsApp con args) = do
  pcon <- prettyDataCon con
  pargs <- hsep <$> mapM prettyExprParen args
  return (pcon <+> pargs)

prettyName :: HasId x => x -> Name -> Styled Doc
prettyName x name = do
  showId <- view showVarId
  let putId = if showId then (<> ("!" <> int (getId x ^. unId))) else id
  return . putId . text . (^. nameString) $ name

(<++>) :: Monad m => m Doc -> m Doc -> m Doc
(<++>) = liftM2 (<+>)
