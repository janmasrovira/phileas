{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData          #-}
{-# LANGUAGE TemplateHaskell     #-}
module Phileas.Core where

import           Data.Graph
import qualified Data.HashMap.Lazy     as HMap
import qualified Data.HashSet          as HSet
import qualified Data.Map              as Map
import qualified Data.Set              as Set
import           Phileas.Phi
import           Phileas.Phi.Pretty
import           Phileas.Phi.Semantics
import           Phileas.Prelude

class Monad m => MonadPhi m where
  typeConDataDef :: TypeCon -> m TypeConDataDef
  newId :: m Id
  getOptions :: m PhileasOptions

data PhileasOptions = PhileasOptions {
  _debugTrace          :: Bool
  , _traceStyle        :: PhiStyle
  , _proveFalse        :: Bool
  , _maxInductionDepth :: Int
  , _timeoutMicro      :: Maybe Int
  } deriving (Data, Eq)

defaultOptions = PhileasOptions {
  _debugTrace = False
  , _traceStyle = defaultStyle
  , _proveFalse = True
  , _maxInductionDepth = 2
  , _timeoutMicro = Nothing
  }

testOptions = PhileasOptions {
  _debugTrace = False
  , _traceStyle = debugStyle
  , _proveFalse = True
  , _maxInductionDepth = 1
  , _timeoutMicro = Nothing
  }

instance Pretty Equality where
  pPrint Eq{..} = pPrint _eqLeft <+> "≡" <+> pPrint _eqRight

data Proof =
  Refl Equality
  | Induction Expression [(DataCon, Proof)]
  | Cong DataCon [Proof]
  | FactorApp Expression [Proof]
  | HypCong Proof
  | UseEq Equality (Maybe String)
  | ExtensionalEq Var Proof
  | FactorAlts Proof
  | AbsurdHyp Property
  | Absurd Property
  deriving (Eq)

instance Pretty Proof where
  pPrint p = case p of
    Refl eq -> "Reflexivity " <> pPrint eq
    Induction e ps -> hang2 ("Induction on " <> pprintExpr e) (vcat (map pprintCase ps))
      where pprintCase (dcon, prf) = hang2 (prettyPhi conSty (_conVar dcon)) (pPrint prf)
            conSty = defaultStyle
            pprintExpr e = "(" <> prettyPhi defaultStyle e <> " ∷ "
              <> prettyPhi defaultStyle (exprType e) <> ")"
    Cong dcon ps -> "Congruence on " <> pPrint (_conVar dcon)
      $+$ nest 2 (vcat (map pPrint ps))
    FactorApp fun ps -> "FactorApp on " <> pPrint fun
      $+$ nest 2 (vcat (map pPrint ps))
    HypCong ps -> "Congruence on hypotheses " $+$ pPrint ps
    UseEq eq name -> case name of
      Just name -> "Close using hypothesis " <> text name <> ": " <> pPrint eq
      Nothing -> "Close using hypothesis " <> pPrint eq
    ExtensionalEq var p -> "Extensional equality introduces "
      <> pPrint var <> " ∷ " <> pPrint (_varType var) $+$ pPrint p
    FactorAlts p -> "factor"
    AbsurdHyp p -> "Contradiction in hypothesis: " <> pPrint p
    Absurd p -> "Property proven False. Contradiction: " <> pPrint p
    where hang2 a b = a $+$ (nest 2 b)

instance Pretty EvalEquality where
  pPrint (EvalEq lexpr rcapp) = pPrint (Eq lexpr (foldConsApp rcapp))

data Theory = Theory {
  _theoModule     :: Module
  , _theoTheorems :: [Theorem]
  }

data Theorem = Theorem
  { _thmProof    :: Proof
  , _thmProperty :: Property
  } deriving (Eq)

data Property = Property {
  _propForAllVars    :: HashSet Var
  , _propEquality    :: Equality
  , _propAntecedents :: [Property]
  , _propName        :: Maybe String
  }
  deriving (Eq, Data, Show)

data Quantified a = Quantified {
  _forallVars   :: HashSet Var
  , _quantThing :: a
  }

data Equality = Eq {
  _eqLeft    :: Expression
  , _eqRight :: Expression
  }
  deriving (Show, Data)

data Blocking =
  CaseBlocking CaseId Expression
  | Expandable TypeCon Expression
  deriving (Eq)


-- | An eval equality is an equality where the left expression is NOT a
-- 'ConsApp' whereas the right expression IS. Since the the right expression is
-- further evaluated, using this equality as a rewrite rule whenever possible
-- (from left to right) should help the proof advance.
--
-- TODO: consider extending this idea to EvalProperty.
data EvalEquality = EvalEq {
  _evEqLeft    :: Expression
  , _evEqRight :: ConsApp
  }
  deriving (Show, Data, Eq)

fromEquality :: Equality -> Maybe EvalEquality
fromEquality Eq{..} = case (unfoldConsApp _eqLeft, unfoldConsApp _eqRight) of
  (Nothing, Just r) -> Just (EvalEq _eqLeft r)
  (Just r, Nothing) -> Just (EvalEq _eqRight r)
  _                 -> Nothing

instance Eq Equality where
  a == b = eqAux a b || eqAux (flipEq a) b
    where eqAux a b = _eqLeft a == _eqLeft b && _eqRight a == _eqRight b
          flipEq :: Equality -> Equality
          flipEq (Eq a b) = Eq b a

makeLenses ''Equality
makeLenses ''Property
makeLenses ''Theorem

instance HasFreeVars Equality where
  freeVars e = freeVars (e ^. eqLeft) <> freeVars (e ^. eqRight)

instance HasFreeVars Property where
  freeVars Property{..} = freeVars _propEquality <> mconcatMap freeVars _propAntecedents

instance Pretty Property where
  pPrint pred = prettyPhi defaultStyle pred

instance PrettyPhi Property where
  rprettyPhi pred@Property{..} = aux <$> ask
    where aux sty@PhiStyle{..} =
             quantification
            (hsep (punctuate " ⇒" ((map pPrint _propAntecedents) `snoc` pPrint _propEquality)))
            where
              pPrint :: (forall p. PrettyPhi p => p -> Doc)
              pPrint = prettyPhi sty
              quantification b
                | null allVars = b
                | otherwise = "∀" <> hsep (map pPrint (HSet.toList _propForAllVars))
                              <> "." <> parens b
                where allVars = HSet.toList _propForAllVars

instance PrettyPhi Equality where
  rprettyPhi Eq{..} = aux <$> ask
    where aux sty@PhiStyle{..} = pPrint _eqLeft <+> "≡" <+> pPrint _eqRight
            where
              pPrint :: (forall p. PrettyPhi p => p -> Doc)
              pPrint = prettyPhi sty

-- | Returns Just if it was applied at least one time.
-- applyEvalEquality :: EvalEquality -> Expression -> Maybe Expression
applyEvalEquality :: (SubsExpr a, Eq a) => EvalEquality -> a -> Maybe a
applyEvalEquality EvalEq{..} e
  | e == e' = Nothing
  | otherwise = Just e'
  where e' = subsExprEq _evEqLeft consExpr e
        consExpr = addEvents (history _evEqLeft) (foldConsApp _evEqRight)

applyFactorAlts :: (SubsExpr a, Eq a) => a -> Maybe a
applyFactorAlts e
  | e == e' = Nothing
  | otherwise = Just e'
  where e' = factorAlts e

instance SubsExpr Equality where
  subsExpr subs = over eqRight (subsExpr subs)
                  . over eqLeft (subsExpr subs)
  universeExprs Eq{..} = universeExprs _eqLeft <> universeExprs _eqRight

instance SubsExpr Bind where
  subsExpr = substitutionBind
  universeExprs = subexpressionsBind

instance SubsExpr Property where
  subsExpr subs = over propEquality (subsExpr subs)
                 . over propAntecedents (map (subsExpr subs))
  universeExprs p = universeExprs (_propEquality p)
    <> concatMap universeExprs (_propAntecedents p)

class HasHistory c where
  default hasHappened :: HistoryEvent -> c -> Int
  hasHappened :: HistoryEvent -> c -> Int
  hasHappened e =  fromMaybe 0 . Map.lookup e . _unHistory . history
  history :: c -> History
  addEvents :: History -> c -> c
  default addEvent :: HistoryEvent -> c -> c
  addEvent :: HistoryEvent -> c -> c
  addEvent = addEvents . singleEvent

instance HasHistory History where
  history h = h
  addEvents h l = h <> l

instance HasHistory VarSort where
  history s = case s of
    UniversalVar h -> h
    _              -> mempty
  addEvents e s = case s of
    UniversalVar h -> UniversalVar (addEvents e h)
    _              -> s

instance HasHistory Var where
  history v = history (v ^. varSort)
  addEvents e = over varSort (addEvents e)

instance HasHistory Expression where
  history e = mconcat [ history v | EVar v <- universeExprs e]
  addEvents ev = subsExpr sub
    where sub e = case e of
            EVar v -> Just $ EVar (addEvents ev v)
            _      -> Nothing

instance HasHistory PConsApp where
  history (PConsApp v args) = mconcatMap history args
  addEvents ev (PConsApp con args) = PConsApp con (map (addEvents ev) args)

instance HasHistory ConsApp where
  history (ConsApp _ args) = mconcatMap history args
  addEvents ev (ConsApp con args) = (ConsApp con (map (addEvents ev) args))

instance HasHistory Equality where
  history e = mconcatMap history [(e ^. eqLeft),  (e ^. eqRight)]
  addEvents e = over eqLeft (addEvents e) . over eqRight (addEvents e)

instance HasHistory Property where
  history p = history (p ^. propEquality) <> mconcatMap history (p ^. propAntecedents)
  addEvents e = over propEquality (addEvents e) . over propAntecedents (map (addEvents e))

newName :: MonadPhi m => m Name
newName = do
  _nameId <- newId
  let _nameString = "var." <> show (_unId _nameId)
  return Name {..}

newUniversalVarFromVar :: MonadPhi m => Var -> m Var
newUniversalVarFromVar v = case _varSort v of
  UniversalVar h -> newUniversalVar h (_varType v)
  _              -> error "argument must be a universal var"

newUniversalVar :: MonadPhi m => History -> Type -> m Var
newUniversalVar hist _varType = do
  _varName <- newName
  _varId <- newId
  let _varSort = UniversalVar hist
  return Var{..}

-- applies newly created variables to a constructor until it is fully applied
saturateConstructor :: MonadPhi m => History -> DataCon -> m PConsApp
saturateConstructor hist (_conVar -> con) =
  case sig of
    TypeSig [] args _ret ->
      PConsApp con <$> mapM (newUniversalVar hist) args
    TypeSig free _ _ ->
      error "cannot instantiate constructor, it has universally quantified types"
  where sig = funTypeSig (_varType con)

-- | Instantiates each constructor of the given type, if possible.
--
-- It will fail if it cannot be evaluated to a TypeConApp.
casesOfType :: MonadPhi m => History -> Type -> m (Maybe (TypeCon, [PConsApp]))
casesOfType hist t = case evalType t of
  TypeConApp con args -> do
    def <- typeConDataDef con
    case def of
      PrimitiveTypeCon -> return Nothing
      DefinedTypeCon dt ->
        Just . (con,) <$> mapM (saturateConstructor hist) (specializedDataCons args dt)
  _                   -> return Nothing
  where
    specializedDataCons :: [Type] -> DataTypeDef -> [DataCon]
    specializedDataCons targs = map specializeVarType . _typeDataCons
      where
        specializeVarType :: DataCon -> DataCon
        specializeVarType = over (conVar . varType) (flip applyType targs)

simplifyProperty :: Property -> Property
simplifyProperty
  | otherwise =
  simplify
  . eval
  where
    simplify =
      id
      . subsExprRec factorExprAlts
      . subsExprRec simplifyCase
      . subsExprRec betaReduce
      . removeTrivialHypsPred
      . removeVarSynonimsPred
      . simplifyAntecedents
    simplifyAntecedents = over propAntecedents (map simplifyProperty)

-- TODO: what about history?
removeVarSynonimsPred :: Property -> Property
removeVarSynonimsPred p@Property{..} =
  subsVars subsMap p
  where
    subsMap = HMap.fromList [ (v, EVar rep) | (rep, vs) <- classes, v <- vs ]
    classes :: [(Var, [Var])]
    classes = map mkClass graph
    mkClass s = case s of
      AcyclicSCC v -> (v, [])
      CyclicSCC (v:vs) -> (v, vs)
      CyclicSCC [] -> error $ "If Data.Graph should used NonEmpty "
                      <> "I could avoid this dead branch"
    graph :: [SCC Var]
    graph = stronglyConnComp edges
    edges :: [(Var, Var, [Var])]
    edges = [ (v, v, outs) | eqs <- equalities
              , let ((v, _) :| _) = eqs
                    outs = map snd (toList eqs) ]
    equalities =
      groupAllWith fst
      . concatMap duplicate
      . mapMaybe getVarSynonim
      $ _propAntecedents
    duplicate (a, b) = [(a, b), (b, a)]
    getVarSynonim Property{..}
      | [] <- _propAntecedents
      , Eq (EVar x) (EVar y) <- _propEquality
      , not (isConstructorVar x), not (isConstructorVar y)
      = Just (x, y)
      | otherwise = Nothing

removeTrivialHypsPred :: Property -> Property
removeTrivialHypsPred =
  over propAntecedents (mapMaybe keep)
  where keep Property{..}
          | [] <- _propAntecedents
          , Eq a b <- _propEquality, a == b = Nothing
        keep p = Just p

-- | Induction on a case expression.
-- The arguments are:
-- 1. The blocking expression
-- 2. The goal equality.
-- 3. An environment which supports substitution of expressions
--    and has a history of events.
--
-- It returns Nothing when induction was not possible.
-- If it succeeds, it returns a list of cases that must be proved separately.
-- Each case is paired with its induction hypotheses and equality hypotheses.
inductOnExpression :: forall m a. (HasHistory a, SubsExpr a, MonadPhi m)
  => Blocking -> Equality -> a -> m (Maybe [ (DataCon, [Property], a) ])
inductOnExpression block eq a = do
  consApps <- casesOfType hist ety
  case consApps of
    Nothing -> return Nothing
    Just (_, consApps) -> Just <$> sequence
      [ do
          return (con, hyps, a') | pcons <- consApps
        , let hyps = indHypotheses pcons
              e' = addIndEvent (foldConsApp (toConsApp pcons))
              a' = subsExprEq e e' a
              mrender :: forall a. PrettyPhi a => a -> String
              mrender = renderPhi defaultStyle
              con = let ConsApp dcon _ = toConsApp pcons in dcon
        ]
  where
    e = blockingExpr block
    ety = case e of
      EVar v
        | isConstructorVar v -> error ("cannot induct on consvar: " <> renderPhiDef e)
      _ -> exprType e
    addIndEvent :: forall a. HasHistory a => a -> a
    addIndEvent = addEvent event
    indHypotheses :: PConsApp -> [Property]
    indHypotheses (PConsApp con args) = map addIndEvent (mapMaybe mkHyp args)
      where mkHyp arg
              | arg ^. varType == ety = Just Property {
                  _propForAllVars = mempty
                  , _propEquality = subsExprEq e (EVar arg) eq
                  , _propAntecedents = mempty
                  , _propName = Nothing
                  }
              | otherwise = Nothing
    hist = history a
    event = blockingEvent block

blockingEvent :: Blocking -> HistoryEvent
blockingEvent (CaseBlocking cid _) = InductionOn cid
blockingEvent (Expandable tcon _)  = ExpandOn tcon

instance HeadBetaReducible Equality where
  eval (Eq l r) = Eq (eval l) (eval r)

instance HeadBetaReducible Property where
  eval Property{..} = Property {
    _propForAllVars
    , _propName
    , _propEquality = eval _propEquality
    , _propAntecedents = map eval _propAntecedents
    }

factorAlts :: SubsExpr a => a -> a
factorAlts = subsExpr factorExprAlts

-- | converts an expression such as
-- case e of
-- x -> F (G x)
-- y -> F (G y)
--
-- into
--
-- F (G (case e of
-- x -> x
-- y -> y))
factorExprAlts :: Expression -> Maybe Expression
factorExprAlts e = case e of
  Case cid ce t alts -> case factor (map _altExpr alts) of
    Nothing -> Nothing
    Just (ty', app, es) -> Just (fromMaybe e' (factorExprAlts e'))
      where e' = app $ Case cid ce ty' (zipWithExact (set altExpr) es alts)
  _                 -> Nothing
  where factor :: [Expression] -> Maybe (Type, Expression -> Expression, [Expression])
        factor (App (consExpr -> Just cons) x:as) = case mapM strip as of
          Just as' -> case exprType cons of
                       FunType ty _ -> Just (ty, App cons, x:as')
                       _            -> error "unexpected type"
          Nothing  -> Nothing
          where
                strip (App g x)
                  | cons == g = Just x
                  | otherwise = Nothing
                strip _ = Nothing
        factor _ = Nothing
        consExpr e = case e of
          EVar v -> guard (isConstructorVar v) >> return e
          _      -> Nothing

predTopExpressions :: Property -> [Expression]
predTopExpressions = concatMap eqExprs . predEqualities
  where eqExprs (Eq l r) = [l, r]

predEqualities :: Property -> [Equality]
predEqualities Property{..} = _propEquality : concatMap predEqualities _propAntecedents

blockingExpr :: Blocking -> Expression
blockingExpr (CaseBlocking _ e) = e
blockingExpr (Expandable _ e)   = e

inductionCandidates :: [Expression] -> [Blocking]
inductionCandidates = nubBy ((==)`on`blockingExpr) . catMaybes . concatMap candidatesOfExpr
  where
    candidatesOfExpr exp = case exp of
      App a b           -> go a
      Case cid e t alts -> [mkBlocking (cid, e)] <> go e
      TypeLambda _ e    -> go e
      TypeApp e t       -> go e
      Let{}             -> []
      EVar{}            -> []
      Lit{}             -> []
      Lambda{}          -> []
      Bottom            -> []
      where go = candidatesOfExpr
            mkBlocking :: (CaseId, Expression) -> Maybe Blocking
            mkBlocking (cid, e)
              | Nothing <- unfoldConsApp e
              = Just (CaseBlocking cid e)
              | otherwise = Nothing

expandableCandidates :: Equality -> Maybe Blocking
expandableCandidates Eq{..} =
  case (unfoldConsApp _eqLeft, unfoldConsApp _eqRight) of
      (Just _, Nothing)  -> Just (Expandable (getTypeCon _eqRight) _eqRight)
      (Nothing , Just _) -> Just (Expandable (getTypeCon _eqLeft) _eqLeft)
      _                  -> Nothing
  where
    getTypeCon :: Expression -> TypeCon
    getTypeCon e = case exprType e of
      TypeConApp tc _ -> tc
      _               -> error "unexpected type"

-- | F ((G x) x') = F ((G y) y') returns Just [x = y, x' = y']
equalityCong :: Equality -> Maybe (DataCon, [Equality])
equalityCong eq@Eq{..} =
  case (unfoldConsApp _eqLeft, unfoldConsApp _eqRight) of
    (Just (ConsApp con args), Just (ConsApp con2 args2))
      | con == con2 ->
        let eqs = zipWithExact Eq args args2
        in Just (con, eqs)
    _ -> Nothing
  where getDataCon = getVar >=> mkDataCon
