{-|
This module replaces the Prelude module in base.

It is supposed to be imported in every module of this project.
-}
module Phileas.Prelude
  (
    module Control.Applicative
  , module Control.Monad
  , module Control.Monad.Except
  , module Control.Monad.Extra
  , module Control.Monad.Identity
  , module Control.Monad.Reader
  , module Control.Monad.State.Lazy
  , module Control.Monad.Trans
  , module Control.Monad.Trans.Maybe
  , module Data.Data
  , module Data.Either
  , module Data.Either.Extra
  , module Data.Foldable
  , module Data.Function
  , module Data.Generics.Uniplate.Operations
  , module Data.HashMap.Lazy
  , module Data.HashSet
  , module Data.Hashable
  , module Data.List.Extra
  , module Data.List.NonEmpty
  , module Data.Map.Lazy
  , module Data.Maybe
  , module Data.Proxy
  , module Data.Semigroup
  , module Data.Set
  , module Lens.Micro.Platform
  , module Prelude
  , module Rainbow
  , module Safe
  , module Safe.Exact
  , module Text.Pretty.Simple
  , module Text.PrettyPrint.HughesPJClass
  -- Monad Extra
  , fromMaybeM
  -- NonEmpty Extra
  , snocL1
  -- Safe
  , headMayDefault
  , firstJustDefaultM
  -- List
  , nubHash
  , allSameOn
  , difference
  , differenceOn
  -- * Lifted IO
  , print
  , readFile
  , putStr
  , putStrLn
  , ioError
  , printPrettyShow
  , timeout
  -- * Monad Classes
  , MonadBase(..)
  -- * Debug
  , assert
  , trace
  , traceId
  , traceShowId
  , traceStack
  , traceM
  , traceIO
  -- * Rainbow
  , Rainbow(..)
  , putChunkLn
  , putChunk
  , putChunks
  , unlinesChunks
  )
where

import           Control.Applicative
import           Control.Exception                 hiding (ioError)
import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.Extra
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.State.Lazy
import           Control.Monad.Trans
import           Control.Monad.Trans.Maybe
import           Data.Data                         (Data, Typeable)
import           Data.Either
import           Data.Either.Extra
import           Data.Foldable
import           Data.Function
import           Data.Generics.Uniplate.Operations
import           Data.Hashable
import           Data.HashMap.Lazy                 (HashMap)
import qualified Data.HashMap.Lazy                 as HMap
import           Data.HashSet                      (HashSet)
import qualified Data.HashSet                      as HSet
import           Data.List.Extra                   hiding (group, groupBy)
import           Data.List.NonEmpty                (NonEmpty (..), group,
                                                    groupAllWith, groupBy,
                                                    nonEmpty, (<|))
import qualified Data.List.NonEmpty                as L1
import           Data.Map.Lazy                     (Map)
import           Data.Maybe
import           Data.Proxy
import           Data.Semigroup                    hiding (option)
import           Data.Set                          (Set)
import           Debug.Trace                       hiding (traceIO)
import qualified Debug.Trace                       as D
import           Lens.Micro.Platform
import           Prelude                           hiding (groupBy, ioError,
                                                    print, putStr, putStrLn,
                                                    readFile)
import qualified Prelude                           as P
import           Rainbow                           (Chunk, chunk, fore, only256)
import qualified Rainbow                           as R
import           Safe                              hiding (at)
import           Safe.Exact
import qualified System.Timeout                    as Timeout
import           Text.Pretty.Simple                (pShow)
import qualified Text.Pretty.Simple                as Pretty
import           Text.PrettyPrint.HughesPJClass    hiding (empty, (<>))

print :: (MonadIO m, Show a) => a -> m ()
print = liftIO . P.print

readFile :: MonadIO m => FilePath -> m String
readFile = liftIO . P.readFile

putStr :: MonadIO m => String -> m ()
putStr = liftIO . P.putStr

ioError :: MonadIO m => IOError -> m a
ioError = liftIO . P.ioError

putStrLn :: MonadIO m => String -> m ()
putStrLn = liftIO . P.putStrLn

fromMaybeM :: Monad m => m b -> m (Maybe b) -> m b
fromMaybeM r = maybeM r return

snocL1 :: NonEmpty a -> a -> NonEmpty a
snocL1 (h :| t) a = (h :| t ++ [a])

allSameOn :: (a -> a -> Bool) -> [a] -> Bool
allSameOn eq []     = True
allSameOn eq (x:xs) = all (eq x) xs

headMayDefault :: p -> [p] -> p
headMayDefault d []    = d
headMayDefault _ (h:_) = h

firstJustDefaultM :: Monad m => m b -> [m (Maybe b)] -> m b
firstJustDefaultM d l = catMaybes <$> sequence l >>= headMayDefault d . map return

printPrettyShow :: (Show a, MonadIO m) => a -> m ()
printPrettyShow = Pretty.pPrint

nubHash :: (Hashable a, Eq a) => [a] -> [a]
nubHash = HSet.toList . HSet.fromList

traceIO :: MonadIO m => String -> m ()
traceIO = liftIO . D.traceIO

putChunkLn :: (MonadIO m, R.Renderable a) => Chunk a -> m ()
putChunkLn = liftIO . R.putChunkLn

putChunk :: (MonadIO m, R.Renderable a) => Chunk a -> m ()
putChunk = liftIO . R.putChunk

putChunks :: (MonadIO m, R.Renderable a) => [Chunk a] -> m ()
putChunks = mapM_ putChunk

unlinesChunks :: [[Chunk String]] -> [Chunk String]
unlinesChunks = (`snoc` nl) . intercalate [nl]
  where nl = chunk "\n"

difference :: Eq a => [a] -> [a] -> [a]
difference = differenceOn id

differenceOn :: Eq b => (a -> b) -> [a] -> [a] -> [a]
differenceOn f a b' = filter (not . (`elem`b) . f) a
  where b = map f b'

timeout :: MonadIO m => Int -> IO a -> m (Maybe a)
timeout micro x = liftIO $ Timeout.timeout micro x

class Rainbow a where
  type RainbowStyle a :: *
  rainbowDef :: a -> [Chunk String]
  rainbow :: RainbowStyle a -> a -> [Chunk String]

instance Semigroup (Chunk String) where
  (<>) = mappend

class MonadBase m where
  type BaseMonad m :: (* -> *)
  liftBase :: BaseMonad m a -> m a
