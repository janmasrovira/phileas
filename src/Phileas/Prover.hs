{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData        #-}
{-# LANGUAGE TemplateHaskell   #-}
module Phileas.Prover (
  parseFilteredModule
  , proveModule
  , proveModuleNoTrace
  -- , proveFile
  , ProveResult
  , resultAnswer
  , ProverAnswerStyle(..)
  , PhileasOptions(..)
  , ProverAnswer(..)
  , Interact
  , FilteredModule
  , Phileas
  , defaultOptions
  , testOptions
  , filterModule
  , getTheorem
  , provenTrue
  , provenFalse
  , _properties
  -- * Runners
  , runPhileasIO
  , runPhileasTimeoutIO
  , runPhileasNoTrace
  ) where

import qualified Data.HashMap.Lazy                as HMap
import qualified Data.HashSet                     as HSet
import           Language.GhcCore.Backend.Phi
import           Language.Haskell.Backend.GhcCore
import           Phileas.Core
import           Phileas.Phi
import           Phileas.Phi.Semantics
import           Phileas.Prelude
import qualified Rainbow                          as R

data ProverAnswerStyle = ProverAnswerStyle {
  _showProof      :: Bool
  , _showProperty :: Bool
  }

data ProveResult =
  ProveResult {
  _property :: Property
  , _answer :: ProverAnswer
  }
  | Timeout {
      _property :: Property
      }
  deriving (Eq)

resultAnswer Timeout{} = Nothing
resultAnswer ProveResult{..} = Just _answer

defTheoremStyle :: ProverAnswerStyle
defTheoremStyle =
  ProverAnswerStyle {
  _showProof = False
  , _showProperty = False
  }

showPropName :: Property -> String
showPropName Property{..} = fromMaybe "" _propName

predProof :: ProverAnswerStyle -> Property -> Maybe Proof -> [Chunk String]
predProof ProverAnswerStyle{..} prop mayproof =
  unlinesChunks (catMaybes [rpred, rproof])
  where
    rproof
      | Just proof <- mayproof
      , _showProof = Just [R.underline (chunk "Proof:\n")
                     , chunk (prettyShow proof)]
      | otherwise = Nothing
    rpred
      | _showProperty = Just [R.underline (chunk "Property:\n")
                        , chunk (prettyShow prop)]
      | otherwise = Nothing

instance Rainbow ProveResult where
  type RainbowStyle ProveResult = ProverAnswerStyle
  rainbowDef = rainbow defTheoremStyle
  rainbow sty ProveResult{..} = rainbow sty _answer
  rainbow sty Timeout{..} =
      map (fore color) [ chunk (showPropName _property) & R.bold, chunk ": "
      , chunk "The prover run out of time.\n" ]
      ++ (predProof sty _property Nothing)
    where
      color = R.red R.<> only256 R.brightRed

instance Rainbow ProverAnswer where
  type RainbowStyle ProverAnswer = ProverAnswerStyle
  rainbowDef = rainbow defTheoremStyle
  rainbow :: ProverAnswerStyle -> ProverAnswer -> [Chunk String]
  rainbow sty@ProverAnswerStyle{..} ans = case ans of
    FoundFalse thm@Theorem{..} ->
      map (fore color) [ chunk (showPropName _thmProperty) & R.bold, chunk ": "
      , chunk "The property was proven false.\n" ]
      ++ (predProof sty _thmProperty (Just _thmProof))
    Dunno prop    ->
      [chunk (showPropName prop) & fore color & R.bold, chunk ": "
      , chunk ("The prover was unable to reach a conclusion.\n") & fore color]
      ++ (predProof sty prop Nothing)
    FoundTrue thm@Theorem{..} ->
      map (fore color) [ chunk (showPropName _thmProperty) & R.bold, chunk ": "
      , chunk "The property was proven true.\n" ]
      ++ (predProof sty _thmProperty (Just _thmProof))
    where

      color = case ans of
                FoundTrue{}  -> R.green R.<> only256 R.brightGreen
                FoundFalse{} -> R.red R.<> only256 R.brightRed
                _            -> R.yellow R.<> only256 R.brightYellow

instance Pretty Theorem where
  pPrint Theorem{..} = pPrint _thmProperty $+$ pPrint _thmProof

instance Pretty ProverAnswer where
  pPrint a = case a of
    FoundFalse thm -> "The given property is false"
    Dunno prop     -> "The prover was unable to reach a conclusion"
    FoundTrue thm  -> "The property was proven true" $+$ pPrint thm


equalityRefl :: Equality -> Maybe Proof
equalityRefl e@Eq{..}
  | _eqLeft == _eqRight = Just (Refl e)
  | otherwise = Nothing

data ProverState = ProverState {
  _nextId :: Id
  }

data ProverEnv = ProverEnv {
  _goal      :: Property
  , _fmodule :: FilteredModule
  , _options :: PhileasOptions
  } deriving (Data, Eq)

data FilteredModule = FModule {
  _properties :: [Property]
  , _pmodule  :: Module
  } deriving (Data, Eq)

data ProverAnswer
  = FoundFalse Theorem
  | Dunno Property
  | FoundTrue Theorem
  deriving (Eq)

instance Ord ProveResult where
  compare (Timeout a) (Timeout b) = (compare `on` _propName) a b
  compare (Timeout a) _ = GT
  compare (ProveResult _ a) (ProveResult _ b) = compare a b
  compare ProveResult{} Timeout{} = LT

instance Ord ProverAnswer where
  compare = compare `on` cmp
    where cmp a = (num a, name a)
          num FoundTrue{}  = 0
          num FoundFalse{} = 1
          num Dunno{}      = 2
          name (Dunno p)      = _propName p
          name (FoundTrue t)  = _propName . _thmProperty $ t
          name (FoundFalse t) = _propName . _thmProperty $ t

provenTrue :: ProverAnswer -> Bool
provenTrue FoundTrue{} = True
provenTrue _           = False

provenFalse :: ProverAnswer -> Bool
provenFalse FoundFalse{} = True
provenFalse _            = False

getTheorem :: ProverAnswer -> Maybe Theorem
getTheorem (FoundTrue t)  = Just t
getTheorem (FoundFalse t) = Just t
getTheorem _              = Nothing

type Prover m = StateT ProverState (ReaderT ProverEnv m)
type ProverIO = Prover Interact
type ProverPure = Prover Identity
type Phileas = ReaderT PhileasOptions

-- | All IO interactions of the prover should be contained in this class.
class Monad m => MonadInteractions m where
  traceM' :: String -> m ()

newtype Interact a = Interact {_runInteract :: IO a}
  deriving (Functor, Applicative, Monad)

runInteract :: MonadIO m => Interact a -> m a
runInteract = liftIO . _runInteract

instance MonadInteractions Interact where
  traceM' :: String -> Interact ()
  traceM' = Interact . traceIO
    where addTag msg = "traceM': " <> msg

instance MonadInteractions Identity where
  traceM' = const (return ())

instance Monad m => MonadBase (Prover m) where
  type BaseMonad (Prover m) = m
  liftBase = lift . lift

instance Monad m => MonadBase (Phileas m) where
  type BaseMonad (Phileas m) = m
  liftBase = lift

makeLenses ''ProverEnv
makeLenses ''PhileasOptions
makeLenses ''FilteredModule
makeLenses ''ProverState

instance HasHistory ProverEnv where
  history ProverEnv{..} = history _goal
  addEvents e = over goal (addEvents e)

instance SubsExpr ProverEnv where
  subsExpr subs = over goal (subsExpr subs)
  universeExprs = universeExprs . _goal

instance SubsExpr DefBody where
  subsExpr s b = case b of
    DefExpr e      -> DefExpr (subsExpr s e)
    DefPrimitive _ -> b
  universeExprs b = case b of
    DefExpr e      -> universeExprs e
    DefPrimitive _ -> []

instance Monad m => MonadPhi (Prover m) where
  -- typeConDataDef :: TypeCon -> (Prover m) TypeConDataDef
  typeConDataDef tycon =
    HMap.lookupDefault err tycon <$> view (fmodule . pmodule . dataTypes)
    where err = error $ "type constructor not found: " ++ prettyShow tycon
  newId = do
    i <- use nextId
    modifying nextId succ
    return i
  getOptions = view options

-- | Collects the properties of the modules and removes its definitions.
filterModule :: Module -> Either String FilteredModule
filterModule m0 = do
  _varIdentical <- findVarIdentical
  _varImplies <- findVarImplies
  _varForAll <- findVarForAll
  let _properties = moduleProperties _varImplies _varIdentical _varForAll m
      _pmodule = over topDefinitions (HMap.filterWithKey (curry (not . defIsProperty))) m
  return FModule{..}
  where
    m :: Module
    m = over topDefinitions replacePatError m0
    -- | Replaces occurrences of pattern errors by `bottom`
    replacePatError = transformBi subs
      where subs e
              | App (TypeApp (TypeApp (EVar patError) (TypeConApp liftedRep [])) ty) (Lit UndefinedLit) <- e
              , liftedRep ^. typeConName . nameString == "LiftedRep"
              , equivType bottomType (_varType patError) =
                TypeApp Bottom ty
              | otherwise = e
    defIsProperty (v, _) | isConstructorVar v = False
    defIsProperty (v, _) = case retType of
      TypeConApp t [] -> t ^. typeConName . nameString == phileasPreludePropertyStr
      _               -> False
      where retType = funTypeRet (v ^. varType)
    phileasPreludePropertyStr = "Property"
    topVars = map fst (HMap.toList (m ^. topDefinitions))
    varHasStringName str = (==str) . (^. varName . nameString)
    findVarImplies = findVar "⇒"
    findVarIdentical = findVar "≡"
    findVarForAll = findVar "∀"
    findVar str = case filter (varHasStringName str) topVars of
      []  -> throwError ("var " ++ str ++ " not found")
      [v] -> return v
      _   -> throwError ("unexpected multiple definitions of " ++ str)
    moduleProperties :: Var -> Var -> Var -> Module -> [Property]
    moduleProperties varImplies varIdentical varForAll mod =
      mapMaybe mkPropertyMay topVars
      -- [ mkPropertyMay (Just (var ^. varName . nameString)) def
      -- | t@(var, DefExpr def) <- topVars
      -- , defIsProperty t ]
      where
        topVars = HMap.toList (_topDefinitions mod)
        mkPropertyMay :: (Var, DefBody) -> Maybe Property
        mkPropertyMay (var, DefExpr e)
          | isConstructorVar var = Nothing
          | var`elem`[varForAll, varImplies, varIdentical] = Nothing
          | otherwise = case funTypeSig (var ^. varType) of
              TypeSig _ args ret
                | any isPropertyType args -> Nothing
                | isPropertyType ret ->
                  Just $ mkProperty (Just (var ^. varName . nameString)) e
                | otherwise -> Nothing
          where isPropertyType t = case t of
                  TypeConApp t [] ->
                    t ^. typeConName . nameString == phileasPreludePropertyStr
                  _ -> False
        mkPropertyMay _ = Nothing
        mkProperty :: Maybe String -> Expression -> Property
        mkProperty name def = putName (go def')
          where
            putName = set propName name
            def' = addForAllTop def
            -- | Implicit ∀ quantification of parameters. This allows properties
            -- of the form `prop a b = ...`
            addForAllTop e = case e of
              Lambda var e' -> App (EVar varForAll) (Lambda var (addForAllTop e'))
              TypeLambda var e' -> addForAllTop e'
              _ -> e
            insForAllVar :: Var -> Property -> Property
            insForAllVar var = over propForAllVars (HSet.insert var)
            goEq :: Expression -> (HashSet Var, Equality)
            goEq e = case e of
              App (TypeApp (EVar forall) _) (Lambda var b)
                | forall == varForAll -> case goEq b of
                    (vars, eq) -> (HSet.insert var vars, eq)
              App (App (TypeApp (EVar fun) _) a) b
                | fun == varIdentical -> (mempty, Eq a b)
              _ -> error $ "property parse error:\n" ++ prettyShow def
                   ++ prettyShow e ++ "\n"
                   ++ "\nimplies: " ++ show (varImplies ^. varId)
                   ++ "\nforall: " ++ show (varForAll ^. varId)
                   ++ "\nidentical: " ++ show (varIdentical ^. varId)
                   ++ "\n" ++ show e
            go e = case e of
              App (EVar forall) (Lambda var b)
                | forall == varForAll -> insForAllVar var (go b)
              App (App (EVar fun) a) b
                | varImplies == fun -> case unfoldPredImplication e of
                    (ants, conseq) ->
                      let
                        (forallVars, eq) = goEq conseq
                      in
                        Property {
                        _propName = Nothing
                        , _propForAllVars = forallVars
                        , _propEquality = eq
                        , _propAntecedents = map go ants
                        }
              Lambda var b -> insForAllVar var (go b)
              _ -> let (forallVars, eq) = goEq e
                   in Property {
                _propName = Nothing
                , _propForAllVars = forallVars
                , _propEquality = eq
                , _propAntecedents = mempty
                }
            unfoldPredImplication :: Expression -> ([Expression], Expression)
            unfoldPredImplication e = go e
              where
                go e = case e of
                  App (App (EVar impl) a) b
                    | varImplies == impl -> case go b of
                        (ants, conseq) -> (a : ants, conseq)
                  _ -> ([], e)


extendTheorem :: Property -> (Proof -> Proof) -> Theorem -> Theorem
extendTheorem prop extend thm = Theorem (extend (_thmProof thm)) prop

mkInitState :: FilteredModule -> ProverState
mkInitState fmod = ProverState {..}
  where _nextId = succ maxId
        maxId = maximumDef (Id 0) [i | i@Id{} <- universeBi m]
        m = _pmodule fmod

-- -- | hsFile = test/Data/Module.hs
-- proveFile :: MonadInteractions m =>
--   PhileasOptions -> [FilePath] -> FilePath
--   -> IO (Phileas m (HashMap String (Property, ProveResult)))
-- proveFile opts incl hsFile = sequence . proveModule <$> parseFilteredModule incl hsFile

parseFilteredModule :: [FilePath] -> FilePath -> IO FilteredModule
parseFilteredModule incl hsFile = do
  ParseResult{_coreModules, _dynFlags} <- loadCoreModules incl [hsFile]
  let CompileResult{_compiledCoreBinds, _phiModule} = compileModule _dynFlags _coreModules
  case filterModule _phiModule of
    Left err   -> error err
    Right fmod -> return fmod

proveModuleNoTrace :: PhileasOptions -> FilteredModule -> HashMap String (Property, ProveResult)
proveModuleNoTrace opts = runIdentity . runPhileas opts . sequence . fmap floatM . proveModule
  where floatM (a,b) = (a,) <$> b

runPhileas :: PhileasOptions -> Phileas m a -> m a
runPhileas = flip runReaderT

runPhileasNoTrace :: PhileasOptions -> Phileas Identity a -> a
runPhileasNoTrace opts = runIdentity . runPhileas opts

runPhileasIO :: MonadIO m => PhileasOptions -> Phileas Interact a -> m a
runPhileasIO opts = runInteract . runPhileas opts

runPhileasTimeoutIO :: MonadIO m => PhileasOptions
  -> (Property, Phileas Interact ProveResult) -> m ProveResult
runPhileasTimeoutIO opts (prop, x) =
  case micro of
    Just tm
      | tm > 0 -> fmap (fromMaybe (Timeout prop)) . timeout tm . runInteract . runPhileas opts $ x
    _ -> runPhileasIO opts x
  where micro = _timeoutMicro opts

proveModule :: forall m. MonadInteractions m =>
  FilteredModule -> HashMap String (Property, Phileas m ProveResult)
proveModule fmod = HMap.fromList (map proveProperty (_properties fmod))
  where
    getGoalName :: Property -> String
    getGoalName = fromJustNote "unnamed top level property" . _propName
    proveProperty :: Property -> (String, (Property, Phileas m ProveResult))
    proveProperty prop = (propName, (prop, runProver))
      where
        mkEnv :: PhileasOptions -> Property -> ProverEnv
        mkEnv opts prop =
          typeCheck $
          ProverEnv {
          _goal = prop
          , _fmodule = fmod
          , _options = opts
          }
        propName = getGoalName prop
        runProver = do
          opts <- ask
          let env = mkEnv opts prop
          _answer <- liftBase (runReaderT (evalStateT proofStep iniState) env)
          return ProveResult {..}
            where _property = prop
    iniState = mkInitState fmod

proofStep :: MonadInteractions m => Prover m ProverAnswer
proofStep = do
  traceStep
  simplified $ do
    traceEnv
    disjoin [
      stepRefl
      , stepAntecedentAbsurd
      , stepAbsurdCong
      , stepCong
      , stepFactorApp
      , stepExtensionalEq
      , stepCongHyp
      , stepUseEq
      , stepInduction
      ]

-- | If every subproof succeeds then a theorem is returned.
--
-- If at least one subproof reaches a contradiction, then, if `iff` is True the
-- theorem is proven false, otherwise returns Dunno
--
-- If none of the above happens, the prover returns Dunno.
conjoin :: forall m. MonadInteractions m => Bool -> Property -> ([Proof] -> Proof)
  -> [Prover m ProverAnswer] -> Prover m ProverAnswer
conjoin iff prop joinProofs = go (Just [])
  where
    go :: Maybe [Proof] -> [Prover m ProverAnswer] -> Prover m ProverAnswer
    go (Just revPrfs) [] = return $
      FoundTrue $ Theorem (joinProofs (reverse revPrfs)) prop
    go Nothing [] = dunno
    go revPrfs (x:xs) = do
      a <- x
      case a of
        FoundTrue thm -> go ((_thmProof thm :) <$> revPrfs) xs
        Dunno prop -> do
          prfFalse <- view (options . proveFalse)
          if
            | prfFalse -> go Nothing xs
            | otherwise -> dunno
        FoundFalse thm -> do
          prop <- view goal
          if
            | iff -> return $ FoundFalse $ extendTheorem prop (joinProofs . pure) thm
            | otherwise -> dunno

conjoinIff :: MonadInteractions m => Property -> ([Proof] -> Proof) -> [Prover m ProverAnswer] -> Prover m ProverAnswer
conjoinIff = conjoin True

conjoinSuff :: MonadInteractions m => Property -> ([Proof] -> Proof) -> [Prover m ProverAnswer] -> Prover m ProverAnswer
conjoinSuff = conjoin False

continueIff :: MonadInteractions m => (Proof -> Proof) -> Prover m ProverAnswer -> Prover m ProverAnswer
continueIff = continue True

continueSuff :: MonadInteractions m => (Proof -> Proof) -> Prover m ProverAnswer -> Prover m ProverAnswer
continueSuff = continue False

continue :: MonadInteractions m => Bool -> (Proof -> Proof) -> Prover m ProverAnswer -> Prover m ProverAnswer
continue iff extProof m = do
  a <- m
  prop <- view goal
  case a of
    FoundTrue thm -> return $ FoundTrue $ extendTheorem prop extProof thm
    FoundFalse thm
      | iff -> return $ FoundFalse $ extendTheorem prop extProof thm
      | otherwise -> dunno
    Dunno _ -> dunno

dunno :: MonadInteractions m =>  Prover m ProverAnswer
dunno = Dunno <$> view goal

disjoin :: MonadInteractions m => [Prover m ProverAnswer] -> Prover m ProverAnswer
disjoin ps = do
  case ps of
    [] -> dunno
    (p:ps) -> do
      r <- p
      case r of
        FoundTrue thm  -> return r
        FoundFalse thm -> return r
        Dunno prop     -> disjoin ps

stepRefl :: MonadInteractions m =>  Prover m ProverAnswer
stepRefl = do
  p <- view goal
  let eq = p ^. propEquality
  case equalityRefl eq of
    Just proof -> return $ FoundTrue Theorem {
      _thmProof = proof
      , _thmProperty = p
      }
    Nothing    -> dunno


stepInduction :: forall m. MonadInteractions m => Prover m ProverAnswer
stepInduction = do
  prop <- view goal
  let tops = predTopExpressions prop
      blocks = inductionCandidates tops
      blocks' = blocks <> maybeToList (expandableCandidates (prop ^. propEquality))
  disjoin (steps blocks')
  where
    steps :: [Blocking] -> [Prover m ProverAnswer]
    steps = map stepInductionExpr
    stepInductionExpr :: Blocking -> Prover m ProverAnswer
    stepInductionExpr block = do
      dep <- view (options . maxInductionDepth)
      let e = blockingExpr block
          event = blockingEvent block
      if
        | hasHappened event e >= dep -> dunno
        | otherwise -> do
            env <- ask
            let eq = _thmProperty ^. propEquality
                _thmProperty = env ^. goal
            mayCases <- inductOnExpression block eq env
            case mayCases of
              Nothing -> dunno
              Just x -> do
                let
                  datacons = map (\(d,_,_) -> d) x
                  mkthm :: [Proof] -> Proof
                  mkthm prfs = Induction (blockingExpr block) (zipExact datacons prfs)
                prop <- view goal
                conjoinSuff prop mkthm [
                    proofStepWithEnv env'
                  | (dcon, hyps, env) <- x
                  , let env' = over (goal . propAntecedents) (hyps ++) env ]

stepUseEq :: MonadInteractions m =>  Prover m ProverAnswer
stepUseEq = do
  _thmProperty <- view goal
  let eq = _thmProperty ^. propEquality
      hyps = mapMaybe getConcreteEq (_thmProperty ^. propAntecedents)
      usedEq = firstJust (justWhenEq eq) hyps
  case usedEq of
    Nothing -> dunno
    Just (hyp, name) -> let _thmProof = UseEq hyp name
      in return $ FoundTrue Theorem {..}
    where
      justWhenEq a (b, name)
        | a == b = Just (b, name)
        | otherwise = Nothing
      getConcreteEq Property{..}
        | HSet.null _propForAllVars &&
          null _propAntecedents = Just (_propEquality, _propName)
        | otherwise = Nothing

proofTheorem :: MonadInteractions m =>  Proof -> Prover m Theorem
proofTheorem _thmProof = do
  _thmProperty <- view goal
  return Theorem {..}

stepExtensionalEq :: MonadInteractions m =>  Prover m ProverAnswer
stepExtensionalEq = do
  eq@Eq{..} <- view (goal . propEquality)
  case (exprType _eqLeft, exprType _eqRight) of
    (FunType arg ret, FunType arg2 ret2)
      | arg == arg2 -> do
          x <- newUniversalVar mempty arg
          let eq' = Eq (appx x _eqLeft) (appx x _eqRight)
              appx x f = App f (EVar x)
          continueIff (ExtensionalEq x) (proofStepWithEq eq')
      | otherwise -> dunno
    _ -> dunno

subProof :: MonadInteractions m =>  Property -> Prover m ProverAnswer
subProof = proofStepWithGoal

stepFactorApp :: MonadInteractions m =>  Prover m ProverAnswer
stepFactorApp = do
  Eq{..} <- view (goal . propEquality)
  case (unfoldApp1 _eqLeft, unfoldApp1 _eqRight) of
    (Just (f1, args1), Just (f2, args2))
      | f1 == f2 -> do
          let mkthm = FactorApp f1
              eqs = zipWithExact Eq (toList args1) (toList args2)
          prop <- view goal
          conjoinSuff prop mkthm (map proofStepWithEq eqs)
    _ -> dunno

stepCong :: MonadInteractions m =>  Prover m ProverAnswer
stepCong = do
  eq <- view (goal . propEquality)
  case equalityCong eq of
    Just (con, eqs) -> do
      let mkthm :: [Proof] -> Proof
          mkthm = Cong con
      prop <- view goal
      conjoinIff prop mkthm (map proofStepWithEq eqs)
    Nothing -> dunno

stepCongHyp :: MonadInteractions m =>  Prover m ProverAnswer
stepCongHyp = do
  env <- ask
  hyps <- view (goal . propAntecedents)
  let hyps' = map factorizeProperty hyps
      worked = any isRight hyps'
      factHyps = concatMap fromRes hyps'
      env' = set (goal . propAntecedents) factHyps env
  if
    | worked -> continueIff HypCong (proofStepWithEnv env')
    | otherwise -> dunno
    where
      fromRes (Left p)   = [p]
      fromRes (Right ps) = ps
      -- | Either the original property or an equivalent conjunction of
      -- properties Return Left if no change was made. Right if factorization
      -- was applied at least one time. Note that the goal may not have changed
      -- if at least one hypotheses changed.
      factorizeProperty :: Property -> Either Property [Property]
      factorizeProperty p@Property{..} = x
        where
          congeq = equalityCong _propEquality
          x = case congeq of
            Nothing
              | worked -> Right [p]
              | otherwise -> Left p
            Just (dcon, eqs) -> Right [ set propAntecedents ants'
                                        $ set propEquality eq p | eq <- eqs ]
          ants' = concatMap fromRes ants
          ants = map factorizeProperty _propAntecedents
          worked = any isRight ants

-- | NOTE that this is a very simple step since it does only work when there are no
-- hypotheses.
stepAbsurdCong :: MonadInteractions m =>  Prover m ProverAnswer
stepAbsurdCong = do
  prop <- isContradiction <$> view goal
  case prop of
    Just abs -> return (FoundFalse (Theorem (Absurd abs) abs))
    Nothing  -> dunno

-- | Tries to find a contradiction in the hypotheses.
stepAntecedentAbsurd :: MonadInteractions m =>  Prover m ProverAnswer
stepAntecedentAbsurd = do
  hyps <- view (goal . propAntecedents)
  let contrs = mapMaybe isContradiction hyps
  case contrs of
    []    -> dunno
    (p:_) -> FoundTrue <$> proofTheorem (AbsurdHyp p)

isContradiction :: Property -> Maybe Property
isContradiction p@Property{..}
  | [] <- _propAntecedents
  , Eq l r <- _propEquality
  , Just (ConsApp a _) <- unfoldConsApp l
  , Just (ConsApp b _) <- unfoldConsApp r
  , a /= b = Just p
  | otherwise = Nothing

proofStepWithEq :: MonadInteractions m => Equality -> Prover m ProverAnswer
proofStepWithEq q = local (set (goal . propEquality) q) proofStep

proofStepWithGoal :: MonadInteractions m => Property -> Prover m ProverAnswer
proofStepWithGoal g = local (set goal g) proofStep

proofStepWithEnv :: MonadInteractions m => ProverEnv -> Prover m ProverAnswer
proofStepWithEnv env = local (const env) proofStep

traceP :: MonadInteractions m => String -> Prover m ()
traceP = whenDebug . liftBase . traceM'

whenDebug :: Monad m => Prover m () -> Prover m ()
whenDebug = whenM (view (options . debugTrace))

traceStep :: MonadInteractions m => Prover m ()
traceStep = traceP ("\n+" <> replicate 60 '-' <> "+\n")

traceEnv :: forall m. (MonadInteractions m) => Prover m ()
traceEnv = do
  whenDebug $ do
    traceBlocking
    traceHyps
    traceGoal
      where
        ltraceM' = liftBase . traceM'
        traceBlocking = do
          prop <- view goal
          let blks = inductionCandidates (predTopExpressions prop)
          sty <- view (options . traceStyle)
          ltraceM' $ render $ vcat
            [ int ix <> ". blocking: " <> prettyPhi sty e <> " :: "  <> pty
            | (ix, blockingExpr -> e) <- zip [0..] blks
            , let tye = exprType e
                  pty = prettyPhi sty tye
            ]
          ltraceM' ""
        traceGoal :: Prover m ()
        traceGoal = do
          g <- view (goal . propEquality)
          sty <- view (options . traceStyle)
          ltraceM' (render ("Goal:" $+$ prettyPhi sty g))
        traceHyps :: Prover m ()
        traceHyps = do
          hs <- view (goal . propAntecedents)
          sty <- view (options . traceStyle)
          ltraceM' $ render $ vcat [ "hypothesis " <> int i <> ".\n" <> prettyPhi sty h
                                  | (i, h) <- zip [1..] hs ]

instance HeadBetaReducible ProverEnv where
  eval ProverEnv{..} = ProverEnv {
    _goal = eval _goal
    , _fmodule
    , _options
    }

simplified :: MonadInteractions m => Prover m ProverAnswer -> Prover m ProverAnswer
simplified a = local simplify a
  where simplify = over goal simplifyProperty

-- Just a sanity check for debugging
typeCheck :: ProverEnv -> ProverEnv
typeCheck p
  | and checks = p
  | otherwise = error ""
  where allExpressions = universeBi p
        checks = [ seq (exprType e) True | e <- allExpressions ]

mkSimpleProperty :: String -> Expression -> Expression -> Property
mkSimpleProperty propName a b = Property {..}
  where
    _propName = Just propName
    _propForAllVars = freeVars a <> freeVars b
    _propAntecedents = []
    _propEquality = Eq a b

instance Pretty ProveResult where
  pPrint ProveResult {..} = pPrint _property $+$ pPrint _answer
  pPrint Timeout {..} = pPrint _property $+$ "Timeout"

instance PrettyPhi ProverEnv where
  rprettyPhi ProverEnv{..} = rprettyPhi _goal
