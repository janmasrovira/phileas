{-# LANGUAGE DuplicateRecordFields #-}
module Phileas.Main where

import           GHC                              (CoreModule (..))
import           Language.GhcCore.Backend.Phi
import           Language.Haskell.Backend.GhcCore
import           Language.Haskell.Coloured
import           Options.Applicative
import           Phileas.Phi
import           Phileas.Phi.Pretty
import           Phileas.Prelude
import           Phileas.Prover
import qualified Rainbow                          as R
import           System.Timeout

data ParseOptions = POptions {
  _inputFile            :: Maybe String
  , _includeDirs        :: String
  , _proveFalse         :: Bool
  , _outputGhcCore      :: Bool
  , _outputPhi          :: Bool
  , _outputPhiAst       :: Bool
  , _outputProof        :: Bool
  , _prettyShowId       :: Bool
  , _prettyShowType     :: Bool
  , _prettyShowConsType :: Bool
  , _prettyShowCaseType :: Bool
  , _prettyShowHistory  :: Bool
  , _debugTrace         :: Bool
  , _maxInductionDepth  :: Int
  , _timeoutSeconds     :: Maybe Int
  }
  deriving (Show)

data Options = Options {
  _inputFile          :: String
  , _includeDirs      :: [FilePath]
  , _timeoutSeconds   :: Maybe Int
  , _proveFalse       :: Bool
  , _outputGhcCore    :: Bool
  , _outputPhi        :: Bool
  , _outputProof      :: Bool
  , _outputPhiAst     :: Bool
  , _phiStyle         :: PhiStyle
  , _proveResultStyle :: ProverAnswerStyle
  , _phileasOptions   :: PhileasOptions
  }

type MApp = ReaderT Options IO

poptions :: Parser ParseOptions
poptions =
  POptions <$>
  optional
    (strOption
       (long "input" <> short 'i' <> metavar "infile" <>
        help "input file to be analyzed")) <*>
  strOption
    (long "include" <> short 'l' <> metavar "dir" <>
     help
       "include directories separated by ':'. One must contain PhileasPrelude.hs")
  <*>
  switch (long "prove-false" <> help "also tries to prove that the theorem is false") <*>
  switch (long "ghc-core" <> help "output ghc-core") <*>
  switch (long "phi" <> help "output phi") <*>
  switch (long "phi-ast" <> help "output phi ast") <*>
  switch (long "show-proof" <> help "output proof of theorems") <*>
  switch (long "show-id" <> help "output var id") <*>
  switch (long "show-type" <> help "output var type") <*>
  switch (long "show-cons-type" <> help "output constructor type") <*>
  switch (long "show-case-type" <> help "output case return type") <*>
  switch (long "show-history" <> help "trace var history") <*>
  switch (long "trace" <> help "output debug trace") <*>
  option auto (long "max-induction-depth"
                <> help "Allow repeated induction this many times"
                <> showDefault
                <> value 1
                <> metavar "INT")  <*>
  option intOption (long "timeout" <> short 't'
               <> help "timeout per property"
               <> value Nothing
               <> metavar "SECONDS"
              )
  where
    intOption :: ReadM (Maybe Int)
    intOption = Just <$> maybeReader readMay


opts :: ParserInfo ParseOptions
opts =
  info
    (poptions <**> helper)
    (fullDesc <>
     progDesc
       "Developed by Jan Mas Rovira (janmasrovira@gmail.com). Phileas is an automatic theorem prover that uses first order logic and structural induction to prove properties about inductively defined data types" <>
     header "Automatic theorem prover")

main :: IO ()
main = execParser opts >>= runApp

parseOptions :: ParseOptions -> IO Options
parseOptions POptions{..} = do
  _inputFile <- case _inputFile of
    Nothing -> fail "No input file specified"
    Just f  -> return f
  let includeDirs = splitOn ":" _includeDirs
      _phiStyle = defaultStyle {_showVarId = _prettyShowId
                               , _showVarType = _prettyShowType
                               , _showConsType = _prettyShowConsType
                               , _showCaseType = _prettyShowCaseType
                               , _showVarHistory = _prettyShowHistory
                               }
      _phileasOptions = PhileasOptions {
        _debugTrace
        , _traceStyle = _phiStyle
        , _proveFalse
        , _maxInductionDepth
        , _timeoutMicro = (* 10^6) <$>_timeoutSeconds
        }
      _proveResultStyle = ProverAnswerStyle {
        _showProof = _outputProof
        , _showProperty = _outputProof
        }
  return Options {_includeDirs = includeDirs, ..}

runApp :: ParseOptions -> IO ()
runApp = parseOptions >=> runReaderT app

phiStyle :: MApp PhiStyle
phiStyle = asks _phiStyle

app :: MApp ()
app = do
  Options{..} <- ask
  input <- readFile _inputFile
  ParseResult{_coreModules, _dynFlags} <- liftIO (loadCoreModules _includeDirs [_inputFile])
  let doStuffCoreModule :: GHC.CoreModule -> MApp ()
      doStuffCoreModule cm = do
        let fullCoreProgram = cm_binds cm
        putStrLn . hsColourTerm . showCoreProgram _dynFlags $ fullCoreProgram
  when _outputGhcCore
    (forM_ _coreModules doStuffCoreModule)
  let CompileResult{..} = compileModule _dynFlags _coreModules
      fullCoreProgram = concatMap cm_binds _coreModules
      doStuffPhi :: [GHC.CoreModule] -> MApp ()
      doStuffPhi cm = do
        let CompileResult{..} = compileModule _dynFlags cm
        let fullCoreProgram = concatMap cm_binds cm
        when _outputGhcCore
          (putStrLn . hsColourTerm . showCoreProgram _dynFlags $ concat _compiledCoreBinds)
        style <- phiStyle
        putStrLn (hsColourTerm (renderPhi style _phiModule))
        when _outputPhiAst (printPrettyShow _phiModule)
  when _outputPhi (doStuffPhi _coreModules)
  putStrLn (unlines $ ["", "PHILEAS RESULTS", replicate 15 '='])
  doSolve _phiModule

filterModuleIO :: Module -> MApp FilteredModule
filterModuleIO m = case filterModule m of
  Left err -> ioError (userError err)
  Right r  -> return r

doSolve :: Module -> MApp ()
doSolve m = do
  opts <- asks _phileasOptions
  Options{..} <- ask
  fmod <- filterModuleIO m
  putStrLn $ "\nPhileas has found " <> show (length (_properties fmod)) <> " properties to prove.\n"
  resList <- sort . toList
    <$> sequence (fmap (runPhileasTimeoutIO opts) (proveModule fmod))
  mapM_ (printProveResult _proveResultStyle) resList
  putStrLn ""
  printStats resList
    where
      printProveResult sty r = putChunks (rainbow sty r) >> putStrLn ""
      printStats m = putStrLn $ show proven <> " out of " <> show total
        <> " theorems were proved. " <> show fail <> " could not be proved."
        where total = (length m)
              proven = length (filter provenTrue (mapMaybe resultAnswer (toList m)))
              fail = total - proven
