module Language.Haskell.Coloured (
  hsColourTerm
  ) where

import           Data.Colour.Names
import           Data.Colour.RGBSpace                (uncurryRGB)
import qualified Data.Colour.SRGB                    as C
import           Phileas.Prelude
import           Language.Haskell.HsColour
import           Language.Haskell.HsColour.Colourise
import           Language.Haskell.HsColour.Output

hsColourTerm :: String -> String
hsColourTerm = hscolour
  format colourPrefs inclAnchors partial title literate
  where
    inclAnchors = False
    colourPrefs = customColourPrefs
    format = TTYg XTerm256Compatible
    partial = False
    title = "output"
    literate = False

customColourPrefs :: ColourPrefs
customColourPrefs = ColourPrefs
  { keyword          = [Bold, fg cornflowerblue]
  , keyglyph         = [fg seagreen]
  , layout           = [fg cyan]
  , comment          = [fg white]
  , conid            = [Bold, fg orchid]
  , varid            = [Normal]
  , conop            = [Bold, fg orchid]
  , varop            = [fg lightskyblue]
  , string           = [fg magenta]
  , char             = [fg magenta]
  , number           = [fg palevioletred]
  , cpp              = [Dim, fg magenta]
  , selection        = [Normal, fg mediumseagreen]
  , variantselection = [Dim, fg red, Underscore]
  , definition       = [Normal]
  }

hsColour :: C.Colour Double -> Colour
hsColour = uncurryRGB Rgb . C.toSRGB24

fg :: C.Colour Double -> Highlight
fg = Foreground . hsColour
