{-# LANGUAGE TemplateHaskell #-}
module Language.Haskell.Backend.GhcCore (
  loadCoreModules
  , ParseResult(..)
  , showCoreProgram
  , showModuleCoreProgram
  ) where

import qualified CoreSyn       as H
import qualified Digraph       as H
import           Phileas.Prelude
import qualified GHC           as H
import qualified GHC.Paths     as H
import qualified HscMain       as H
import qualified HscTypes      as H
import qualified Module        as H
import qualified Outputable    as H
import qualified PprCore       as H
import qualified TidyPgm       as H


summaryToModule :: H.ModSummary -> H.Ghc H.CoreModule
summaryToModule = H.parseModule
             >=> H.typecheckModule
             >=> fmap H.coreModule . H.desugarModule
             >=> simplifyModule

simplifyModule :: H.ModGuts -> H.Ghc H.CoreModule
simplifyModule guts = do
  hsc_env <- H.getSession
  simpl_guts <- liftIO $ H.hscSimplify hsc_env guts
  (cg, md) <- liftIO $ H.tidyProgram hsc_env simpl_guts
  return $ H.CoreModule
    { H.cm_module = H.cg_module cg,
      H.cm_types = H.md_types md,
      H.cm_safe = H.mg_safe_haskell simpl_guts,
      H.cm_binds = H.cg_binds cg }

data ParseResult = ParseResult {
  _coreModules :: [H.CoreModule]
  , _dynFlags  :: H.DynFlags
  }
makeLenses ''ParseResult

setupGhc :: H.GhcMonad m => [FilePath] -> m H.DynFlags
setupGhc includeDirs = do
  flags <- H.getSessionDynFlags
  let flags' = flags { H.importPaths = includeDirs ++ H.importPaths flags }
  H.setSessionDynFlags flags'
  return flags'

loadCoreModules :: [FilePath] -> [FilePath] -> IO ParseResult
loadCoreModules includeDirs haskellFiles = runGhc $ do
  _dynFlags <- setupGhc includeDirs
  mapM_ loadFile haskellFiles
  graph <- H.depanal [] True
  let sorted_graph = H.flattenSCCs (H.topSortModuleGraph False graph Nothing)
  _coreModules <- mapM summaryToModule sorted_graph
  return ParseResult {..}

runGhc :: H.Ghc a -> IO a
runGhc = H.runGhc (Just H.libdir)

loadFile :: H.GhcMonad m => FilePath -> m H.SuccessFlag
loadFile filename = do
  target <- H.guessTarget filename Nothing
  H.setTargets [target]
  H.load H.LoadAllTargets

showCoreProgram :: H.DynFlags -> H.CoreProgram -> String
showCoreProgram f = H.showSDoc f . H.pprCoreBindings

showModuleCoreProgram :: H.DynFlags -> H.CoreModule -> String
showModuleCoreProgram f = showCoreProgram f . H.cm_binds
