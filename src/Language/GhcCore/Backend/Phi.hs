{-# LANGUAGE TemplateHaskell #-}
module Language.GhcCore.Backend.Phi (
  compileModule
  , CompileResult(..)
  ) where

import qualified Coercion           as H
import qualified CoreSyn            as H
import qualified Data.HashMap.Lazy  as HMap
import qualified DataCon            as H
import qualified GHC                as H
import qualified HscTypes           as H
import qualified HscTypes           as H
import qualified Literal            as H
import qualified Name               as H hiding (varName)
import qualified Outputable         as H
import qualified Pair               as H
import           Phileas.Phi.Extra
import           Phileas.Phi.Pretty
import           Phileas.Phi.Syntax
import           Phileas.Prelude
import qualified TyCoRep            as H
import qualified Type               as H
import qualified Unique             as H
import qualified Var                as H

instance Hashable H.Unique where
  hashWithSalt i u = hashWithSalt i (H.getKey u)
data CompileEnv = CompileEnv {
  _dynFlags :: H.DynFlags
  }
data CompileState = CompileState {
  _pmodule          :: Module
  , _cmodules       :: [H.CoreModule]
  , _uniqueMap      :: HashMap H.Unique Id
  , _nextId         :: Id
  , _varMap         :: HashMap H.Unique Var
  , _typeMap        :: HashMap H.Unique Type
  , _builtInVarMap  :: HashMap String Var
  , _builtInTypeMap :: HashMap String Type
  }
data CompileResult = CompileResult {
  _compiledCoreBinds :: [H.CoreProgram]
  , _phiModule       :: Module
  }
data BuiltInTypes = BuiltInTypes {
  _integerTypeCon :: Maybe TypeCon
  , _boolTypeCon  :: Maybe TypeCon
  , _listTypeCon  :: Maybe TypeCon
  , _trueVar      :: Maybe Var
  , _falseVar     :: Maybe Var
  , _nilVar       :: Maybe Var
  , _consVar      :: Maybe Var
  }
makeLenses ''CompileState
makeLenses ''CompileEnv

type Compile = ReaderT CompileEnv (State CompileState)

unsup :: H.Outputable a => a -> Compile b
unsup b = error . ("unsupported syntax: " ++) . (`H.showPpr`b) <$> asks _dynFlags

ierr :: String -> a
ierr msg = error ("programmer error: " ++ msg)

ierrM :: Compile String -> Compile a
ierrM msg = msg >>= ierr

compileModule :: H.DynFlags -> [H.CoreModule] -> CompileResult
compileModule _dynFlags m = ret (execState (runReaderT loadModules env) iniState)
  where
    ret s = CompileResult {
      _compiledCoreBinds = map H.cm_binds (s ^. cmodules)
      , _phiModule = s ^. pmodule
      }
    env = CompileEnv {..}
    iniState = CompileState {
      _nextId = Id 0
      , _pmodule = emptyModule
      , _cmodules = m
      , _uniqueMap = HMap.empty
      , _varMap = HMap.empty
      , _typeMap = HMap.empty
      , _builtInTypeMap = HMap.empty
      , _builtInVarMap = HMap.empty
      }
    emptyModule = Module {
      _topDefinitions = HMap.empty
      , _dataTypes = HMap.empty
      , _nameMap = HMap.empty
      }

loadBuiltIn :: Compile ()
loadBuiltIn = do
  x <- findBuiltInTypes
  loadBuiltInTypes x
  loadBuiltInFuns x

loadModules :: Compile ()
loadModules = do
  filterInternalBinds
  loadBuiltIn
  ms <- use cmodules
  forM_ ms $ \m -> do
    loadDataConstructors (H.typeEnvDataCons (H.cm_types m))
    loadCoreProgram (H.cm_binds m)

loadCoreProgram :: H.CoreProgram -> Compile ()
loadCoreProgram = mapM_ loadTopLevelBind
  where
    loadTopLevelBind b = case b of
      H.NonRec var e -> declareTopLevelVar False var e
      H.Rec bs       -> mapM_ (uncurry (declareTopLevelVar True)) bs

newId :: Compile Id
newId = do
  i <- use nextId
  modifying nextId succ
  return i

compilePolyTypeType :: H.Type -> Compile PolyTypeVar
compilePolyTypeType hty = case hty of
  H.TyVarTy v -> compilePolyTypeVar v
  _ -> ierrM (("expected type variable, but found " ++) <$> showOutputable hty)

compilePolyTypeVar :: H.Var -> Compile PolyTypeVar
compilePolyTypeVar hvar = do
  i <- uniqueId (H.getUnique hvar)
  n <- compileName i (H.getName hvar)
  pvar <- newPolyTypeVar n
  declareTypeVar hvar (PolyVarType pvar)
  return pvar

newPolyTypeVar :: Name -> Compile PolyTypeVar
newPolyTypeVar _polyName = do
  _polyId <- newId
  return PolyTypeVar {..}

newVar :: H.Var -> VarSort -> Compile Var
newVar hvar _varSort = do
  _varId <- newId
  _varType <- compileType (H.varType hvar)
  _varName <- compileName _varId (H.varName hvar)
  let var = Var {..}
  declareVar hvar var
  return var

newName :: String -> Compile Name
newName str = (`Name` str) <$> newId

uniqueId :: H.Unique -> Compile Id
uniqueId u = do
  m <- use uniqueMap
  case HMap.lookup u m of
    Just i -> return i
    Nothing -> do
      i <- newId
      modifying uniqueMap (HMap.insert u i)
      return i

compileType :: H.Type -> Compile Type
compileType ty =
  firstJustDefaultM varTy
    [ forallTy
    , funTy
    , conAppTy
    , appTy
    ]
  where
    forallTy, funTy, appTy, conAppTy :: Compile (Maybe Type)
    forallTy
      | Just (tyVar, rest) <- H.splitForAllTy_maybe ty = do
          forallVar <- compilePolyTypeVar tyVar
          Just . ForAllType forallVar <$> compileType rest
      | otherwise = return Nothing
    funTy
      | Just (arg, ret) <- H.splitFunTy_maybe ty =
          Just <$> liftM2 FunType (compileType arg) (compileType ret)
      | otherwise = return Nothing
    conAppTy
      | Just (tycon, args) <- H.splitTyConApp_maybe ty =
          Just <$> liftM2 TypeConApp (compileTypeCon tycon) (mapM compileType args)
      | otherwise = return Nothing
    appTy
      | Just (fun, arg) <- H.splitAppTy_maybe ty =
          Just <$> liftM2 AppType (compileType fun) (compileType arg)
      | otherwise = return Nothing
    varTy :: Compile Type
    varTy
      | Just tyvar <- H.getTyVar_maybe ty = lookupTypeVar tyvar
      | otherwise = ierrM ((("cannot compile type: " ++ showTypeCon ty ++ ", ") ++) <$> showOutputable ty)
      where
        showTypeCon t = case t of
            H.TyVarTy{}    -> "TyVarTy"
            H.CastTy{}     -> "CastTy"
            H.CoercionTy{} -> "CoercionTy"
            H.LitTy{}      -> "LitTy"
            H.TyConApp{}   -> "TyConApp"
            H.AppTy{}      -> "AppTy"
            _              -> "other"

declareVar :: H.Var -> Var -> Compile ()
declareVar k v = modifying varMap (HMap.insert (H.getUnique k) v)

compileVar :: VarSort -> H.Var -> Compile Var
compileVar _varSort hvar = do
  _varId <- uniqueId (H.getUnique hvar)
  _varType <- compileType (H.varType hvar)
  _varName <- compileName _varId (H.varName hvar)
  return Var {..}

compileTypeCon :: H.TyCon -> Compile TypeCon
compileTypeCon tyCon = do
  _typeConId <- uniqueId (H.getUnique tyCon)
  _typeConName <- compileName _typeConId (H.getName tyCon)
  return TypeCon {..}

declareTopLevelVar :: Bool -> H.Var -> H.CoreExpr -> Compile ()
declareTopLevelVar isRec hvar he = do
  var0 <- compileVar (DefinedVar Nothing isRec) hvar
  declareVar hvar var0
  e <- compileExpr he
  let de = DefExpr e
      var = definedVar var0 de
  declareVar hvar var
  modifying (pmodule . topDefinitions) (HMap.insert var de)

filterInternalBinds :: Compile ()
filterInternalBinds = do
  cms <- use cmodules
  mapM filterModule cms >>= assign cmodules
  where
    filterModule :: H.CoreModule -> Compile H.CoreModule
    filterModule cm = do
      p' <- mapMaybeM aux (H.cm_binds cm)
      return cm {H.cm_binds = p'}
    aux :: H.CoreBind -> Compile (Maybe H.CoreBind)
    aux b = case b of
      H.NonRec v e -> aux1 (v, e) >>= return . fmap (uncurry H.NonRec)
      H.Rec rs -> do
        rs' <- mapMaybeM aux1 rs
        return (if null rs' then Nothing else Just (H.Rec rs'))
    aux1 :: (H.Var, H.CoreExpr) -> Compile (Maybe (H.Var, H.CoreExpr))
    aux1 (v, e) =
      ifM (isInternalVar v) (return Nothing) (return (Just (v,e)))
    isInternalVar :: H.Var -> Compile Bool
    isInternalVar = isInternalType . H.varType
    isInternalType :: H.Type -> Compile Bool
    isInternalType t = do
      s <- mapM showOutputable tycons
      return (any (`elem`internalTypesStrs) s)
      where
        tycons = [ tycon | H.TyConApp tycon _ <- universe t ]
        internalTypesStrs = ["Addr#", "TyCon", "KindRep"
                            , "TrName", "KindRepFun", "Module"
                            , "KindRepTyConApp"]

-- | Some internal definitions should not be exported since they are of no
-- interest. For now, it suffices to check if they contain a '$' sign.
topVarShouldBeExported :: Var -> Compile Bool
topVarShouldBeExported = return . not . elem '$' . _nameString . _varName

declareLambdaVar :: H.Var -> Compile Var
declareLambdaVar hvar = do
  var <- compileVar (UniversalVar mempty) hvar
  declareVar hvar var
  return var

declareLetVar :: Bool -> H.Var -> Compile Var
declareLetVar isRec hvar = do
  var <- compileVar (DefinedVar Nothing isRec) hvar
  declareVar hvar var
  return var

declareBuiltInVar :: Var -> Compile ()
declareBuiltInVar v = modifying builtInVarMap (HMap.insert k v)
  where k = v ^. varName . nameString

declareTypeVar :: H.Var -> Type -> Compile ()
declareTypeVar k v = modifying typeMap (HMap.insert (H.getUnique k) v)

declareBuiltInType :: DataTypeDef -> Type -> Compile ()
declareBuiltInType d v = modifying builtInTypeMap (HMap.insert k v)
  where k = d ^. typeCon . typeConName . nameString

addDataTypeDef :: DataTypeDef -> Compile ()
addDataTypeDef d = modifying (pmodule . dataTypes)
  (HMap.insert (d ^. typeCon) (DefinedTypeCon d))

lookupDataTypeDef :: TypeCon -> Compile TypeConDataDef
lookupDataTypeDef tc = HMap.lookupDefault err tc <$> use (pmodule . dataTypes)
  where err = error "DataType not defined"

addPrimitiveDataTypeDef :: TypeCon -> Compile ()
addPrimitiveDataTypeDef t = modifying (pmodule . dataTypes)
  (HMap.insert t PrimitiveTypeCon)

compileLiteral :: H.Literal -> Compile Literal
compileLiteral l = case l of
  H.MachInt i      -> return (IntLit i)
  H.LitInteger i _ -> return (IntLit i)
  _                -> return UndefinedLit

compileName :: Id -> H.Name -> Compile Name
compileName namedId n = do
  _nameString <- showOutputable n
  _nameId <- uniqueId (H.nameUnique n)
  let name = Name {..}
  modifying (pmodule . nameMap) (HMap.insert namedId name)
  return name

compileAlt :: H.CoreAlt -> Compile Alt
compileAlt (acon, hvars, e) = do
  let ce = compileExpr e
  case acon of
    H.DEFAULT -> Alt Wildcard <$> ce
    H.DataAlt con -> do
      con <- lookupVar (H.dataConWorkId con)
      vars <- forM hvars declareLambdaVar
      Alt (PatCons (PConsApp con vars)) <$> ce
    H.LitAlt l -> error "match lit"

showOutputable :: H.Outputable a => a -> Compile String
showOutputable a = (`H.showPpr`a) <$> view dynFlags

lookupTypeVar :: H.TyVar -> Compile Type
lookupTypeVar hty = firstJustDefaultM err
    [ HMap.lookup (H.getUnique hty) <$> use typeMap
    , do
        s <- showOutputable hty
        HMap.lookup s <$> use builtInTypeMap ]
  where
  err = ierrM ((\s -> "type '" ++ s ++ "' not found") <$> showOutputable hty)

lookupVar :: H.Var -> Compile Var
lookupVar hvar =
  firstJustDefaultM err
      [ HMap.lookup (H.getUnique hvar) <$> use varMap
      , do
        s <- showOutputable hvar
        HMap.lookup s <$> use builtInVarMap ]
  where
    err = ierrM ((\s -> "var '" ++ s ++ "' not found") <$> showOutputable hvar)

loadBuiltInFuns :: BuiltInTypes -> Compile ()
loadBuiltInFuns BuiltInTypes {..} = do
  loadPatError
    where
      loadPatError = do
        _varName <- newName "patError"
        _varId <- newId
        polyName <- newName "a"
        poly <- newPolyTypeVar polyName
        let _varType = ForAllType poly (PolyVarType poly)
            _varSort = DefinedVar (Just (DefExpr Bottom)) True
            patError = Var {..}
        declareBuiltInVar patError

findBuiltInTypes :: Compile BuiltInTypes
findBuiltInTypes = do
  m <- concatMap H.cm_binds <$> use cmodules
  let types = [ H.varType v | v <- vars]
      tycons = [ t | H.TyConApp t _ <- types]
      vars = [ v | H.Var v <- concatMap allExpr m ]
      allExpr :: H.CoreBind -> [H.CoreExpr]
      allExpr b = universeBi b
  _integerTypeCon <- firstJustM isInteger tycons
  _boolTypeCon <- firstJustM isBool tycons
  _listTypeCon <- firstJustM isList tycons
  _trueVar <- firstJustM isTrueVar vars
  _falseVar <- firstJustM isFalseVar vars
  _nilVar <- firstJustM isNilVar vars
  _consVar <- firstJustM isConsVar vars
  _bottomVar <- firstJustM isBottomVar vars
  return BuiltInTypes {..}
  where
    isInteger = compileIfHasName "Integer" compileTypeCon
    isBool = compileIfHasName "Bool" compileTypeCon
    isList = compileIfHasName "[]" compileTypeCon
    isTrueVar = compileIfHasName "True" (compileVar (ConstructorVar False))
    isFalseVar = compileIfHasName "False" (compileVar (ConstructorVar False))
    isNilVar = compileIfHasName "[]" (compileVar (ConstructorVar False))
    isConsVar = compileIfHasName ":" (compileVar (ConstructorVar True))
    isBottomVar = compileIfHasName "bottom" (compileVar (DefinedVar (Just (DefExpr Bottom)) False))
    compileIfHasName name compileFun x =
      ifM (hasStringName name x) (Just <$> compileFun x) (return Nothing)
    hasStringName name t = do
      i <- uniqueId (H.getUnique t)
      s <- _nameString <$> compileName i (H.getName t)
      return (s == name)

loadBuiltInTypes :: BuiltInTypes -> Compile ()
loadBuiltInTypes BuiltInTypes {..} = do
  loadBool
  loadList
  where
    loadBool = do
      let boolDataType = DataTypeDef {
            _typeDataCons = map DataCon [trueVar, falseVar]
            , _typeCon = boolTypeCon }
      declareBuiltInType boolDataType boolType
      forM_ [trueVar, falseVar] declareBuiltInVar
      addDataTypeDef boolDataType
      where falseVar = fromJustNote "falseVar missing" _falseVar
            trueVar = fromJustNote "trueVar missing" _trueVar
            boolTypeCon = fromJustNote "boolTypeCon missing" _boolTypeCon
            boolType = TypeConApp boolTypeCon []
    loadList = do
      let listDataType = DataTypeDef {
            _typeDataCons = map DataCon [nilVar, consVar]
            , _typeCon = listTypeCon
            }
      declareBuiltInType listDataType listType
      forM_ [nilVar, consVar] declareBuiltInVar
      addDataTypeDef listDataType
        where listTypeCon = fromJustNote "listTypeCon missing" _listTypeCon
              listType = case nilVar ^. varType of
                ForAllType poly _ -> TypeConApp listTypeCon [PolyVarType poly]
                _ -> ierr "wrong type"
              nilVar = fromJustNote "nilVar missing" _nilVar
              consVar = fromJustNote "consVar missing" _consVar

compileBind :: H.CoreBind -> Compile Bind
compileBind b = case b of
  H.NonRec hvar e -> NonRec <$> compileBinding False (hvar, e)
  H.Rec bs        -> Rec <$> mapM (compileBinding True) bs

compileBinding :: Bool -> (H.Var, H.CoreExpr) -> Compile (Var, Expression)
compileBinding isRec (hvar, e) =
  liftM2 (,) (declareLetVar isRec hvar) (compileExpr e)

compileExpr :: H.CoreExpr -> Compile Expression
compileExpr e = case e of
  H.Var v               -> EVar <$> lookupVar v
  H.Lit l               -> Lit <$> compileLiteral l
  H.App a b             -> compileApp a b
  H.Lam var e           -> compileLambda var e
  H.Let bd e            -> compileLet bd e
  H.Case e var ret alts -> compileCase e var ret alts
  H.Cast e c            -> compileCast e c
  H.Tick{}              -> unsup e
  H.Type{}              -> unsup e
  H.Coercion{}          -> unsup e
  where
    -- | Used, e.g. for newtypes
    compileCast :: H.CoreExpr -> H.Coercion -> Compile Expression
    compileCast he (H.unPair . H.coercionKind -> (hfromType, htoType)) = do
      e <- compileExpr he
      fromType <- compileType hfromType
      toType <- compileType htoType
      let caseUnwrap = case fromType of
               TypeConApp tycon args -> do
                 x <- findNewtypeCon tycon
                 case x of
                   Nothing -> return e
                   Just newtypecon -> do
                     caseId <- CaseId <$> newId
                     _varId <- newId
                     _varName <- newName "unwrap"
                     let argVar = Var {..}
                         con = _conVar newtypecon
                         _varSort = UniversalVar mempty
                         _varType = toType
                         alt = Alt (PatCons (PConsApp con [argVar])) (EVar argVar)
                         caseExpr = Case caseId e toType [alt]
                     return caseExpr
               _ -> return e
      case toType of
        TypeConApp tycon args -> do
          x <- findNewtypeCon tycon
          case x of
            Just newtypecon ->
              let con = EVar (_conVar newtypecon)
                  specializedCon = foldTypeApp con args
              in return (App specializedCon e)
            Nothing -> caseUnwrap
        _ -> caseUnwrap
      where
        findNewtypeCon tycon = do
          x <- lookupDataTypeDef tycon
          return $ case x of
            PrimitiveTypeCon -> error "primitive coercion"
            DefinedTypeCon def -> case _typeDataCons def of
              [newtypecon] -> Just newtypecon
              l -> Nothing
    compileLambda :: H.Var -> H.CoreExpr -> Compile Expression
    compileLambda hvar e
      | H.isTyVar hvar = do
          -- NOTE: For polymorphic lambdas such as
          -- id = \ (@ a) (@ b) (x :: a -> b) -> x
          -- (@ a) and (@ b) are type variables
          i <- uniqueId (H.getUnique hvar)
          polyName <- compileName i (H.getName hvar)
          polyVar <- newPolyTypeVar polyName
          declareTypeVar hvar (PolyVarType polyVar)
          ce <- compileExpr e
          return (TypeLambda polyVar ce)
      | otherwise = liftM2 Lambda (declareLambdaVar hvar) (compileExpr e)
    compileLet :: H.CoreBind -> H.CoreExpr -> Compile Expression
    compileLet b e = liftM2 Let (compileBind b) (compileExpr e)
    compileCase :: H.CoreExpr -> H.Var -> H.Type -> [H.CoreAlt] -> Compile Expression
    compileCase e hvar ret alts = do
      var <- declareLambdaVar hvar
      ret' <- compileType ret
      alts' <- putWildcardLast <$> mapM compileAlt alts
      e' <- compileExpr e
      let alts'' = subst var e' alts'
      caseId <- CaseId <$> newId
      return (Case caseId e' ret' alts'')
        where
          putWildcardLast (w@(Alt Wildcard _):xs) = snoc xs w
          putWildcardLast a = a
    subst var e = transformBi trans
      where trans x
              | EVar v <- x, v == var = e
              | otherwise = x
    compileApp :: H.CoreExpr -> H.CoreExpr -> Compile Expression
    compileApp ha b = case b of
      H.Type ht -> do
        t <- compileType ht
        a <- compileExpr ha
        return (TypeApp a t)
      _ -> liftM2 App (compileExpr ha) (compileExpr b)


loadDataConstructors :: [H.DataCon] -> Compile ()
loadDataConstructors ds = mapM_ loadDataTypeDef groupedByType
  where
    groupedByType :: [(H.Type, [H.DataCon])]
    groupedByType = map (\((ty, con) :| rest) -> (ty, con : map snd rest))
      $ groupBy (H.eqType`on`fst)
        $ sortBy (H.nonDetCmpType`on`fst)
        [ (H.dataConOrigResTy con, con) | con <- ds ]
    -- NOTE: it is safe to use nonDetCmpType here since we only rely on the
    -- ordering locally.
    loadDataTypeDef :: (H.Type, [H.DataCon]) -> Compile ()
    loadDataTypeDef (hresType, cons) = case H.splitAppTys hresType of
      (H.TyConApp tycon [], args) -> do
        _typeConId <- uniqueId (H.getUnique tycon)
        _typeConName <- compileName _typeConId (H.getName tycon)
        let _typeCon = TypeCon {..}
        _typeArgs <- mapM compilePolyTypeType args
        _typeDataCons <- mapM compileDataCon cons
        addDataTypeDef DataTypeDef {..}
      _ -> error "unexpected type"
    compileDataCon :: H.DataCon -> Compile DataCon
    compileDataCon con = do
      _varId <- uniqueId (H.getUnique con)
      _varName <- compileName _varId (H.getName con)
      _varType <- compileDataConType con
      let _varSort = ConstructorVar (isDataConSelfRec con)
          consVar = Var {..}
      declareVar (H.dataConWorkId con) consVar
      return (DataCon consVar)
    compileDataConType :: H.DataCon -> Compile Type
    compileDataConType = compileType . H.dataConUserType


isDataConSelfRec :: H.DataCon -> Bool
isDataConSelfRec con = any isArgSelfRec (H.dataConOrigArgTys con)
  where
    isArgSelfRec :: H.Type -> Bool
    isArgSelfRec a = case a of
      H.TyConApp tyCon _ -> H.getUnique tyCon == dataConResTyCon
      _                  -> False
    dataConResTyCon :: H.Unique
    dataConResTyCon = case H.dataConOrigResTy con of
      H.TyConApp tycon _ -> H.getUnique tycon
      _                  -> error "data constructor unexpected result type"
