projRootDir=.
execName=phileas
srcDir=src
exec=$(projRootDir)/$(execName)
cabalFile=$(projRootDir)/phileas.cabal
hsFiles=$(shell find $(srcDir) -type f -name '*.hs')

all: $(exec)

$(exec): $(hsFiles) $(cabalFile)
	stack install --local-bin-path=$(projRootDir)

.PHONY: clean
clean:
	rm -rf $(exec)

.PHONY: test
test-core: $(exec)
	$(exec) -i $(HS) --ghc-core

.PHONY: test
test: $(exec)
	$(exec) -i $(HS) --include "src/" $(FLAGS)
